﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager> {
  [HideInInspector]
  public bool SoundOn = true;
  [HideInInspector]
  public bool MusicOn = true;
  [HideInInspector]
  public float volume = 1f;

  public AudioSource[] music = null;
  public AudioSource[] sound = null;

  public void MuteSound(bool state) {
    SoundOn = !state;
    foreach (AudioSource aud in sound)
      aud.mute = state;
  }

  public void MuteMusic(bool state) {
    MusicOn = !state;
    if (music == null || music.Length == 0)
      return;
    foreach (AudioSource aud in music)
      aud.mute = state;
  }

  public void SetVolume(float vol) {
    volume = vol;
  }

  public void StopSound(string soundname) {
    if (!SoundOn)
      return;
    if (sound == null || sound.Length == 0)
      return;
    foreach (AudioSource aud in sound) {
      if (!aud.gameObject.name.Equals(soundname))
        continue;
      aud.Stop();
      break;
    }
  }

  public void PlayOneShot(string soundname) {
    if (!SoundOn)
      return;
    if (sound == null || sound.Length == 0)
      return;
    foreach (AudioSource aud in sound) {
      if (!aud.gameObject.name.Equals(soundname))
        continue;
      aud.PlayOneShot(aud.clip);
      break;
    }
  }

  public AudioSource Play(string soundname) {
    if (!SoundOn)
      return null;
    if (sound == null || sound.Length == 0)
      return null;
    foreach (AudioSource aud in sound) {
      if (!aud.gameObject.name.Equals(soundname))
        continue;
      aud.Play();
      return aud;
    }
    return null;
  }
}
