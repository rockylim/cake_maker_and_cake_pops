﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMManager : BaseManager {
  public Scrollbar scrollbar;
  public GameObject store;
  // Start is called before the first frame update
  void Start() {
    if (Constants.IS_PAID) {
      store.SetActive(false);
    }
  }

  public void LetsEat() {
  }

  //public void Freeze()
  //{
  //    StartCoroutine(routine());
  //    IEnumerator routine()
  //    {
  //        GameObject finish = GameObject.Find("Panel Finish");
  //        finish.SetActive(false);
  //        yield return new WaitForSeconds(0.2f);

  //        base.TakeScreenShot();
  //        yield return new WaitForSeconds(0.8f);

  //        finish.SetActive(true);
  //    }
  //}

  public void ScrollSnap(bool right) {
    float val = right ? 0.46f : -0.46f;
    scrollbar.value = Mathf.Clamp(scrollbar.value + val, 0f, 1f);
  }

}
