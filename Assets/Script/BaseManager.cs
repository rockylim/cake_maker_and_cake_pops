﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BaseManager : MonoBehaviour {
  public static List<string> loadedFiles = new List<string>();
  private bool takedScreenShot = false;
  const string m_screenshotFolder = "Screenshots";
  public void PlaySoundEffect(string soundname) {
    AudioManager.Instance.PlayOneShot(soundname);
  }

  public void PlaySoundEffectDelay(string parameters) {
    string[] param = parameters.Split(',');
    float delay = float.Parse(param[1]);
    string soundname = param[0];
    StartCoroutine(Delay());
    IEnumerator Delay() {
      yield return new WaitForSeconds(delay);
      PlaySoundEffect(soundname);
    }
  }

  public void PlayTapToEat() {
    AudioManager.Instance.StopSound("vo_fantastic");
    AudioManager.Instance.StopSound("vo_keep_going");
    AudioManager.Instance.PlayOneShot("vo_yummy");
  }

  public void StopSoundEffect(string soundname) {
    AudioManager.Instance.StopSound(soundname);
  }

  public void ShowBanner() {
    ServicesManager.Instance.ShowBanner();
  }

  public void HideBanner() {
    ServicesManager.Instance.HideBanner();
  }

  public void TakeScreenShot() {
    if (takedScreenShot)
      return;
    takedScreenShot = true;
    string filename = DateStr();
    string folderpath = Path.Combine(Application.persistentDataPath, m_screenshotFolder);
    if (!Directory.Exists(folderpath)) {
      Directory.CreateDirectory(folderpath);
    }

    string savePath;
#if UNITY_EDITOR
    savePath = folderpath;
#else
    savePath = m_screenshotFolder + "/";
#endif                 
    ScreenCapture.CaptureScreenshot(savePath + "/" + filename);
  }

  public void TakeScreenShot(GameObject hideObj) {
    StartCoroutine(routine());
    IEnumerator routine() {
      hideObj.SetActive(false);
      yield return null;

      TakeScreenShot();
      yield return null;

      hideObj.SetActive(true);
    }
  }

  private string DateStr() {
    string date = System.DateTime.Now.ToString();
    date = date.Replace("/", "-");
    date = date.Replace(" ", "_");
    date = date.Replace(":", "-");
    return date + ".png";
  }

  public void RemoveFile(string filename) {
    string folderpath = Path.Combine(Application.persistentDataPath, m_screenshotFolder);
    if (!Directory.Exists(folderpath)) { Directory.CreateDirectory(folderpath); }
    File.Delete(Path.Combine(folderpath, filename));
  }

  public Sprite[] LoadScreenShots() {
    string folderpath = Path.Combine(Application.persistentDataPath, m_screenshotFolder);
    if (!Directory.Exists(folderpath)) { Directory.CreateDirectory(folderpath); }
    loadedFiles.Clear();
    List<Sprite> sprites = new List<Sprite>();
    foreach (string file in Directory.GetFiles(folderpath)) {
      Debug.Log("loadfile=" + file);
      if (!file.Contains("png"))
        continue;
      Texture2D tex = LoadTextureFromFile(file);
      sprites.Add(SpriteFromTexture2D(tex));
      loadedFiles.Add(file);
    }
    return sprites.ToArray();
  }

  private Texture2D LoadTextureFromFile(string filename) {
    byte[] bytes;
    bytes = File.ReadAllBytes(filename);
    Texture2D load_texture = new Texture2D(1, 1);
    load_texture.LoadImage(bytes);
    return load_texture;
  }

  private Sprite SpriteFromTexture2D(Texture2D texture) {
    return Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
  }
}
