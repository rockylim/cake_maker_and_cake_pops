﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CakeMake;

public class CBShape : MonoBehaviour
{
    public Sprite[] sprites;
    public Image[] shapes;
    public Image baked, baked2;

    private void OnEnable()
    {
        foreach(Image image in shapes)
        {
            string favour = "_" + DataManager.CurrentFavour.ToString().ToLower();
            string shape = "_" + image.name.ToLower();
            image.sprite = Utils.SearchSprite(sprites, new string[] { favour, shape });
        }
    }

    public void SelectShape(int mode)
    {
        DataManager.CurrentShape = (CakeShape)mode;
        string favour = "_" + DataManager.CurrentFavour.ToString().ToLower();
        string shape = "_" + DataManager.CurrentShape.ToString().ToLower();
        baked.sprite = Utils.SearchSprite(sprites, new string[] { favour, shape });
        baked2.sprite = Utils.SearchSprite(sprites, new string[] { favour, shape });
    }
}