﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CPManager : BaseManager {
  public UnityEvent[] favourSelected = null;
  public GameObject buttonStore;
  public RectTransform buttonUndo;

  // Start is called before the first frame update
  void Start() {

    if (Constants.IS_PAID) {
      buttonStore.SetActive(false);
      buttonUndo.anchoredPosition = new Vector2(10, buttonUndo.anchoredPosition.y);
    }
  }

  void Update() {
    //ClickObject();
  }


  public void SelectFavour(int mode) {
    DataManager.CurrentFavour = (FavourMode)mode;
    favourSelected[mode].Invoke();
  }

  private GameObject selected = null;
  public void ClickObject() {
    if (Input.GetMouseButtonUp(0)) {
      selected = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
      if (selected != null) {
        Button button = selected.GetComponent<Button>();
        AudioManager.Instance.PlayOneShot("click");
      }
    }
  }
}