﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Dialog : MonoBehaviour
{
    public DOTweenAnimation tweenAnim = null;

    // Use this for initialization
    void Start()
    {
        if (tweenAnim == null)
            tweenAnim = GetComponent<DOTweenAnimation>();
    }

    private void OnEnable(){

    }

    private void OnDisable(){
        RemoveEventListeners();
    }

    private void OnDestroy(){
        RemoveEventListeners();
    }

    public virtual void Open()
    {
        Opening();
        tweenAnim.DORestart();
    }

    private void Opening()
    {
        gameObject.SetActive(true);
        transform.localScale = Vector3.one;
        AddEventListeners();
    }

    public virtual void Close()
    {
        RemoveEventListeners();
        tweenAnim.DOPlayBackwards();
        if(this.gameObject.activeInHierarchy)
            StartCoroutine(Closed());
    }

    private IEnumerator Closed()
    {
        yield return new WaitForSeconds(tweenAnim.delay + tweenAnim.duration);
        transform.localScale = Vector3.zero;
        gameObject.SetActive(false);
    }

    public virtual void AddEventListeners()
    {
    }

    public virtual void RemoveEventListeners()
    {
    }

    public bool IsOpened()
    {
        return transform.localScale.x == 1;
    }
}
