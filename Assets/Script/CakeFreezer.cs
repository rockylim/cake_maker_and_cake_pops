﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CakeFreezer : MonoBehaviour {
  public Sprite[] itemSprites;
  public GameObject prefabItem;
  public Transform grid;
  public Transform frontView;
  public GameObject buttonRemove;
  private BaseManager baseManager;

  private void OnEnable() {
    ResetItemPanel();
  }

  public void LoadSprites() {
    baseManager = FindObjectOfType<BaseManager>();
    if (baseManager != null)
      itemSprites = baseManager.LoadScreenShots();
  }

  private bool isCreating = false;
  public void ResetItemPanel() {
    if (isCreating)
      return;
    LoadSprites();
    CakeMake.Utils.removeAllChildren(grid);
    if (itemSprites == null || itemSprites.Length == 0)
      return;

    StartCoroutine(routine());
    IEnumerator routine() {
      isCreating = true;

      yield return null;

      for (int i = 0; i < itemSprites.Length; i++) {
        Sprite sprite = itemSprites[i];
        GameObject go = Instantiate(prefabItem, grid);
        go.transform.SetParent(grid);
        Image image = go.GetComponent<Image>();
        image.sprite = sprite;
        yield return null;

        go.transform.localScale = Vector3.one;
        Button button = go.GetComponent<Button>();
        string loadedFile = BaseManager.loadedFiles[i];
        button.onClick.AddListener(delegate {
          SelectShot(go.transform, loadedFile);
        });
        //button.onClick.AddListener(delegate {
        //    if (AudioManager.Instance != null)
        //        AudioManager.Instance.PlayOneShot("click");
        //});
        //button.onClick.AddListener(DisableItemPanel);
      }
      yield return null;

      isCreating = false;
    }
  }

  private string selectedFile = "";
  private Vector3 prevPos;
  private Vector2 prevSize;
  public void SelectShot(Transform view, string filename) {
    if (!string.IsNullOrEmpty(selectedFile))
      return;
    selectedFile = filename;
    prevPos = view.transform.position;
    RectTransform parentCanvas = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
    Vector2 canvasSize = parentCanvas.sizeDelta;
    Vector2 canvasPos = parentCanvas.position;

    frontView.gameObject.SetActive(true);
    frontView.GetComponent<Image>().sprite = view.GetComponent<Image>().sprite;

    frontView.position = view.transform.position;
    frontView.DOMove(new Vector3(canvasPos.x, canvasPos.y, 0f), 1f).OnComplete(delegate { buttonRemove.SetActive(true); });
    frontView.DORestart();

    prevSize = frontView.GetComponent<RectTransform>().sizeDelta;
    StartCoroutine(ResizeRoutine(frontView, canvasSize));
  }

  public void DeselectShot() {
    buttonRemove.SetActive(false);
    Tweener tween = frontView.transform.DOMove(prevPos, 1f);
    frontView.transform.DORestart();
    tween.OnComplete(delegate {
      frontView.gameObject.SetActive(false);
      selectedFile = "";
    });
    StartCoroutine(ResizeRoutine(frontView, prevSize));
  }

  IEnumerator ResizeRoutine(Transform view, Vector2 size) {
    RectTransform rect = view.GetComponent<RectTransform>();
    float t = 0.5f;
    while (t > 0f) {
      yield return null;
      rect.sizeDelta = Vector2.Lerp(rect.sizeDelta, size, Time.deltaTime * 10f);
      t -= Time.deltaTime;
    }
    rect.sizeDelta = size;
    //while (true)
    //{
    //    yield return null;
    //    rect.sizeDelta = rect.position;
    //}
  }

  public void RemoveShot() {
    BaseManager manager = FindObjectOfType<BaseManager>();
    manager.RemoveFile(selectedFile);
    ResetItemPanel();
  }
}