﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPContentManager : BaseContentManager
{
    public Transform pToppings, pIcings;

    public void CreateTopping(Image content)
    {
        CreateContent(content, pToppings, 1f, false, 150f, 150f);
    }

    public void CreateIcing(Image content)
    {
        CreateContent(content, pIcings, 1f, false, 500f, 500f);
    }
}