﻿using System.Collections;
using System.Collections.Generic;
using CakeMake;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CBManager : BaseManager {
  public Sprite[] sprites;
  public Image premix, premix2, batter, cakepan, cakebaked;
  public ParticleSystemRenderer premixFX;
  public GameObject buttonStore;
  public RectTransform buttonUndo;

  void Start() {
    if (Constants.IS_PAID) {
      buttonStore.SetActive(false);
      buttonUndo.anchoredPosition = new Vector2(10, buttonUndo.anchoredPosition.y);
    }
  }

  public void SelectFavour(int mode) {
    DataManager.CurrentFavour = (FavourMode)mode;
    //favourSelected[mode].Invoke();
    string favour = "_" + DataManager.CurrentFavour.ToString().ToLower();
    Debug.Log("selected favour = " + favour);
    premix.sprite = Utils.SearchSprite(sprites, new string[] { "cakemix1", favour });
    premix2.sprite = Utils.SearchSprite(sprites, new string[] { "cakemix1", favour });
    batter.sprite = Utils.SearchSprite(sprites, new string[] { "cakemix2", favour });
    cakepan.sprite = Utils.SearchSprite(sprites, new string[] { "bakedcake_", favour });
    cakebaked.sprite = Utils.SearchSprite(sprites, new string[] { "bakedcake2", favour });

    premixFX.material.mainTexture = premix.sprite.texture;
  }

  //public CakeShape CurrentShape;
  //public Image CakeShape;
  //public Image CakeShapeResult;
  //public Sprite[] CakeSprites;

  //public void SelectShape(int shape)
  //{
  //    CurrentShape = (CakeShape)shape;
  //    //Sprite sprite = GetSprite(DataManager.CurrentFavour, CurrentShape);
  //    Sprite sprite = Utils.GetSprite(CakeSprites, DataManager.CurrentFavour, CurrentShape);
  //    CakeShape.sprite = sprite;
  //    CakeShapeResult.sprite = sprite;
  //}

  ////private Sprite GetSprite(FavourMode favour, CakeShape cakeShape)
  ////{
  ////    foreach (Sprite sprite in CakeSprites)
  ////    {
  ////        string s1 = sprite.name.ToLower();
  ////        string s2 = favour.ToString().ToLower();
  ////        string s3 = cakeShape.ToString().ToLower();
  ////        if (!s1.Contains(s2))
  ////            continue;
  ////        if (!s1.Contains(s3))
  ////            continue;
  ////        return sprite;
  ////    }
  ////    return null;
  ////}
}