﻿using Caviezel.Pretend;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CPMixer : EventTrigger {
  public PtdParticleController[] m_particles;
  public Image fill;
  public Image butterMix;
  public Sprite[] sprites;
  private bool dragging;
  private DOTweenAnimation tween = null;
  private AudioSource audMixer = null;
  CanvasGroup[] m_particleCanvas;
  int m_canvasAnimMode;
  bool m_isDone;
  private void Start() {
    CPBowl.Instance.PutInBowl(butterMix.transform);
    butterMix.gameObject.SetActive(true);
    audMixer = GetComponent<AudioSource>();
    audMixer.Play();
    m_particleCanvas = new CanvasGroup[2];
    for (int i = 0; i < m_particles.Length; i++) {
      m_particleCanvas[i] = m_particles[i].GetComponent<CanvasGroup>();
      m_particles[i].InitParticle();
    }
  }

  public void Update() {
    for (int i = 0; i < m_particles.Length; i++) {
      m_particles[i].UpdateComponent(Time.deltaTime);
    }

    if (dragging) {
      if ((m_particles[0].IsActive == false) && (!m_isDone)) {
        for (int i = 0; i < m_particles.Length; i++) {
          m_particles[i].OnParticleStart();
          m_particles[i].IsActive = true;
        }
      }
      m_canvasAnimMode = 1;
      transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, -90f)), Time.deltaTime * 5f);
      if (!audMixer.isPlaying)
        audMixer.Play();
    } else {
      m_canvasAnimMode = -1;
      transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 5f);
      if (audMixer.isPlaying)
        audMixer.Stop();
    }

    if (!m_isDone) {
      if (m_canvasAnimMode == 1) {
        m_particleCanvas[0].alpha = m_particleCanvas[0].alpha + 1f * Time.deltaTime;
        if (m_particleCanvas[0].alpha >= 1) {
          m_particleCanvas[0].alpha = 1;
          m_canvasAnimMode = 0;
        }
      } else if (m_canvasAnimMode == -1) {
        m_particleCanvas[0].alpha = m_particleCanvas[0].alpha - 5f * Time.deltaTime;
        if (m_particleCanvas[0].alpha <= 0) {
          m_particleCanvas[0].alpha = 0;
          m_canvasAnimMode = 0;
        }
      }
      m_particleCanvas[1].alpha = m_particleCanvas[0].alpha;
    }

    if (Mathf.Abs(270 - transform.eulerAngles.z) < 3f) {
      float duration = audMixer.clip.length;
      duration = 5f;  //mixer clip has empty sound at the end

      fill.fillAmount += Time.deltaTime / duration;
      butterMix.color = new Color(1f, 1f, 1f, fill.fillAmount);
      Mixing();
    }

    if (fill.fillAmount == 1f && tween == null) {
      tween = GetComponent<DOTweenAnimation>();
      tween.DOPlay();
      // done
      for (int i = 0; i < m_particles.Length; i++) {
        m_particleCanvas[i].alpha = 0;
        m_particles[i].IsActive = false;
        m_particles[i].gameObject.SetActive(false);
      }

      m_canvasAnimMode = 0;
      m_isDone = true;
    }

  }

  private float time = 0f;
  public void Mixing() {
    if (Time.time - time < Time.deltaTime * 1.5f)
      return;
    time = Time.time;
    Image image = GetComponent<Image>();
    foreach (Sprite sp in sprites) {
      if (image.sprite == sp)
        continue;
      image.sprite = sp;
      break;
    }
  }

  public override void OnPointerDown(PointerEventData eventData) {
    base.OnPointerDown(eventData);
    dragging = true;
  }

  public override void OnPointerUp(PointerEventData eventData) {
    base.OnPointerUp(eventData);
    dragging = false;
  }
}
