﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseContentManager : MonoBehaviour
{
    public GameObject prefabContent;
    public List<GameObject> ContentList = new List<GameObject>();

    public void DeleteContents()
    {
        foreach (GameObject content in ContentList)
            Destroy(content);
        ContentList.Clear();
        AudioManager.Instance.PlayOneShot("delete x");
    }

    public void UndoContent()
    {
        if (ContentList.Count < 1)
            return;
        GameObject content = ContentList[ContentList.Count - 1];
        ContentList.Remove(content);
        Destroy(content);
        AudioManager.Instance.PlayOneShot("undo");
    }

    public void FixContents()
    {
        if (ContentList.Count < 1)
            return;
        foreach (GameObject content in ContentList)
        {
            content.GetComponent<CMContent>().moveContent = false;
            //Destroy(content.GetComponent<Button>());
        }
    }

    public void CreateContent(Image content, Transform parent, float scale = 1f, bool native = false, float width = 0f, float height = 0f)
    {
        GameObject go = Instantiate(prefabContent, parent);
        go.GetComponent<CMContent>().ChangeContent(content, scale, native, width, height);
        ContentList.Add(go);
    }

    //public void CreateContent(Image content, Transform parent)
    //{
    //    GameObject go = Instantiate(prefabContent, parent);
    //    go.GetComponent<CMContent>().ChangeContent(content);
    //    ContentList.Add(go);
    //}
}