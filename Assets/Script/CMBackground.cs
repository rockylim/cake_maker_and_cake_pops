﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CMBackground : CMContent
{
    public bool nativesize = false;

    public void ChangeContent(Image image)
    {
        base.ChangeContent(image, nativesize);
    }
}