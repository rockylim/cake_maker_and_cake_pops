﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CakeMake
{
    public class Utils : MonoBehaviour
    {
        public static Utils instance = null;

        // Start is called before the first frame update
        void Start()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(this.gameObject);

            DontDestroyOnLoad(this);
        }

        public static void removeAllChildren(Transform tr)
        {
            if (tr == null || tr.childCount < 1)
                return;
            Transform[] children = tr.GetComponentsInChildren<Transform>();
            for (int i = 0; i < children.Length; i++)
                if (children[i] != tr && children[i] != null)
                    UnityEngine.Object.DestroyImmediate(children[i].gameObject);
        }

        public static Sprite GetSprite(Sprite[] sprites, FavourMode favour, CakeShape cakeShape = CakeShape.NONE)
        {
            foreach (Sprite sprite in sprites)
            {
                string s1 = sprite.name.ToLower();
                string s2 = favour.ToString().ToLower();
                string s3 = cakeShape.ToString().ToLower();
                if (!s1.Contains(s2))
                    continue;
                if (!s1.Contains(s3))
                    continue;
                return sprite;
            }
            return null;
        }

        public static Sprite SearchSprite(Sprite[] sprites, string[] keys)
        {
            foreach(Sprite sprite in sprites)
            {
                string s = sprite.name.ToLower();
                int i = 0;
                for(; i < keys.Length; i++)
                {
                    if (!s.Contains(keys[i].ToLower()))
                        break;
                }
                if (i == keys.Length)
                    return sprite;
            }
            return null;
        }

        public static void AutoClickButton(GameObject go)
        {
            ExecuteEvents.Execute(go.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
        }
    }
}