﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMContentManager : BaseContentManager {
  public Transform pCakes, pTrims, pCandles, pDecoders, pFlowers, pHoliday, pToppers;
  public Transform pDonuts, pCreams, pLetters, pJellys, pLollipops;

  public void CreateCake(Image content) {
    CreateContent(content, pCakes, 1f, true);
  }

  public void CreateTrim(Image content) {
    if ((content.sprite.name == "trim2") || (content.sprite.name == "trim3") || (content.sprite.name == "trim4")) {
      CreateContent(content, pTrims, 3.5f);
      return;
    }
    CreateContent(content, pTrims, 0.7f);

  }

  public void CreateCandle(Image content) {
    CreateContent(content, pCandles, 0.3f, true);
  }

  public void CreateDecoder(Image content) {
    CreateContent(content, pDecoders, 0.2f, true);
  }

  public void CreateFlower(Image content) {
    CreateContent(content, pFlowers, 1f, false, 128f, 128f);
  }

  public void CreateHoliday(Image content) {
    if (content.sprite.name.Contains("("))
      CreateContent(content, pHoliday, 0.2f, true);
    else
      CreateContent(content, pHoliday);
  }

  public void CreateTopper(Image content) {
    CreateContent(content, pToppers, 0.7f, true);
  }

  public void CreateDonut(Image content) {
    CreateContent(content, pDonuts);
  }

  public void CreateCream(Image content) {
    CreateContent(content, pCreams);
  }

  public void CreateLetter(Image content) {
    CreateContent(content, pLetters);
  }

  public void CreateJelly(Image content) {
    CreateContent(content, pJellys, 1f, false, 100f, 50f);
  }

  public void CreateLollipop(Image content) {
    CreateContent(content, pLollipops, 0.4f, true);
  }
}