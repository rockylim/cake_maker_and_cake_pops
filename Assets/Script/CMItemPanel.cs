﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class CMItemPanel : MonoBehaviour {
  public Sprite[] itemSprites;
  public int lockLevel = 0;
  public GameObject prefabItem;
  public Transform grid;
  public GameObject MessageReceiver;
  public string MessageFunc;

  DecorVoiceoverController m_voiceoverController;

  private bool isPurchased {
    get {
      return (DataManager.CurrentGame == GameMode.CAKEMAKE && DataManager.PurchasedCakeMake) ||
              (DataManager.CurrentGame == GameMode.CAKEPOP && DataManager.PurchasedCakePop) ||
              (DataManager.CurrentGame == GameMode.CAKEBAKE && DataManager.PurchasedCakeBake) || Constants.IS_PAID;
    }
  }
  private CodelessIAPButton iAPButton;

  private void Start() {
    m_voiceoverController = this.transform.parent.Find("DecorVoiceoverController").GetComponent<DecorVoiceoverController>();
  }

  private void OnEnable() {
#if UNITY_EDITOR
    lockLevel = 0;
#endif
    ResetItemPanel();
  }

  private bool isCreating = false;
  public void ResetItemPanel() {
    Debug.Log("xxxx ResetItemPanel");
    Debug.Log("xxxx " + isPurchased);
    Debug.Log("xxxx " + DataManager.PurchasedCakeMake);
    Debug.Log("xxxx " + DataManager.PurchasedCakeBake);
    Debug.Log("xxxx " + DataManager.PurchasedCakePop);
    Debug.Log("xxxx " + DataManager.Purchased3Games);

    if (isCreating || lockLevel == itemSprites.Length)
      return;
    lockLevel = isPurchased ? itemSprites.Length : lockLevel;

    StartCoroutine(routine());
    IEnumerator routine() {
      isCreating = true;
      CakeMake.Utils.removeAllChildren(grid);
      yield return null;

      for (int i = 0; i < itemSprites.Length; i++) {
        Sprite sprite = itemSprites[i];
        GameObject go = Instantiate(prefabItem, grid);
        go.transform.SetParent(grid);
        Image image = go.transform.GetChild(0).GetComponent<Image>();
        image.sprite = sprite;
        yield return null;

        go.transform.localScale = Vector3.one;
        Button button = go.GetComponent<Button>();
        button.onClick.AddListener(delegate { ApplyContent(image); });
        button.onClick.AddListener(delegate {
          if (AudioManager.Instance != null) {
            AudioManager.Instance.PlayOneShot("click");
            if (m_voiceoverController != null) {
              m_voiceoverController.PlaySfx();
            }
          }

        });
        button.onClick.AddListener(DisableItemPanel);

        if (i < lockLevel || isPurchased || lockLevel == 0) {
          go.transform.GetChild(1).gameObject.SetActive(false);
        } else {
          go.transform.GetChild(1).gameObject.SetActive(true);
          button = go.transform.GetChild(1).GetComponent<Button>();
          button.onClick.AddListener(delegate {
            AudioManager.Instance.PlayOneShot("click");
            BuyProductItem();
          });

          //IAPButton iAPButton = go.transform.GetChild(1).GetComponent<IAPButton>();
          //if (iAPButton == null)
          //{
          //    iAPButton = go.transform.GetChild(1).gameObject.AddComponent<IAPButton>();
          //    iAPButton.onPurchaseComplete = new IAPButton.OnPurchaseCompletedEvent();
          //}
          //switch (DataManager.CurrentGame)
          //{
          //    case GameMode.CAKEMAKE:
          //        iAPButton.productId = DataManager.PRODUCT_CAKEMAKE;
          //        break;
          //    case GameMode.CAKEPOP:
          //        iAPButton.productId = DataManager.PRODUCT_CAKEPOP;
          //        break;
          //    case GameMode.CAKEBAKE:
          //        iAPButton.productId = DataManager.PRODUCT_CAKEBAKE;
          //        break;
          //    default:
          //        break;
          //}
          //iAPButton.onPurchaseComplete.AddListener(DataManager.Instance.PurchasedNonConsumable);
          //iAPButton.onPurchaseComplete.AddListener(delegate { Purcahsed(); });
        }
      }
      yield return null;

      isCreating = false;
    }
  }

  public void Purcahsed() {
    if (lockLevel == itemSprites.Length)
      return;
    Debug.Log("CMItemPanel:Purcahsed" + lockLevel + ":" + itemSprites.Length);

    StartCoroutine(routine());
    IEnumerator routine() {
      yield return new WaitForSeconds(0.5f);

      ResetItemPanel();
    }
  }

  private void BuyProductItem() {
    switch (DataManager.CurrentGame) {
      case GameMode.CAKEMAKE:
        IAPManager.Instance.BuyUnlockCakeMake();
        break;
      case GameMode.CAKEPOP:
        IAPManager.Instance.BuyUnlockCakePop();
        break;
      case GameMode.CAKEBAKE:
        IAPManager.Instance.BuyUnlockCakeBake();
        break;
      default:
        break;
    }
  }


  public void LockLevels(int locked) {
    StartCoroutine(routine());
    IEnumerator routine() {
      yield return null;

      for (int i = 0; i < grid.childCount; i++) {
        if (i < locked)
          grid.GetChild(i).GetChild(1).gameObject.SetActive(false);
        else
          grid.GetChild(i).GetChild(1).gameObject.SetActive(true);
      }
      yield return null;
    }
  }

  public void ApplyContent(Image sprite) {
    if (MessageReceiver == null || string.IsNullOrEmpty(MessageFunc))
      return;
    MessageReceiver.SetActive(true);
    MessageReceiver.SendMessage(MessageFunc, sprite);
  }

  public void DisableItemPanel() {
    //if (isCreating)
    //    return;
    CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
    if (canvasGroup != null) {
      StartCoroutine(routine());
      IEnumerator routine() {
        while (canvasGroup.alpha > 0f) {
          canvasGroup.alpha -= Time.deltaTime * 5f;
          yield return null;
        }
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;

        while (isCreating)
          yield return null;
        gameObject.SetActive(false);
      }
    } else {
      gameObject.SetActive(false);
    }
  }

  public void EnableItemPanel() {
    gameObject.SetActive(true);
    CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
    if (canvasGroup != null) {
      StartCoroutine(routine());
      IEnumerator routine() {
        yield return null;

        while (canvasGroup.alpha < 1f) {
          canvasGroup.alpha += Time.deltaTime * 5f;
          yield return null;
        }
        canvasGroup.alpha = 1f;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
      }
    }
  }

}
