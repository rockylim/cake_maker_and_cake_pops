﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RatePopup : BaseManager {
  public static void ShowRate() {
    SceneManager.LoadSceneAsync("51Rate", LoadSceneMode.Additive);
  }

  public void RateApop() {
#if UNITY_ANDROID
    if (Constants.IS_AMAZON) {
        Application.OpenURL("https://www.amazon.com/Cake-Maker-Pops-Dessert-Kitchen/dp/B07BTD8TL8");
    } else {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.detentionapps.cakemakepopbake");
    }
#elif UNITY_IPHONE
    Application.OpenURL("https://apps.apple.com/us/app/cake-maker-cake-pops-cooking/id1364617590");
#endif
  }

  public void CloseRate() {
    SceneManager.UnloadSceneAsync("51Rate");
  }
}