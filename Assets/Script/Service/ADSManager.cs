﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class ADSManager : Singleton<ADSManager> {//, IUnityAdsListener {
  private string gameid = "3530894";
  private string placementVideo = "video";
  private string placementRewarded = "rewardedVideo";
  private string placementBanner = "banner";
  //private bool testmode = true;

  // Start is called before the first frame update
  void Start() {
    //Advertisement.AddListener(this);
#if UNITY_EDITOR
    //Advertisement.Initialize(gameid, true);     //testmode = true
#else
      //  Advertisement.Initialize(gameid, false);    //testmode = false
#endif
    //LoadBanner();
  }

  public void ShowVideo() {
    //if (Advertisement.isShowing)
    //  return;
    //if (Advertisement.IsReady(placementVideo))
    //  Advertisement.Show(placementVideo);
  }

  public void ShowRewardedVideo() {
    //if (Advertisement.isShowing)
    // return;
    //if (Advertisement.IsReady(placementRewarded))
    // Advertisement.Show(placementRewarded);
  }
  /*
    public void LoadBanner() {
  #if UNITY_ANDROID
      if (!Constants.IS_AMAZON) { return; }
  #endif
      BannerLoadOptions options = new BannerLoadOptions { loadCallback = OnLoadBannerSuccess, errorCallback = OnLoadBannerFail };
      Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
      Advertisement.Banner.Load(placementBanner, options);
    }

    private void OnLoadBannerSuccess() {
      Debug.Log("OnLoadBannerSuccess");
      BannerOptions options = new BannerOptions { showCallback = OnShowBanner, hideCallback = OnHideBanner };
      Advertisement.Banner.Show(placementBanner, options);
    }

    private void OnLoadBannerFail(string message) {
      Debug.LogError("OnLoadBannerFail reason = " + message);
      //retry to show banner maybe if the reason was that the placement was not ready
    }

    private void OnShowBanner() {
  #if UNITY_ANDROID
      if (!Constants.IS_AMAZON) { return; }
  #endif
      Debug.Log("OnShowBanner");
    }
    private void OnHideBanner() {
  #if UNITY_ANDROID
      if (!Constants.IS_AMAZON) { return; }
  #endif
      Debug.Log("OnHideBanner");
    }


    public void HideBanner() {
  #if UNITY_ANDROID
      if (!Constants.IS_AMAZON) { return; }
  #endif
      if (!Advertisement.Banner.isLoaded)
        return;
      Advertisement.Banner.Hide();
    }
  */
  //public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) {
  //    if (placementId == placementRewarded && showResult == ShowResult.Finished) {
  //}
  //}

  public void OnUnityAdsReady(string placementId) {
    Debug.Log("ready placement = " + placementId);
    //if(placementId == placementBanner)
    //{
    //    Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
    //    Advertisement.Banner.Show(placementBanner);
    //    Debug.Log("Show Banner on Top Center");
    //}
  }

  public void OnUnityAdsDidError(string message) {
    Debug.Log("OnUnityAdsDidError = " + message);
  }

  public void OnUnityAdsDidStart(string placementId) {
    Debug.Log("OnUnityAdsDidStart = " + placementId);
  }
}
