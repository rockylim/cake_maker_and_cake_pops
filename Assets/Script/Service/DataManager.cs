﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public enum GameMode {
  NONE = 0,
  CAKEMAKE = 1,
  CAKEPOP = 2,
  CAKEBAKE = 3
}

public enum FavourMode {
  NONE = 0,
  Chocolate = 1,
  Confetti = 2,
  GreenMint = 3,
  Marble = 4,
  MintChocolate = 5,
  Orange = 6,
  RedVelvet = 7,
  Vanilla = 8,
  Strawberry = 10,
}

public enum CakeShape {
  NONE = 0,
  CIRCLE = 1,
  FLOWER = 2,
  HEART = 3,
  SQUARE = 4,
  STAR = 5
}

public class DataManager : Singleton<DataManager> {
  public static string PRODUCT_NO_ADS = "no_ads";                  //1.99
  public static string PRODUCT_3GAMES = "unlock_3games";           //9.99
  public static string PRODUCT_CAKEMAKE = "unlock_cakemake";       //3.99
  public static string PRODUCT_CAKEPOP = "unlock_cakepop";         //3.99
  public static string PRODUCT_CAKEBAKE = "unlock_cakebake";       //3.99

  public static string STORE_NO_ADS = "Remove ALL Ads! NO Ads Forever!";
  public static string STORE_3GAMES = "";
  public static string STORE_CAKEMAKE = "Candy, Donuts, Cakes, Candles, Toppers, more!";
  public static string STORE_CAKEPOP = "Fun faces, Sticks, Sprinkles, Frosting!";
  public static string STORE_CAKEBAKE = "Gummies, Chocolate, Holiday Fun, Frosting!";

  private int goldAmount = 0;

  public static GameMode CurrentGame = GameMode.NONE;
  public static FavourMode CurrentFavour = FavourMode.NONE;
  public static CakeShape CurrentShape = CakeShape.NONE;

  public static bool NoAds {
    get {
      return PlayerPrefs.GetInt(PRODUCT_NO_ADS, 0) == 1 ? true : false || Constants.IS_PAID;
    }
    set {
      PlayerPrefs.SetInt(PRODUCT_NO_ADS, value == true ? 1 : 0);
    }
  }

  public static bool Purchased3Games { get { return PlayerPrefs.GetInt(PRODUCT_3GAMES, 0) == 1 ? true : false; } }
  public static bool PurchasedCakeMake { get { return PlayerPrefs.GetInt(PRODUCT_CAKEMAKE, 0) == 1 ? true : false; } }
  public static bool PurchasedCakePop { get { return PlayerPrefs.GetInt(PRODUCT_CAKEPOP, 0) == 1 ? true : false; } }
  public static bool PurchasedCakeBake { get { return PlayerPrefs.GetInt(PRODUCT_CAKEBAKE, 0) == 1 ? true : false; } }

  public void Start() {
#if UNITY_EDITOR
    PlayerPrefs.SetInt(PRODUCT_NO_ADS, 0);
    PlayerPrefs.SetInt(PRODUCT_3GAMES, 0);
    PlayerPrefs.SetInt(PRODUCT_CAKEMAKE, 0);
    PlayerPrefs.SetInt(PRODUCT_CAKEPOP, 0);
    PlayerPrefs.SetInt(PRODUCT_CAKEBAKE, 0);
#endif

    if (Constants.IS_PAID) {
      PlayerPrefs.SetInt(PRODUCT_NO_ADS, 0);
      PlayerPrefs.SetInt(PRODUCT_3GAMES, 0);
      PlayerPrefs.SetInt(PRODUCT_CAKEMAKE, 0);
      PlayerPrefs.SetInt(PRODUCT_CAKEPOP, 0);
      PlayerPrefs.SetInt(PRODUCT_CAKEBAKE, 0);
    }
  }

  public void AddGold(int amount) {
    goldAmount += amount;
  }

  public void RemoveAds() {
    Debug.Log("No Ads !!!");
    NoAds = true;
  }

  public void PurchasedNonConsumable(Product p) {
    string productId = p.definition.id;
    if (PlayerPrefs.GetInt(productId, 0) == 1)
      return;

    Debug.Log(productId);
    PlayerPrefs.SetInt(productId, 1);
    PlayerPrefs.SetInt(PRODUCT_NO_ADS, 1);       //no ads for all purchased product
    if (productId.Equals(PRODUCT_3GAMES)) {
      PlayerPrefs.SetInt(PRODUCT_CAKEMAKE, 1);
      PlayerPrefs.SetInt(PRODUCT_CAKEPOP, 1);
      PlayerPrefs.SetInt(PRODUCT_CAKEBAKE, 1);
    }
    if (NoAds == true)
      ServicesManager.Instance.HideBanner();
    CMItemPanel panel = FindObjectOfType<CMItemPanel>();
    if (panel != null)
      panel.Purcahsed();
  }

}