﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplovinManager : Singleton<ApplovinManager> {
  // Put AppLovin SDK Key here or in your AndroidManifest.xml / Info.plist
  private const string SDK_KEY = "kyNwmH7leQ_s8k-gI4fiBNUxQNCFMf95bW_Lp75SX4IgoHL5L_pXNHSpff20JB5vi5FWCKzlPGPCXmHum4P8Mt";

  // Controlled State
  private bool IsPreloadingRewardedVideo = false;

  void Start() {
    // Check if user replaced the SDK key
    if ("YOUR_SDK_KEY_HERE".Equals(SDK_KEY)) {
      Log("ERROR: PLEASE UPDATE YOUR SDK KEY IN Assets/ApplovinManager.cs");
    } else {
      // Set SDK key and initialize SDK
      //AppLovin.SetSdkKey(SDK_KEY);
      //AppLovin.InitializeSdk();
      //AppLovin.SetUnityAdListener("MainMenu");
      //AppLovin.SetRewardedVideoUsername("demo_user");
    }
  }

  public void ShowInterstitial() {
    Log("Showing interstitial ad");

    // Optional: You can call `AppLovin.PreloadInterstitial()` and listen to the "LOADED" event to preload the ad from the network before showing it
    //AppLovin.ShowInterstitial();
  }

  public void PreloadOrShowRewardedInterstitial() {
    /*
    if (AppLovin.IsIncentInterstitialReady()) {

      Log("Showing rewarded ad...");

      IsPreloadingRewardedVideo = false;

      AppLovin.ShowRewardedInterstitial();
    } else {

      Log("Preloading rewarded ad...");

      IsPreloadingRewardedVideo = true;

      AppLovin.LoadRewardedInterstitial();
    }
    */
  }

  public void ShowBanner() {
    Log("Showing banner ad");
    //AppLovin.ShowAd(AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
  }

  private void onAppLovinEventReceived(string ev) {
    // Log AppLovin event
    Log(ev);

    //
    // Special Handling for Rewarded events
    //

    if (ev.Contains("REWARD")) {
      if (ev.Equals("REWARDAPPROVEDINFO")) {
        // Process an event like REWARDAPPROVEDINFO:100:Credits
        char[] delimiter = { '|' };
        string[] split = ev.Split(delimiter);

        // Pull out the amount of virtual currency.
        double amount = double.Parse(split[1]);

        // Pull out the name of the virtual currency
        string currencyName = split[2];

        // Do something with this info - for example, grant coins to the user
        // myFunctionToUpdateBalance(currencyName, amount);

        Log("Rewarded " + amount + " " + currencyName);
      }
    }
    // Check if this is a Rewarded Video preloading event
    else if (IsPreloadingRewardedVideo && (ev.Equals("LOADED") || ev.Equals("LOADFAILED"))) {

      IsPreloadingRewardedVideo = false;

    }
  }

  private void Log(string message) {
    Debug.Log(message);
  }
}
