﻿using System.Collections;
using System.Collections.Generic;
//#if ADS_ADMOB
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using GoogleMobileAds.Ump.Api;
//#endif
//using KidozSDK;
using UnityEngine;
using UnityEngine.Advertisements;
//#endif
//#if ADS_UNITY

public class ServicesManager : Singleton<ServicesManager> {

  //public static ServicesManager instance { get; set; }
  public bool m_admobGDPRActive = true;
  public AdmobTestConfig m_admobTest;

  [HideInInspector] public int rewardedCoins;

  #region ADMOB
  [HideInInspector] public bool enableTestMode;
  [HideInInspector] public string appID;
  [HideInInspector] public string bannerID;
  //#if ADS_ADMOB
  [HideInInspector] public AdPosition bannerPosition;
  //#endif
  [HideInInspector] public string interstitialID;
  [HideInInspector] public string rewardedVideoAdsID;

  //#if ADS_ADMOB
  private BannerView bannerView;
  private InterstitialAd interstitial;
  //private RewardBasedVideoAd rewardVideoAd;
  //#endif
  #endregion

  #region UnityAds
  [HideInInspector] public bool testMode;
  [HideInInspector] public string gameID;
  [HideInInspector] public string bannerPlacementID;
  [HideInInspector] public string videoAdPlacementID;
  [HideInInspector] public string rewardedVideoAdPlacementID;
  bool m_unityInsterstitialReady;
  #endregion

  #region Kidoz
  public string kidozPubId;
  public string kidozSecurity;
  #endregion

  #region IAP
  [HideInInspector] public string buy10000chicks;
  [HideInInspector] public string buy100000chicks;
  [HideInInspector] public string buy1000000chicks;
  #endregion


  bool isRewardAdded;

  //private void Awake()
  //{
  //    if (instance == null)
  //        instance = this;
  //    else if (instance != this)
  //        Destroy(this.gameObject);
  //}

  // Use this for initialization
  IEnumerator Start() {
    DontDestroyOnLoad(this.gameObject);

    //FORCE INIT
#if UNITY_IOS
      appID = "ca-app-pub-2712579443424568~8031706216";
      bannerID = "ca-app-pub-2712579443424568/9867532060";
      interstitialID = "ca-app-pub-2712579443424568/3517746135";      

      gameID = "3713348";
#else
    if (!Constants.IS_AMAZON) {
      // P LAY STORE
      appID = "ca-app-pub-2712579443424568~4451553857";
      bannerID = "ca-app-pub-2712579443424568/5381492148";
      interstitialID = "ca-app-pub-2712579443424568/8606421289";
    } else {
      appID = "ca-app-pub-2712579443424568~4159462167";
      bannerID = "ca-app-pub-2712579443424568/5254768010";
      interstitialID = "ca-app-pub-2712579443424568/7665686239";
    }
    gameID = "3713349";
#endif

#if UNITY_IOS
    if (enableTestMode) {
      appID = "ca-app-pub-3940256099942544~1458002511";
      bannerID = "ca-app-pub-3940256099942544/2934735716";
      interstitialID = "ca-app-pub-3940256099942544/4411468910";
      rewardedVideoAdsID = "ca-app-pub-3940256099942544/1712485313";
    }
#else
    if (enableTestMode) {
      appID = "ca-app-pub-3940256099942544~3347511713";
      bannerID = "ca-app-pub-3940256099942544/6300978111";
      interstitialID = "ca-app-pub-3940256099942544/1033173712";
      rewardedVideoAdsID = "ca-app-pub-3940256099942544/5224354917";
    }
#endif

    kidozPubId = "14425";
    kidozSecurity = "rn5tbNGC10ty9joMRQPxr5kMEBExd6r3";
    if (!Constants.IS_PAID) {
      InitAdmobConsent();
      //InitializeUnityAds();
      //InitializeKidoz();
#if !UNITY_EDITOR
    //InitializeApplovin();
#endif
      yield return new WaitForSeconds(2f);
      ShowBanner();
    }
  }

  public void HideBanner() {
#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) { return; }
#endif
    this.bannerView.Destroy();      //Admob
                                    //    Advertisement.Banner.Hide();    //UnityAds
                                    //    AppLovin.HideAd();              //Applovin
  }

  public void ShowBanner() {
    if (Constants.IS_PAID) {
      return;
    }
#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) { return; }
#endif
    if (DataManager.NoAds) {
      HideBanner();
      return;
    }
    ShowBannerAdmob();
    //ShowBannerUnityAds();
    //ShowBannerApplovin();
  }

  public void ShowInterstitial() {
    Debug.Log("try to show interstital");
    if (DataManager.NoAds) {
      Debug.Log("not show interstital, not ads");
      return;
    }


#if UNITY_ANDROID
    if (Constants.IS_AMAZON) {
      int rand = Random.Range(1, 6);
      if (rand == 1) {
        //ShowInterstitialApplovin();
        ShowInterstitialAdmob();
      } else if (rand == 2) {
        //ShowInterstitialUnityAds();
        ShowInterstitialAdmob();
      } else if (rand == 3) {
        ShowInterstitialAdmob();
      } else if (rand == 4) {
        //ShowInterstitialKidoz();
        ShowInterstitialAdmob();
      }
    } else {
      int rand = Random.Range(1, 4);
      if (rand <= 2) {
        ShowInterstitialAdmob();
      } /*else if (rand == 2) {
        ShowInterstitialKidoz();
      }*/
    }
#else
    int rand = Random.Range(1, 6);
    if (rand == 1) {
      ShowInterstitialApplovin();
    } else if (rand == 2) {
      ShowInterstitialUnityAds();
    } else if (rand == 3) {
      ShowInterstitialAdmob();
    } else if (rand == 4) {
      ShowInterstitialKidoz();
    }
#endif          
  }

  #region Admob
  private void RequestBannerAdmob() {
#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) { return; }
#endif
    //#if ADS_ADMOB
    bannerView = new BannerView(bannerID, AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth), AdPosition.Bottom);
    bannerView.OnBannerAdLoaded += () => {
      // do something
    };
    AdRequest request = new AdRequest();

    bannerView.LoadAd(request);
    //#endif
  }
  private void RequestInterstialAdmob() {
    //#if ADS_ADMOB
    Debug.Log("******* init asdmob interst");
    // Clean up the old ad before loading a new one.
    if (this.interstitial != null) {
      this.interstitial.Destroy();
      this.interstitial = null;
    }


    //List<string> deviceIds = new List<string>();
    //deviceIds.Add("6802DCD837792ABF057BCF4764051F5F");


    /* RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
     .SetTestDeviceIds(deviceIds)        
     //.SetTagForChildDirectedTreatment(m_tagForChild ? TagForChildDirectedTreatment.True : TagForChildDirectedTreatment.Unspecified)
     .build();
     MobileAds.SetRequestConfiguration(requestConfiguration);*/
    // Create an empty ad request.        
    AdRequest request = new AdRequest();

    // send the request to load the ad.
    InterstitialAd.Load(interstitialID, request,
        (InterstitialAd ad, LoadAdError error) => {
          // if error is not null, the load request failed.
          if (error != null || ad == null) {
            Debug.LogError("interstitial ad failed to load an ad " +
                         "with error : " + error);
            return;
          }

          Debug.Log("Interstitial ad loaded with response : "
                  + ad.GetResponseInfo());

          this.interstitial = ad;
          this.interstitial.OnAdFullScreenContentClosed += () => {
            RequestInterstialAdmob();
          };
        });
    //#endif
  }
  /*private void RequestRewardedVideoAdAdmob() {
    isRewardAdded = false;

    //#if ADS_ADMOB
    this.rewardVideoAd = RewardBasedVideoAd.Instance;

    this.rewardVideoAd.OnAdRewarded += HandleRewardBasedVideoRewarded;

    AdRequest request = new AdRequest.Builder().Build();

    this.rewardVideoAd.LoadAd(request, rewardedVideoAdsID);
    //#endif
  }*/

  public void InitializeAdmob() {
    Debug.Log("******* init asdmob");
    //#if ADS_ADMOB
    MobileAds.Initialize(OnInit);

    this.RequestInterstialAdmob();
    //this.RequestRewardedVideoAdAdmob();
    //#endif
  }
  public void InitializeBannerAdmob() {
#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) { return; }
#endif
    //#if ADS_ADMOB
    MobileAds.Initialize(OnInit);
    //#endif

    //this.RequestBannerAdmob();
  }
  public void ShowBannerAdmob() {
#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) { return; }
#endif
    //#if ADS_ADMOB

    if (this.bannerView != null) {
      Debug.Log("<color=red> SHOW BANNER </color>");
      this.bannerView.Show();
    }
    //#endif
  }
  public void DestroyBannerAdmob() {
#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) { return; }
#endif
    Debug.Log("<color=blue> HIDE BANNER </color>");
    //#if ADS_ADMOB
    this.bannerView.Destroy();
    //#endif
  }
  public void ShowInterstitialAdmob() {
    //#if ADS_ADMOB
    if (this.interstitial != null && this.interstitial.CanShowAd()) {
      Debug.Log("admob Interstitial was loaded succesfully!");

      this.interstitial.Show();
    } else {
      Debug.Log("cant show interstitial");
    }
    //#endif
  }
  /*public void ShowRewardedVideoAdAdmob() {
    //#if ADS_ADMOB
    if (rewardVideoAd.IsLoaded()) {
      Debug.Log("Rewarded was loaded succesfully!");

      rewardVideoAd.Show();
    }
    //#endif
  }*/

  //#if ADS_ADMOB
  public void HandleRewardBasedVideoRewarded(object sender, Reward args) {
    string type = args.Type;
    double amount = args.Amount;
  }
  //#endif
  #endregion

  /* 
     #region UnityAds
     public void InitializeUnityAds() {
   #if UNITY_ANDROID
       if (!Constants.IS_AMAZON) { return; }
   #endif
       //Monetization.Initialize(gameID, testMode);
       Advertisement.Initialize(gameID, testMode, this);
     }


     public void ShowInterstitialUnityAds() {
   #if UNITY_ANDROID
       if (!Constants.IS_AMAZON) { return; }
   #endif
       if (m_unityInsterstitialReady) {
         Advertisement.Show(videoAdPlacementID, this);
         m_unityInsterstitialReady = false;
       }
       LoadUnityInterstitalAd();
     }

     #region IUnityAdsInitializationListener
     public void OnInitializationComplete() {
       UnityEngine.Debug.Log("unity ads : init unity ads completed");
       LoadUnityInterstitalAd();
     }

     public void OnInitializationFailed(UnityAdsInitializationError error, string message) {
       UnityEngine.Debug.Log("unity ads : unity interstitial ads init failed " + message);
     }
     #endregion


     #region IUnityAdsLoadListener
     public void OnUnityAdsAdLoaded(string placementId) {
       UnityEngine.Debug.Log("unity ads : unity interstitial ads to loaded ");
       m_unityInsterstitialReady = true;
     }

     public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message) {
       UnityEngine.Debug.Log("unity ads : unity interstitial ads failed to load " + message);
     }
     #endregion


   #region IUnityAdsShowListener
   public void OnUnityAdsShowClick(string placementId) {
   }
   public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState) {
   }

   public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message) {
     UnityEngine.Debug.Log($"unity ads : Error showing interstitial unity Ad Unit {placementId}: {error.ToString()} - {message}");
   }

   public void OnUnityAdsShowStart(string placementId) {
   }
   #endregion

   void LoadUnityInterstitalAd() {
     UnityEngine.Debug.Log("unity ads : try to load unity interstitial ads");
     if (m_unityInsterstitialReady) {
       UnityEngine.Debug.Log("unity ads : unity interstitial ads is alreay loaded");
       return;
     }
     m_unityInsterstitialReady = false;
     Advertisement.Load(videoAdPlacementID, this);
   }
 #endregion

   #region ApplovinAds

   // Put AppLovin SDK Key here or in your AndroidManifest.xml / Info.plist
   private const string SDK_KEY = "kyNwmH7leQ_s8k-gI4fiBNUxQNCFMf95bW_Lp75SX4IgoHL5L_pXNHSpff20JB5vi5FWCKzlPGPCXmHum4P8Mt";

   private void Log(string message) {
     Debug.Log(message);
   }

   public void InitializeApplovin() {
     if (!Constants.IS_AMAZON) { return; }
     // Check if user replaced the SDK key
     if ("YOUR_SDK_KEY_HERE".Equals(SDK_KEY)) {
       Log("ERROR: PLEASE UPDATE YOUR SDK KEY IN Assets/ApplovinManager.cs");
     } else {
       // Set SDK key and initialize SDK
       AppLovin.SetSdkKey(SDK_KEY);
       AppLovin.InitializeSdk();
       AppLovin.SetUnityAdListener("MainMenu");
       AppLovin.SetRewardedVideoUsername("demo_user");
     }
   }

   public void ShowInterstitialApplovin() {
     if (!Constants.IS_AMAZON) { return; }
     // Optional: You can call `AppLovin.PreloadInterstitial()` and listen to the "LOADED" event to preload the ad from the network before showing it
     AppLovin.ShowInterstitial();
   }

   public void ShowBannerApplovin() {
     if (!Constants.IS_AMAZON) { return; }
     Log("Showing banner ad");
     AppLovin.ShowAd(AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
   }
   #endregion


   #region Kidoz  
   public void InitializeKidoz() {
     Kidoz.Create();
 #if !UNITY_EDITOR
     Kidoz.init(kidozPubId, kidozSecurity);
     Kidoz.initSuccess += (obj) => {
       print("kidoz init success, init interstitial");
       KidozCreateInterstitial();
     };
 #endif
   }

   public void KidozCreateInterstitial() {
     Debug.Log("load kidoz interstitial");
 #if !UNITY_EDITOR
     Kidoz.loadInterstitialAd(false);
     Kidoz.interstitialClose += (obj) => {
       Kidoz.loadInterstitialAd(false);
     };
     Kidoz.interstitialOnLoadFail += (obj) => {
       Debug.Log("failed to load this moronic kidoz ad");
       Debug.Log(obj);
     };
 #endif
   }
   public void ShowInterstitialKidoz() {
     Debug.Log("************* show kidoz interstitial");
 #if !UNITY_EDITOR
     if (Kidoz.getIsInterstitialLoaded()) {
       Kidoz.showInterstitial();
     }
     Debug.Log("unable to load kidoz interstitial");
     Kidoz.loadInterstitialAd(false);
 #endif
   }
   #endregion

 */


  void OnInit(InitializationStatus initstatus) {
    // Callbacks from GoogleMobileAds are not guaranteed to be called on
    // main thread.
    // In this example we use MobileAdsEventExecutor to schedule these calls on
    // the next Update() loop.
    MobileAdsEventExecutor.ExecuteInUpdate(() => {
    });
  }

  void InitAdmobConsent() {
#if UNITY_EDITOR
    if ((m_admobTest == null) || (!m_admobTest.m_testOn)) {
      m_admobGDPRActive = false;
    }
#endif

    if (!m_admobGDPRActive) {
      // init admob
      InitializeAdmob();
      InitializeBannerAdmob();
      return;
    }



    ConsentRequestParameters request;
    if ((m_admobTest != null) && (m_admobTest.m_testOn)) {
#if UNITY_EDITOR
      ConsentInformation.Reset();
#endif
      var debugSettings = new ConsentDebugSettings {
        DebugGeography = DebugGeography.EEA,
        TestDeviceHashedIds = new List<string> { m_admobTest.m_testDeviceID }
      };

      // Here false means users are not under age of consent.
      request = new ConsentRequestParameters {
        TagForUnderAgeOfConsent = false,
        ConsentDebugSettings = debugSettings
      };
    } else {
      request = new ConsentRequestParameters {
        TagForUnderAgeOfConsent = false
      };
    }

    // Check the current consent information status.
    ConsentInformation.Update(request, OnConsentInfoUpdated);
  }

  void OnConsentInfoUpdated(FormError consentError) {
    Debug.Log("admob consent updated");
    if (consentError != null) {
      // Handle the error.
      UnityEngine.Debug.LogError("admob consent error " + consentError);
      return;
    }
    // If the error is null, the consent information state was updated.
    // You are now ready to check if a form is available.
    Debug.Log("admob can if required form?");
    ConsentForm.LoadAndShowConsentFormIfRequired((FormError formError) => {
      if (formError != null) {
        // Consent gathering failed.            
        UnityEngine.Debug.LogError("admob form error " + formError);
        return;
      }

      // Consent has been gathered.
      Debug.Log("admob check can request ads");
      if (ConsentInformation.CanRequestAds()) {
        Debug.Log("admob can req ad! start init admob");
        InitializeAdmob();
        InitializeBannerAdmob();
      }

    });

  }
}