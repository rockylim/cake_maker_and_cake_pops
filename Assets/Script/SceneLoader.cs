﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
  public static string loadedScene = "0MainMenu";
  public static string popupScene = "51Promo";
  public static string nextScene = "";
  public static int changes = 0;
  public static bool showRate = false;

  // Start is called before the first frame update
  void Start() {

  }

  public void LoadScene() {
    if (string.IsNullOrEmpty(loadedScene)) {
      SceneManager.UnloadSceneAsync(loadedScene);
      loadedScene = "";
    }
    //if (string.IsNullOrEmpty(popupScene))
    //{
    //    SceneManager.UnloadSceneAsync(popupScene);
    //    popupScene = "";
    //}
    SceneManager.LoadSceneAsync(nextScene);
  }

  public void LoadSceneAdditive(string scenename) {
    SceneManager.LoadSceneAsync(scenename, LoadSceneMode.Additive);
  }

  public void LoadSceneWithPopup(string scenename) {
    changes++;
    loadedScene = SceneManager.GetActiveScene().name;
    nextScene = scenename;

    float r = Random.value;
    if (r > 0.3f) {
      SceneManager.LoadScene(nextScene);
    } else {
      if (nextScene.Equals("0MainMenu") && showRate == false) {
        SceneManager.LoadSceneAsync("51Rate", LoadSceneMode.Additive);
        popupScene = "51Rate";
        showRate = true;
      } else {
        SceneManager.LoadScene(nextScene);
        //SceneManager.LoadSceneAsync("51Promo", LoadSceneMode.Additive);
        //popupScene = "51Promo";
      }
    }

    if (!nextScene.Equals("0MainMenu"))
      return;
    if (ServicesManager.Instance == null)
      return;
    ServicesManager.Instance.ShowInterstitial();


  }
}
