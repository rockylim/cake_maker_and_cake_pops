﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromoPopup : BaseManager
{
    public GameObject[] promoApps;

    private void Start()
    {
        int r = Random.Range(0, 3);
        for(int i = 0; i < promoApps.Length; i++)
        {
            if(i == r)
                promoApps[i].SetActive(true);
            else
                promoApps[i].SetActive(false);
        }
    }

    public void OpenPromoApp(string link)
    {
        Application.OpenURL(link);
    }
}
