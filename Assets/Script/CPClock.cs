﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CPClock : MonoBehaviour {
  public Transform hour, min, sec;
  private bool realtime = true;
  public UnityEvent CompleteEvent = null;

  // Start is called before the first frame update
  IEnumerator Start() {
    yield return new WaitForSeconds(10f);
    //RunClock("12:15:00");
    //RunClock(900);
  }

  // Update is called once per frame
  void Update() {
    if (!realtime)
      return;

    hour.eulerAngles = new Vector3(0f, 0f, (DateTime.Now.Hour + DateTime.Now.Minute / 60f) * -30f + 180f);
    min.eulerAngles = new Vector3(0f, 0f, DateTime.Now.Minute * -6f + 180f);
    sec.eulerAngles = new Vector3(0f, 0f, DateTime.Now.Second * -6f + 180f);
  }

  //public void RunClock(string til, float speed = 30f)
  //{
  //    TimeSpan ts = new TimeSpan(12, 0, 0);
  //    TimeSpan ts2 = String2Time(til);
  //    realtime = false;
  //    StartCoroutine(RunClockRoutine(ts, ts2));
  //}

  public void RunClock(int seconds) {
    TimeSpan ts = new TimeSpan(DateTime.Now.Ticks);
    TimeSpan ts2 = ts.Add(new TimeSpan(0, 0, seconds));
    realtime = false;
    StartCoroutine(RunClockRoutine(ts, ts2));
  }

  IEnumerator RunClockRoutine(TimeSpan ts, TimeSpan ts2, float speed = 300f) {
    AudioSource aud = AudioManager.Instance.Play("timer");
    while (true) {
      yield return null;
      float seconds = speed * Time.deltaTime;
      ts = ts.Add(new TimeSpan(0, 0, (int)seconds));
      hour.eulerAngles = new Vector3(0f, 0f, (ts.Hours + ts.Minutes / 60f) * -30f + 180f);
      min.eulerAngles = new Vector3(0f, 0f, ts.Minutes * -6f + 180f);
      sec.eulerAngles = new Vector3(0f, 0f, ts.Seconds * -6f + 180f);
      int target = 0;
#if UNITY_EDITOR
      target = 1000000;
#endif      
      if ((ts2 - ts).TotalSeconds <= target) {
        if (aud != null) aud.Stop();
        yield return new WaitForSeconds(1f);
        if (CompleteEvent != null)
          CompleteEvent.Invoke();
        break;
      }
    }
  }

  private TimeSpan String2Time(string stime) {
    int h = int.Parse(stime.Split(':')[0]);
    int m = int.Parse(stime.Split(':')[1]);
    int s = int.Parse(stime.Split(':')[2]);
    return new TimeSpan(h, m, s);
  }
}
