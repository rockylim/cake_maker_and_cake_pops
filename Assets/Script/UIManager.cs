﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : BaseManager {
  public GameObject MusicOn;
  public GameObject MusicOff;

  private void Start() {
    MusicOn.SetActive(AudioManager.Instance.MusicOn);
    MusicOff.SetActive(!AudioManager.Instance.MusicOn);
  }

  public void OnClickMusic(bool mute) {
    AudioManager.Instance.MuteMusic(mute);
  }

  public void SetGameMode(int mode) {
    DataManager.CurrentGame = (GameMode)mode;
  }

  public void MoreGames() {
#if UNITY_IOS || UNITY_STANDALONE_OSX
        Application.OpenURL("https://apps.apple.com/us/developer/detention-apps/id999167300");
#endif
#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) {
      Application.OpenURL("https://play.google.com/store/apps/details?id=com.detentionapps.candydessertpizzamaker&hl=en&gl=US");
    } else {
      Application.OpenURL("https://www.amazon.com/s?i=mobile-apps&rh=p_4%3ABrainfull&search-type=ss");
    }

#endif
  }

  public void Privacy() {
    Application.OpenURL("https://brainfullapps.com/privacy/");
  }

  public void OpenAdAppLink(string link) {
#if UNITY_IOS
        Application.OpenURL(link);
#endif
  }

  public void BuyButton(GameObject go) {
    //ExecuteEvents.Execute(go.gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
    CakeMake.Utils.AutoClickButton(go);
  }
}