﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CMContent : EventTrigger
{
    public bool moveContent = true;
    private bool dragging;
    private Vector3 offset = Vector3.zero;
    public bool fitSize = false;

    private Camera cam;
    private Vector3 worldPointMousePosition;

    void Start()
    {
        cam = Camera.main;
    }

    public void Update()
    {
        if (fitSize)
            FitContent();
        if (!moveContent)
            return;
        if (dragging)
        {
            worldPointMousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(worldPointMousePosition.x, worldPointMousePosition.y, 0f) + offset;
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        dragging = true;
        worldPointMousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
        offset = transform.position - new Vector3(worldPointMousePosition.x, worldPointMousePosition.y, 0f);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        dragging = false;
    }

    public virtual void ChangeContent(Image bg, bool nativesize)
    {
        GetComponent<Image>().sprite = bg.sprite;
        if(nativesize) GetComponent<Image>().SetNativeSize();
    }

    public virtual void ChangeContent(Sprite bg, bool nativesize)
    {
        GetComponent<Image>().sprite = bg;
        if (nativesize) GetComponent<Image>().SetNativeSize();
    }

    public void ChangeContent(Image bg, float scale = 1f, bool native = false, float width = 0f, float height = 0f)
    {
        GetComponent<Image>().sprite = bg.sprite;
        transform.localScale = Vector3.one * scale;
        if(native)
            transform.GetComponent<Image>().SetNativeSize();
        if (width != 0f && height != 0f)
            GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
    }

    //public void ChangeContent(Sprite bg)
    //{
    //    GetComponent<Image>().sprite = bg;
    //    if (nativeSize)
    //        GetComponent<Image>().SetNativeSize();
    //}

    public void FitContent()
    {
        //simular with perserve aspect. let's implement this later
        fitSize = false;
        Image image = GetComponent<Image>();
        Sprite sprite = image.sprite;
        float w = sprite.texture.width;
        float h = sprite.texture.height;
    }
}
