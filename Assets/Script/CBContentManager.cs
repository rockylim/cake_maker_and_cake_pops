﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CBContentManager : BaseContentManager
{
    public Transform pToppings, pDecorations, pWhipCreams;

    public void CreateTopping(Image content)
    {
        CreateContent(content, pToppings, 1f, false, 150f, 150f);
    }

    public void CreateDecoration(Image content)
    {
        CreateContent(content, pDecorations, 1f, false, 200f, 200f);
    }

    public void CreateWhipCream(Image content)
    {
        CreateContent(content, pWhipCreams, 1f, true);
    }
}
