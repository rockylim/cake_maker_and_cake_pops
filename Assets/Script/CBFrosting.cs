﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CBFrosting : CMContent
{
    public Sprite[] sprites;
    public void ChangeContent(Image image)
    {
        base.ChangeContent(image, true);
    }

    public void ChangeContent(string fronsting)
    {
        Sprite sprite = GetSprite(DataManager.CurrentShape, fronsting);
        if (sprite == null)
            return;
        ChangeContent(sprite, false);
        GetComponent<Image>().enabled = true;
    }

    private Sprite GetSprite(CakeShape cakeShape, string frosting)
    {
        foreach (Sprite sprite in sprites)
        {
            string s1 = sprite.name.ToLower();
            string s3 = cakeShape.ToString().ToLower();
            //if (!s1.Contains(s2))
            //    continue;
            if (!s1.Contains(s3))
                continue;
            if (!s1.Contains(frosting.ToLower()))
                continue;

            return sprite;
        }
        return null;
    }
}
