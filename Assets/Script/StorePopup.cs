﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StorePopup : BaseManager
{
    private IEnumerator Start()
    {
        while (!IAPManager.Instance.IsInitialized())
            yield return null;
        GameObject.Find("Panel Purchase").GetComponent<DG.Tweening.DOTweenAnimation>().DOPlay();
    }
    public void ClosePopup()
    {
        SceneManager.UnloadSceneAsync("51Store");
    }
}