﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CPBatter : EventTrigger
{
    private bool dragging;
    public Sprite[] batterSprites;
    public Transform BatterPiece;
    public Transform[] BatterPieces;
    public Transform[] BakedBatterPieces;
    private int actives = 0;
    public DOTweenAnimation HideTween;

    private Camera cam;
    private Vector3 worldPointMousePosition;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;

        //change images of batter and pieces
        BatterPiece.GetComponent<Image>().sprite = batterSprites[(int)DataManager.CurrentFavour];
        foreach(Transform piece in BatterPieces)
            piece.GetComponent<Image>().sprite = batterSprites[(int)DataManager.CurrentFavour];
        foreach (Transform piece in BakedBatterPieces)
            piece.GetComponent<Image>().sprite = batterSprites[(int)DataManager.CurrentFavour];

    }

    // Update is called once per frame
    void Update()
    {
        if (dragging)
        {
            worldPointMousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
            BatterPiece.gameObject.SetActive(true);
            BatterPiece.position = new Vector3(worldPointMousePosition.x, worldPointMousePosition.y, BatterPiece.position.z);

            //if (Input.GetMouseButtonUp(0))
            //{
            //    dragging = false;
            //    SetPiece();
            //}
        }
        else
        {
            BatterPiece.gameObject.SetActive(false);
        }
    }

    public void SetPiece()
    {
        Transform nearOne = null;
        float dist = Mathf.Infinity;
        foreach(Transform piece in BatterPieces)
        {
            if (piece.gameObject.activeSelf)
                continue;
            float d = (piece.transform.position - BatterPiece.transform.position).sqrMagnitude;
            if (d < dist)
            {
                dist = d;
                nearOne = piece;
            }
        }
        if (nearOne == null)
            return;
        if (dist > 2500f)
            return;
        nearOne.gameObject.SetActive(true);
        actives++;
        if(actives == BatterPieces.Length)
        {
            HideTween.DOPlay();
        }
        AudioManager.Instance.PlayOneShot("drop ingredients");
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        dragging = true;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        dragging = false;
        SetPiece();
    }
}
