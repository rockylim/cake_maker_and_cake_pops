﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapToEat : MonoBehaviour
{
    public GameObject   prefabEaten;
    public Transform    parent;

    private Camera cam;
    private Vector3 worldPointMousePosition;

    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            GameObject selected = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
            if (selected == null)
                return;
            CMContent cm = selected.GetComponent<CMContent>();
            if (cm == null)
                return;
            GameObject go = Instantiate(prefabEaten, parent) as GameObject;
            worldPointMousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
            go.transform.position = new Vector3(worldPointMousePosition.x, worldPointMousePosition.y, 0);
            AudioManager.Instance.PlayOneShot("eat");
        }
    }
}
