﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPBowl : Singleton<CPBowl>
{
    public Transform bowl;

    // Start is called before the first frame update
    void Start()
    {
        if (bowl == null)
            bowl = transform;
    }

    public void PutInBowl(Transform food)
    {
        food.SetParent(bowl);
    }
}
