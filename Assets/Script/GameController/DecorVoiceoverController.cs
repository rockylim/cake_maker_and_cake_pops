﻿using UnityEngine;

public class DecorVoiceoverController : MonoBehaviour {
  public BaseManager m_cmManager;
  int counter;
  void Start() {
    counter = 0;
  }

  void Update() {
  }

  public void PlaySfx() {
    counter++;
    if (counter == 1) {
      m_cmManager.PlaySoundEffect("vo_fantastic");
    } else if (counter == 9) {
      m_cmManager.PlaySoundEffect("vo_keep_going");
    } else if (counter == 17) {
      counter = 0;
    }
  }
}