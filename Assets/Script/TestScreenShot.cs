﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TestScreenShot : MonoBehaviour {
  public Text timeText;
  public List<string> loadedFiles = new List<string>();
  // Start is called before the first frame update
  void Start() {

  }

  private void Update() {
    timeText.text = DateStr();
  }

  public void TakeScreenShot() {
#if UNITY_EDITOR
    string folderpath = Directory.GetCurrentDirectory() + "/Screenshots/";
    Debug.Log(Directory.GetCurrentDirectory() + "/Screenshots/");

    ScreenCapture.CaptureScreenshot(folderpath + DateStr());
#else
        ScreenCapture.CaptureScreenshot(DateStr());
#endif
    Debug.Log("TakeScreenShot:" + DateStr());
  }

  public void LoadScreenShot() {
    string folderpath = Application.persistentDataPath;
#if UNITY_EDITOR
    folderpath = Directory.GetCurrentDirectory() + "/Screenshots/";
#endif
    loadedFiles.Clear();
    List<Sprite> sprites = new List<Sprite>();
    foreach (string file in Directory.GetFiles(folderpath)) {
      Debug.Log(file);
      //Texture2D tex = LoadTextureFromFile(file);
      //sprites.Add(SpriteFromTexture2D(tex));
      loadedFiles.Add(file);
    }
  }

  private string DateStr() {
    string date = System.DateTime.Now.ToString();
    date = date.Replace("/", "-");
    date = date.Replace(" ", "_");
    date = date.Replace(":", "-");
    return date;
  }
}
