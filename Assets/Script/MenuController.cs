﻿using Brainfull.CrossPromo;
using Caviezel.AppPermission;
using Caviezel.Core;
using Caviezel.Pretend;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuController : MonoBehaviour, ISceneController {

  public PtdParticleController m_particleController;
  public UIManager m_uiManager;
  public RectTransform[] m_bottomGUI;
  public GameObject m_buttonStore;
  public GameObject m_buttonRetore2;
  BCCrossPromo m_crossPromo;
  bool m_crossPromoChecked;
  AppPermissionController m_permissionController;
  int last = 1;

  #region ISceneController
  public void InitSceneController(ISceneStarter sceneStarter) {
    m_crossPromo = sceneStarter.Engine.GetSystem<BCCrossPromo>();
    Debug.Assert(null != m_crossPromo);

    m_permissionController = sceneStarter.Engine.GetSystem<AppPermissionController>();
    Debug.Assert(null != m_permissionController);

    m_permissionController.SetIsShowAds(!DataManager.NoAds);
    m_permissionController.StartMenu();


#if UNITY_ANDROID
    if (!Constants.IS_AMAZON) {
      for (int i = 0; i < m_bottomGUI.Length; i++) {
        m_bottomGUI[i].anchoredPosition = new Vector2(m_bottomGUI[i].anchoredPosition.x, 15f);
      }
    }
#elif UNITY_IOS
    /*for (int i = 0; i < m_bottomGUI.Length; i++) {
      m_bottomGUI[i].anchoredPosition = new Vector2(m_bottomGUI[i].anchoredPosition.x, 15f);
    } */
#endif

    if (Constants.IS_PAID) {
      m_bottomGUI[3].gameObject.SetActive(false);
      m_buttonStore.SetActive(false);
      m_buttonRetore2.SetActive(false);
    }
  }

  public void StartSceneController(ISceneStarter sceneStarter) {
    m_particleController.InitParticle();
    m_particleController.OnParticleStart();
    m_particleController.IsActive = true;

    //m_uiManager.PlaySoundEffect("tap_button_play");
  }

  public void UpdateSceneController(float dt) {
    m_particleController.UpdateComponent(dt);
    if (last == 1) {
      if (SceneManager.sceneCount == 2) {
        last = 2;
        HideCrossPromo();
      }
    } else if (last == 2) {
      if (SceneManager.sceneCount == 1) {
        last = 1;
        ShowCrossPromo();
      }
    }
    if ((m_crossPromo.IsDownloaded()) && (!m_crossPromoChecked)) {
      m_crossPromoChecked = true;
      m_crossPromo.TryShow(() => m_crossPromo.SetVisibleIconGroup(true));
    }
  }
  #endregion  

  public void HideCrossPromo() {
    m_crossPromo.TryShow(() => m_crossPromo.SetVisibleIconGroup(false));
  }

  public void ShowCrossPromo() {
    m_crossPromo.TryShow(() => m_crossPromo.SetVisibleIconGroup(true));
  }
}