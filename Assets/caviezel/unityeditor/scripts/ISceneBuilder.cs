﻿using UnityEngine;

namespace Caviezel {
  /// @class  ISceneBuilder
  ///
  /// @brief  needs to be a class instead of interface, otherwise it wont reveal the CustomEditor.
  public abstract class ISceneBuilder : MonoBehaviour {
    /// @fn public abstract string[] BuilderIds();
    ///
    /// @brief
    /// Button ids for the builder. the buttons that will be revealed in the inspector will be based
    /// on the length of the string.
    ///
    /// @return A string[].
    public abstract string[] BuilderIds();

    /// @fn public abstract void BuildScene(string builderId);
    ///
    /// @brief  Builds the scene.
    ///
    /// @param  builderId   Identifier for the builder.
    public abstract void BuildScene(string builderId);

    public static void ApplyPrefab(GameObject gameObject) {
#if UNITY_EDITOR
      /*GameObject instanceRoot = UnityEditor.PrefabUtility.FindRootGameObjectWithSameParentPrefab(gameObject);
      Object targetPrefab = UnityEditor.PrefabUtility.GetPrefabParent(instanceRoot);

      UnityEditor.PrefabUtility.ReplacePrefab(
              instanceRoot,
              targetPrefab,
              UnityEditor.ReplacePrefabOptions.ConnectToPrefab
              );*/
#endif
    }
  }
}
