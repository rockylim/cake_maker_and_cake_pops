﻿using UnityEngine;
using UnityEditor;
using Caviezel;

[CustomEditor(typeof(ISceneBuilder), true)]
public class SceneBuilderEditor : Editor {
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		ISceneBuilder script = (ISceneBuilder)target;
        for (int i = 0; i < script.BuilderIds().Length; ++i) {
            if (GUILayout.Button(script.BuilderIds()[i])) {
                script.BuildScene(script.BuilderIds()[i]);
            }
        }		
	}
}
