﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Caviezel {
  using EU = EditorUtils;
  using TextureType = WSpriteImporter.TextureType;

  public static class TextureImporterExt {
    public static string PlatformAndroid() {
      // well done unity! keep changing everything!
#if UNITY_2018_1_OR_NEWER
      return "android";
#else
      return "Android";
#endif
    }

    public static string PlatformIos() {
      // well done unity! keep changing everything!
#if UNITY_2018_1_OR_NEWER
      return "ios";
#else
      return "iPhone";
#endif
    }


    public static bool GetOriginalImageSize(this TextureImporter importer, out int width, out int height) {
      if (importer != null) {
        object[] args = new object[2] { 0, 0 };
        MethodInfo mi = typeof(TextureImporter).GetMethod("GetWidthAndHeight", BindingFlags.NonPublic | BindingFlags.Instance);
        mi.Invoke(importer, args);

        width = (int)args[0];
        height = (int)args[1];

        return true;
      }

      height = width = 0;
      return false;
    }

    public static void OverridePlatformSettings(this TextureImporter importer, string platform, TextureImporterFormat format, int maxSize) {
      TextureImporterPlatformSettings settings = importer.GetPlatformTextureSettings(platform);
      settings.overridden = true;
      settings.format = format;
      settings.maxTextureSize = maxSize;
      importer.SetPlatformTextureSettings(settings);
    }

    /// <summary>
    /// Post processor of texture of the background stuff.
    /// 1. ios, Set max size to 2048 format RGB 16 bit
    /// 2. android, Set max size to 1028 format RGB 16 bit
    /// </summary>    
    public static void SetBackgroundSettings(this TextureImporter importer) {
      // android
      importer.OverridePlatformSettings(PlatformAndroid(), TextureImporterFormat.RGB16, 1024);
      // default, ios included      
      importer.maxTextureSize = 2048;
      importer.crunchedCompression = true;

      // apply changes
      EditorUtility.SetDirty(importer);
      importer.SaveAndReimport();
    }
    public static int PrevPowerOfTwo(int v) {
      if (IsPowerOfTwo(v)) return v;

      v |= v >> 1;
      v |= v >> 2;
      v |= v >> 4;
      v |= v >> 8;
      v |= v >> 16;

      return v - (v >> 1);
    }

    public static bool IsPowerOfTwo(int x) {
      return (x != 0) && ((x & (x - 1)) == 0);
    }
    /// <summary>
    /// The Post Processor for the common images. How it works:
    /// 1. Get the original image dimensions e.g 400x200
    /// 2. Get the max size of width and height (in this case 400), then get previous power of two of it i.e 256
    /// 3. For android ONLY set max size to the POT value, set format to DXT5, if larger than 1024 set format to rgba16 (no atlas)    
    /// </summary>    
    public static void SetCommonSettings(this TextureImporter importer, string atlasName, bool IsResourcesFolder) {
      int w, h;
      importer.GetOriginalImageSize(out w, out h);
      int prevPowerOfTwo = PrevPowerOfTwo(System.Math.Max(w, h)) * 2;
      // android      
      if (!IsResourcesFolder) {
        if (prevPowerOfTwo < 1024) {
          importer.OverridePlatformSettings(PlatformAndroid(), TextureImporterFormat.DXT5Crunched, prevPowerOfTwo);
          importer.spritePackingTag = atlasName;
        } else {
          importer.OverridePlatformSettings(PlatformAndroid(), TextureImporterFormat.RGBA16, prevPowerOfTwo);
        }
      } else {
        importer.OverridePlatformSettings(PlatformAndroid(), TextureImporterFormat.RGBA16, prevPowerOfTwo);
      }


      // default, ios included      
      importer.maxTextureSize = prevPowerOfTwo * 2;
      importer.crunchedCompression = true;

      EditorUtility.SetDirty(importer);
      importer.SaveAndReimport();
    }
  }

  [CustomEditor(typeof(WSpriteImporter))]
  public class WSpriteImporterEditor : Editor {
    delegate void TexturePostProcessor(TextureImporter importer);

    public override void OnInspectorGUI() {
      DrawDefaultInspector();
      WSpriteImporter tgt = (WSpriteImporter)target;
      EU.VPadding(() => {
        tgt.Data.ForEach(d => {
          if (GUILayout.Button("PACK " + d.FolderPaths.Ellipsis(20))) {
            d.Folders.ForEach(f => {
              string fullDir = d.Path + f.FolderName;
              List<string> filePaths = new List<string>();
              CheckFolderFiles(fullDir, filePaths, f.IncludeAllFoldersAndFiles);

              foreach (string fName in filePaths) {
                if (fName.EndsWithMulti(new List<string> { "png", "jpg" })) {
                  string folderName = new DirectoryInfo(fName).Parent.Name;
                  TextureImporter textureImporter = (TextureImporter)AssetImporter.GetAtPath(fName);
                  Process(f.Type, textureImporter, f.SetAtlasNameAsFolderName ? folderName : f.AtlasName, d.IsResourcesFolder);
                }
              }
            });
          }
          EditorUtils.VSpacing();
        });
      });
    }

    void Process(TextureType type, TextureImporter importer, string atlasName, bool IsResourcesFolder) {
      if (type == TextureType.Common) {
        importer.SetCommonSettings(atlasName, IsResourcesFolder);
      } else if (type == TextureType.Background) {
        importer.SetBackgroundSettings();
      }
    }

    void CheckFolderFiles(string dir, List<string> filePaths, bool isIncludeAll) {
      string[] files = Directory.GetFiles(dir);
      foreach (string file in files) {
        if (file.EndsWithMulti(new List<string> { "png", "jpg" })) {
          filePaths.Add(file);
        }
      }

      if (!isIncludeAll) {
        return;
      }

      string[] folders = Directory.GetDirectories(dir);
      foreach (string folder in folders) {
        CheckFolderFiles(folder, filePaths, isIncludeAll);
      }
    }
  }
}
