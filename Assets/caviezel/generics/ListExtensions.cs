﻿using System;
using System.Collections.Generic;

namespace Caviezel {
  public static class ListExt {
    public delegate T Reducer<T, U>(T prev, U current);
    public delegate U Mapper<T, U>(T t);
    public delegate void Iterator<T>(T item, int idx);
    public static void Swap<T>(this IList<T> list, int indexA, int indexB) {
      T tmp = list[indexA];
      list[indexA] = list[indexB];
      list[indexB] = tmp;
    }

    public static void Shuffle<T>(this IList<T> list) {
      System.Random rand = MathExtensions.GetRandom();
      int n = list.Count;
      while (n > 1) {
        n--;
        int k = rand.Next(n + 1);
        T value = list[k];
        list[k] = list[n];
        list[n] = value;
      }
    }

    public static bool IsEqual<T>(this IList<T> list1, IList<T> list2) where T : IEquatable<T> {
      if (list1.Count != list2.Count) {
        return false;
      }

      for (int i = 0; i < list1.Count; ++i) {
        if (!list1[i].Equals(list2[i])) {
          return false;
        }
      }

      return true;
    }

    public static bool IsContain<T>(this IList<T> list1, IList<T> list2, bool shouldSameSize = true) {
      if (shouldSameSize && list1.Count != list2.Count) {
        return false;
      }

      for (int i = 0; i < list2.Count; ++i) {
        if (!list1.Contains(list2[i])) {
          return false;
        }
      }

      return true;
    }

    public static T GetNullable<T>(this IList<T> list) where T : class {
      for (int i = 0; i < list.Count; ++i) {
        T t = list[i] as T;
        if (null != t) {
          return t;
        }
      }
      return null;
    }

    public static List<U> Map<T, U>(this IEnumerable<T> list, Mapper<T, U> mapper) {
      List<U> newList = new List<U>();
      foreach (T itm in list) {
        newList.Add(mapper(itm));
      }
      return newList;
    }

    public static T Fold<T, U>(this IEnumerable<U> l, T initialValue, Reducer<T, U> reducer) {
      T cur = initialValue;
      foreach (U itm in l) {
        cur = reducer(cur, itm);
      }
      return cur;
    }

    public static void Loop<T>(this IList<T> l, Iterator<T> iter) {
      for (int i = 0; i < l.Count; ++i) {
        iter(l[i], i);
      }
    }
  }
}
