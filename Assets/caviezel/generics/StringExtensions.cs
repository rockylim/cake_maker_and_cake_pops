using System;
using System.Collections.Generic;

namespace Caviezel {
  public enum ComparisonType {
    Match,
    Contain,
    StartsWith,
    EndsWith,
    NotEqual
  }

  public static class StringExt {

    /// <summary>
    /// Checks whether the string is empty
    /// </summary>
    public static bool IsEmpty(this string str) {
      return string.IsNullOrEmpty(str);
    }

    public static bool IsEqual(this string lhs, string rhs) {
      //check length first, for faster comparison
      if (lhs.Length == rhs.Length) {
        if (string.Compare(lhs, rhs, true) == 0) {
          return true;
        }
      }
      return false;
    }

    public static bool Matches(this string lhs, string rhs, ComparisonType type) {
      switch (type) {
        case ComparisonType.Match:
          return lhs.IsEqual(rhs);
        case ComparisonType.Contain:
          return lhs.Contains(rhs);
        case ComparisonType.StartsWith:
          return lhs.StartsWith(rhs, StringComparison.Ordinal);
        case ComparisonType.EndsWith:
          return lhs.EndsWith(rhs, StringComparison.Ordinal);
        case ComparisonType.NotEqual:
          return !lhs.IsEqual(rhs);
      }

      return false;
    }

    public static bool EndsWithMulti(this string str, IList<string> candidates) {
      foreach (string c in candidates) {
        if (str.EndsWith(c)) return true;
      }
      return false;
    }

    public static string Ellipsis(this string str, int maxLength = 10) {
      if (str.IsEmpty() || str.Length < maxLength) return str;

      string sub = str.Substring(0, maxLength);
      return sub + "...";
    }
  }
}
