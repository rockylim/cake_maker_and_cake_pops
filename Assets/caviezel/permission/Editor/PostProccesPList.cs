﻿using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
using System.IO;
#endif


namespace Caviezel {
  namespace AppPermission {
    public class PostProcessATT {

      //setting here!
      const bool m_addUserTrackingUsageDescription = false;
      const bool m_addSDKNetworkID = true;

      const string TrackingDescription =
          "Allow advertisers to track activity? The advertisers are stated in our Privacy Policy. Your data will be used to provide you a better and personalized ad experience. Ads help us provide free content to you.";

      [PostProcessBuild(0)]
      public static void OnPostprocessBuild(BuildTarget buildTarget, string pathToXcode) {
#if UNITY_IOS
        if (buildTarget == BuildTarget.iOS) {
          AddPListValues(pathToXcode);
        }
#endif
      }

      static void AddPListValues(string pathToXcode) {
#if UNITY_IOS
        // Get Plist from Xcode project 
        string plistPath = pathToXcode + "/Info.plist";

        // Read in Plist 
        PlistDocument plistObj = new PlistDocument();
        plistObj.ReadFromString(File.ReadAllText(plistPath));

        // set values from the root obj
        PlistElementDict plistRoot = plistObj.root;
        if (m_addUserTrackingUsageDescription) {
          plistRoot.SetString("NSUserTrackingUsageDescription", TrackingDescription);
        }

        if (m_addSDKNetworkID) {
          PlistElementArray array = plistRoot.CreateArray("SKAdNetworkItems");
          array.AddDict().SetString("SKAdNetworkIdentifier", "bvpn9ufa9b.skadnetwork"); // unity ads
          array.AddDict().SetString("SKAdNetworkIdentifier", "4dzt52r2t5.skadnetwork"); // unity ads
          array.AddDict().SetString("SKAdNetworkIdentifier", "cstr6suwn9.skadnetwork"); // google admob
          array.AddDict().SetString("SKAdNetworkIdentifier", "v79kvwwj4g.skadnetwork"); // kidoz
          array.AddDict().SetString("SKAdNetworkIdentifier", "ludvb6z3bs.skadnetwork"); // applovin          

          // from unity dashboard
          array.AddDict().SetString("SKAdNetworkIdentifier", "w9q455wk68.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "5tjdwbrq8w.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "f7s53z58qe.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "2u9pt9hc89.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "4pfyvq9l8r.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "3rd42ekr43.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "c6k4g5qg8m.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "tl55sbb4fm.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "3qy4746246.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "5a6flpkh64.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "glqzh8vgby.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "prcb7njmu6.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "mp6xlyr22a.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "kbd757ywx3.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "ydx93a7ass.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "f38h382jlk.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "22mmun2rn5.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "v72qych5uu.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "f73kdq92p3.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "3sh42y64q3.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "zmvfpc5aq8.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "424m5254lk.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "578prtvx9j.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "9rd848q2bz.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "4468km3ulz.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "4fzdc2evr5.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "8s468mfl3y.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "x44k69ngh6.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "s39g8k73mm.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "238da6jt44.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "32z4fx6l9h.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "av6w8kgt66.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "hs6bdukanm.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "44jx6755aq.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "zq492l623r.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "k674qkevps.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "lr83yxwka7.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "9t245vhmpl.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "wg4vff78zm.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "ppxm28t8ap.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "488r3q3dtq.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "5lm9lj6jb7.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "mlmmfzh3r3.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "t38b2kh725.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "yclnxrl5pm.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "wzmmz9fp6w.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "7ug5zh24hu.skadnetwork");
          array.AddDict().SetString("SKAdNetworkIdentifier", "m8dbw4sv7c.skadnetwork");
        }

        // save
        File.WriteAllText(plistPath, plistObj.WriteToString());
#endif
      }

    }
  }
}