﻿using System;
using Caviezel.Core;
using UnityEngine;
#if UNITY_IOS
using Unity.Advertisement.IosSupport;
#endif

#if UNITY_ANDROID
using UnityEngine.Android;
#endif


namespace Caviezel {
  namespace AppPermission {
    public class AppPermissionController : MonoBehaviour, ISystem {
      [Serializable]
      public class PermissionData {
        public string m_permissionName;
        public int m_changeSceneTarget = -1;
        public bool m_showInInit;
        public CapacityData m_intervalSceneChanges;
      }
      [Header("ANDROID")]
      public PermissionData[] m_androidPermission;

      [Header("IOS")]
      public PermissionData[] m_iOSPermission;
      bool m_isShowAds;
      PermissionData[] m_activePermission;


      #region ISystem implementation
      public void InitSystem() {
#if UNITY_IOS
        m_activePermission = m_iOSPermission;
#endif

#if UNITY_ANDROID
        m_activePermission = m_androidPermission;         
#endif      
      }

      public void StartSystem(CavEngine gameEngine) {
      }

      public void UpdateSystem(float dt) {
      }

      public void OnChangeScene(int index) {
      }
      #endregion     

      public void ShowPermission(string permissionType) {
        if (m_activePermission == null) { return; }
#if UNITY_IOS
        if ((permissionType == "ATT") && (m_isShowAds)) {
          /*if (ATTrackingStatusBinding.GetAuthorizationTrackingStatus() ==
                    ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED) {
            ATTrackingStatusBinding.RequestAuthorizationTracking();
        }*/
#if UNITY_EDITOR
          Debug.Log("****** SHOW ATT PERMISSON******");
#endif
        }
#endif

#if UNITY_ANDROID
        if (permissionType == "StorageWritter") {
          if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)) {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
          }
        }
#endif
      }

      public void StartMenu() {
        if (m_activePermission == null) { return; }
        for (int i = 0; i < m_activePermission.Length; i++) {
          if (m_activePermission[i].m_showInInit) {
            ShowPermission(m_activePermission[i].m_permissionName);
          }

          if (m_activePermission[i].m_changeSceneTarget > -1) {
            m_activePermission[i].m_intervalSceneChanges = new CapacityData(m_activePermission[i].m_changeSceneTarget);
          }
        }
      }

      public void AddChangeSceneCount() {
        if (m_activePermission == null) { return; }
        for (int i = 0; i < m_activePermission.Length; i++) {
          if ((m_activePermission[i].m_intervalSceneChanges != null) && (m_activePermission[i].m_intervalSceneChanges.Max > -1)) {
            if (!m_activePermission[i].m_intervalSceneChanges.Add()) {
              m_activePermission[i].m_intervalSceneChanges.Reset();
              ShowPermission(m_activePermission[i].m_permissionName);
            }
          }
        }
      }

      public void SetIsShowAds(bool isShowAds) {
        m_isShowAds = isShowAds;
      }
    }
  }
}