﻿using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
	namespace UI {
		public abstract class ViewComponent : MonoBehaviour, IViewComponent {
			public abstract void Setup(ISceneStarter sceneStarter, IViewManager viewManager);				
		}
	}
}
