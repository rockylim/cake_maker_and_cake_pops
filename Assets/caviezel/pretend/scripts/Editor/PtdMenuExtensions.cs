﻿using UnityEngine;
using UnityEditor;
using Caviezel.Serialization;

namespace Caviezel {
    namespace Pretend {
        public class PtdMenuExtensions {
            [MenuItem("Pretend/Delete Progress")]
            static void ResetAllData() {
                IDataSerializer serializer = new JsonDataSerializer();
                serializer.Delete(PtdPersister.GetPath());
                //notify
                Debug.Log("Data deleted!");
            }
        }
    }
}
