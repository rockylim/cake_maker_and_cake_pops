using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Caviezel;
using Caviezel.Tween;


namespace Caviezel {
    namespace Pretend {
        // to use this tween, game object and the tween should designed in correct structure!
        // GameObject
        //  -- Part (the name  must to be "Part")
        //      -- body_part EX: handLeft
        //  -- Tweens (the name  must to be "Tweens")
        //      -- tw_name  Ex: tw_walk
        //          -- tw_name_body_part Ex: tw_walk_handLeft, the target for this tween will automatically assigned to handLeft

        public class PtdTweenBodyTargetAssigner {
            static string m_tweenName;
            [MenuItem("Pretend/Assign Tween Body Target")]
            static void AssignTarget() {
                List<GameObject> selectedGameObject = new List<GameObject>(Selection.gameObjects);
                selectedGameObject.Sort((go1, go2) => go1.transform.GetSiblingIndex().CompareTo(go2.transform.GetSiblingIndex()));

                foreach (GameObject gameObject in selectedGameObject) {
                    Transform tweenParent = gameObject.transform.Find("Tweens");
                    Transform partParent = gameObject.transform.Find("Part");
                    if ((tweenParent != null) && (partParent != null)) {
                        RectTransform tweenParentRect = tweenParent.GetComponent<RectTransform>();
                        SetGameObjectDefault(tweenParentRect);
                        for (int i = 0; i < tweenParent.transform.childCount; i++) {
                            AssignTweenToTarget(tweenParent.transform.GetChild(i).gameObject, partParent);
                        }
                    }
                }
            }

            static void AssignTweenToTarget(GameObject tweenGameObject, Transform part) {
                SetGameObjectDefault(tweenGameObject.GetComponent<RectTransform>());
                m_tweenName = tweenGameObject.name;
                ITween[] tweens = tweenGameObject.GetComponentsInChildren<ITween>();

                foreach (ITween tween in tweens) {
                    GameObject target = GetTarget(tween, part);
                    if (target != null) {
                        Assign(tween, target);
                    }
                }
            }

            static GameObject GetTarget(ITween tween, Transform part) {
                string tweenGameObjectName = "";
                if (tween is TweenMove) {
                    tweenGameObjectName = ((TweenMove)tween).gameObject.name;
                    SetGameObjectDefault(((TweenMove)tween).gameObject.GetComponent<RectTransform>());
                } else if (tween is TweenRotation) {
                    tweenGameObjectName = ((TweenRotation)tween).gameObject.name;
                    SetGameObjectDefault(((TweenRotation)tween).gameObject.GetComponent<RectTransform>());
                } else if (tween is TweenFade) {
                    tweenGameObjectName = ((TweenFade)tween).gameObject.name;
                    SetGameObjectDefault(((TweenFade)tween).gameObject.GetComponent<RectTransform>());
                } else if (tween is TweenScale) {
                    tweenGameObjectName = ((TweenScale)tween).gameObject.name;
                    SetGameObjectDefault(((TweenScale)tween).gameObject.GetComponent<RectTransform>());
                } else if (tween is TweenColor) {
                    tweenGameObjectName = ((TweenColor)tween).gameObject.name;
                    SetGameObjectDefault(((TweenColor)tween).gameObject.GetComponent<RectTransform>());
                } else if (tween is TweenScale) {
                    tweenGameObjectName = ((TweenScale)tween).gameObject.name;
                    SetGameObjectDefault(((TweenScale)tween).gameObject.GetComponent<RectTransform>());
                } else if (tween is TweenSpriteAnim) {
                    tweenGameObjectName = ((TweenSpriteAnim)tween).gameObject.name;
                    SetGameObjectDefault(((TweenSpriteAnim)tween).gameObject.GetComponent<RectTransform>());
                }

                if (tweenGameObjectName.Contains("sub_tw")) {
                    //Debug.Log(tweenGameObjectName.Substring(4));
                    m_tweenName = tweenGameObjectName.Substring(4);
                    return null;
                }
                //Debug.Log(tweenGameObjectName + " - " + m_tweenName);
                string targetedPartName = tweenGameObjectName.Substring(m_tweenName.Length + 1);

                Transform[] allParts = part.GetComponentsInChildren<Transform>();

                foreach (Transform prt in allParts) {
                    //Debug.Log(prt.gameObject.name + " + " + targetedPartName);
                    if (prt.gameObject.name == targetedPartName) {
                        return prt.gameObject;
                    }
                }
                return null;
            }

            static void Assign(ITween tween, GameObject target) {
                string tweenName = m_tweenName;
                if (tween is TweenMove) {
                    ((TweenMove)tween).m_tweenData.m_id = tweenName;
                    ((TweenMove)tween).m_tweenData.m_otherTarget = target;
                } else if (tween is TweenRotation) {
                    ((TweenRotation)tween).m_tweenData.m_id = tweenName;
                    ((TweenRotation)tween).m_tweenData.m_otherTarget = target;
                } else if (tween is TweenFade) {
                    ((TweenFade)tween).m_tweenData.m_id = tweenName;
                    ((TweenFade)tween).m_tweenData.m_otherTarget = target;
                } else if (tween is TweenScale) {
                    ((TweenScale)tween).m_tweenData.m_id = tweenName;
                    ((TweenScale)tween).m_tweenData.m_otherTarget = target;
                } else if (tween is TweenColor) {
                    ((TweenColor)tween).m_tweenData.m_id = tweenName;
                    ((TweenColor)tween).m_tweenData.m_otherTarget = target;
                } else if (tween is TweenScale) {
                    ((TweenScale)tween).m_tweenData.m_id = tweenName;
                    ((TweenScale)tween).m_tweenData.m_otherTarget = target;
                } else if (tween is TweenSpriteAnim) {
                    ((TweenSpriteAnim)tween).m_tweenData.m_id = tweenName;
                    ((TweenSpriteAnim)tween).m_tweenData.m_otherTarget = target;
                }
            }

            static void SetGameObjectDefault(RectTransform rect) {
                rect.SetPos(new Vector2(0f, 0f));
                rect.SetScale(new Vector2(1f, 1f));
                rect.SetSize(new Vector2(1f, 1f));
            }
        }
    }
}
