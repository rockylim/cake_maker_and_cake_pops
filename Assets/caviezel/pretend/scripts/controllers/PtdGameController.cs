﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Caviezel.Core;
using Caviezel.Serialization;

namespace Caviezel {
    namespace Pretend {
        public class PtdGameController : MonoBehaviour, ISceneController, IGameController {
            public string m_screenId;
            public GameObject m_mainScreen;
            public RectTransform m_objectRoot;
            public RectTransform m_layerGroup;

            Dictionary<string, PtdObject> m_objects = new Dictionary<string, PtdObject>();
            HashSet<PtdObject> m_saveableObjects = new HashSet<PtdObject>();
            HashSet<IComponentController> m_controllers = new HashSet<IComponentController>();
            HashSet<IDataProvider> m_dataProviders = new HashSet<IDataProvider>();
            PtdSystem m_ptdSystem;
            GamePersisters m_persisters;
            PtdPersister m_persister;

            #region ISceneController
            public void InitSceneController(ISceneStarter sceneStarter) {                
                //get ptd system
                m_ptdSystem = sceneStarter.Engine.GetSystem<PtdSystem>();
                Debug.Assert(null != m_ptdSystem);
                //get persister
                m_persisters = sceneStarter.Engine.GetSystem<GamePersisters>();
                m_persister = m_persisters.GetPersister<PtdPersister>();
                m_persister.SetSceneData(m_screenId);
            }

            public void StartSceneController(ISceneStarter sceneStarter) {                
                InitObjectControllers();
                InitObjects(sceneStarter);
                StartGame();
            }

            public void UpdateSceneController(float dt) {
                //update the controller(s)
                foreach (IComponentController objController in m_controllers) {
                    objController.UpdateController(dt);
                }
                //update the ptd object(s)
                foreach (KeyValuePair<string, PtdObject> obj in m_objects) {
                    obj.Value.UpdateObject(dt);
                }
                //for testing purpose
                if (Input.GetKeyDown(KeyCode.S)) {
                    Save();
                }
            }
            #endregion

            #region IPtdGameController
            public RectTransform MainRoot {
                get { return m_objectRoot; }
            }

            public void RetainComponent(IComponent comp) {
                //get data provider
                IDataProvider dataProvider = comp as IDataProvider;
                if (null != dataProvider) {
                    m_dataProviders.Add(dataProvider);
                }
                //retain component in controllers
                foreach (IComponentController objController in m_controllers) {
                    objController.RetainComponent(comp);
                }
            }

            public void OnComponentProcessed(IComponentController processor, IComponent comp) {
                foreach (IComponentController controller in m_controllers) {
                    //notify the other controller but self
                    if (processor != controller) {
                        controller.OnProcessedComponent(comp);
                    }
                }
            }

            public void BeginComponentInteraction(PtdObject obj) {
                foreach (IComponentController objController in m_controllers) {
                    objController.BeginInteraction(obj);
                }
            }

            public void ProcessComponentInteraction(PtdObject obj) {                
                foreach (IComponentController objController in m_controllers) {                                          
                    objController.ProcessInteraction(obj);
                }
            }

            public void BroadcastEvent(PtdObject senderOwner, BaseSenderEv ev) {
                if (ev.IsSendable()) {
                    Debugger.Log("GAME EVENTS", string.Format("SENDING {0}, by PTDOBJECT with ID : {1}", ev, senderOwner.ObjectId));
                    foreach (KeyValuePair<string, PtdObject> obj in m_objects) {
                        int objId = obj.Value.GetInstanceID();
                        int senderId = senderOwner.GetInstanceID();
                        //default to internal
                        EventScope scope = EventScope.Internal;
                        //set scope as global if object is not the same as the sender
                        if (objId != senderId) {                            
                            scope = obj.Value.IsSibling(senderOwner) ? EventScope.Sibling : EventScope.Global;
                        }
                        //send ev to the obj
                        obj.Value.OnReceiveEvent(ev, scope);
                    }   
                }
            }

            public void ToScene(string id) {
                if (m_ptdSystem.CanOpenScene(id)) {
                    //save before exit
                    Save();
                    //notify
                    foreach (IComponentController objController in m_controllers) {
                        objController.OnExitGame();
                    }
                    SceneManager.LoadScene(id);
                }
            }

            public PtdObject GetObjectById(string id) {
                PtdObject obj = null;
                m_objects.TryGetValue(id, out obj);
                return obj;
            }

            public T GetProviderData<T>(SaveObjectData componentData) where T : class {
                foreach (IDataProvider dataProvider in m_dataProviders) {
                    IDataProvider<T> provider = dataProvider as IDataProvider<T>;
                    T providerData = provider.ProvideData(componentData);
                    if (null != providerData) {                        
                        return providerData;
                    }
                }
                return null;
            }
            #endregion

            void InitObjects(ISceneStarter sceneStarter) {
                PtdObject[] objs = m_mainScreen.GetComponentsInChildren<PtdObject>(true);
                //setup
                foreach (PtdObject obj in objs) {
                    obj.Setup();
                    obj.OnAwakeGame();
                    try {
                        m_objects.Add(obj.ObjectId, obj);
                        //add to the saveables if it's saveable
                        if (obj.m_isSaveable) {
                            m_saveableObjects.Add(obj);
                        }
                    }
                    catch (ArgumentException) {
                        Debug.LogError(string.Format("ptd object with game object name = {0} already exists", obj.name));
                    }
                }
                //init
                foreach (KeyValuePair<string, PtdObject> obj in m_objects) {
                    obj.Value.InitObject(sceneStarter, this);
                }
            }

            void InitObjectControllers() {
                m_controllers.Add(new PtdCollisionController(this));
                m_controllers.Add(new PtdGroundController(this));
                m_controllers.Add(new PtdDestructionController(this));
                m_controllers.Add(new PtdLayerController(m_layerGroup));
                m_controllers.Add(new PtdDrawingController());
            }

            void StartGame() {
                //load save first
                Load();
                //start the game
                foreach (KeyValuePair<string, PtdObject> obj in m_objects) {
                    obj.Value.OnStartGame();
                }
                foreach (IComponentController controller in m_controllers) {
                    controller.OnStartGame();
                }
            }

            void Load() {
                //load save stuff first
                foreach (PtdObject obj in m_saveableObjects) {
                    string objectId = obj.ObjectId;
                    SaveObjectData data = m_persister.GetObjectData(objectId);
                    //add new if not exist
                    if (null == data) {
                        data = new SaveObjectData(objectId);
                    }
                    obj.OnLoad(data);
                }
            }

            void Save() {
                m_persister.ClearSaveables();
                foreach (PtdObject obj in m_saveableObjects) {
                    if (obj.m_isSaveable) {
                        m_persister.SetObjectData(obj.OnSave());   
                    }
                }
                m_persisters.SaveAll();
            }
        }
    }
}
