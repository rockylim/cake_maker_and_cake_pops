﻿using System.Collections.Generic;
using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        class CollisionTask {
            List<ICollider> m_checkedColliders;
            HashSet<ICollider> m_targets = new HashSet<ICollider>();

            public PtdObject Owner { get; set; }

            public CollisionTask(ICollection<ICollider> targets, ICollection<ICollider> checkedColliders) {
                foreach (ICollider target in targets) {
                    if (target.IsActive) {
                        m_targets.Add(target);
                    }
                }
                m_checkedColliders = new List<ICollider>(checkedColliders);
            }

            public ICollider CheckCollision() {
                foreach (ICollider target in m_targets) {
                    if (!target.Owner.IsInAction) {
                        //iterate over the colliders
                        foreach (ICollider otherCollider in m_checkedColliders) {
                            int idx = 0;
                            //iterate over the collider areas
                            while (null != target && idx < target.Colliders.Count) {
                                foreach (ColliderData otherColliderData in otherCollider.Colliders) {
                                    #if UNITY_EDITOR
                                    Debug.Assert(null != otherColliderData.m_area, string.Format("rect transform is null for ptd object with gameobject name = {0}, collider id = {1}", otherCollider.Owner, otherColliderData.m_id));
                                    #endif
                                    //dont detect the collision if the other collider is currently inactive
                                    if (!otherCollider.IsActive) {
                                        continue;
                                    }
                                    //check the intersection between the collider and the other
                                    if (target.Colliders[idx].m_area.RectAgainstRoot().Intersects(otherColliderData.m_area.RectAgainstRoot())) {
                                        OnCollideData onColData = new OnCollideData(target, target.Colliders[idx], otherCollider, otherColliderData);
                                        //dont process if both are currently attached one to another
                                        if (!onColData.IsSticking()) {
                                            if (target.ShouldCollide(onColData) || otherCollider.ShouldCollide(onColData)) {                                                
                                                return target;
                                            }
                                        }
                                    }
                                }
                                //check the next collider
                                ++idx;
                            }
                        }
                    }
                }
                return null;
            }
        }

        public class PtdCollisionController : IComponentController {
            HashSet<ICollider> m_colliders = new HashSet<ICollider>();
            HashSet<CollisionTask> m_collisionTasks = new HashSet<CollisionTask>();
            IGameController m_gameController;

            public PtdCollisionController(IGameController controller) {
                m_gameController = controller;
            }

            public void OnProcessedComponent(IComponent comp) {
                HashSet<CollisionTask> removed = new HashSet<CollisionTask>();
                foreach (CollisionTask task in m_collisionTasks) {
                    if (task.Owner.ObjectId == comp.Owner.ObjectId) {
                        removed.Add(task);
                    }
                }
                //remove
                foreach (CollisionTask remove in removed) {
                    m_collisionTasks.Remove(remove);
                }
            }

            public void RetainComponent(IComponent comp) {
                ICollider collider = comp as ICollider;
                if (null != collider) {
                    m_colliders.Add(collider);
                }
            }

            public void BeginInteraction(PtdObject obj) {}

            public void ProcessInteraction(PtdObject obj) {
                //get the active collider(s)
                HashSet<ICollider> targets = obj.ComponentSystem.GetComponents<ICollider>();
                targets.RemoveWhere(x => !x.IsActive);
                //create candidates
                List<ICollider> candidateColliders = new List<ICollider>();

                //iterate over all colliders
                foreach (ICollider otherCollider in m_colliders) {
                    //iterate over
                    foreach (ICollider target in targets) {
                        if (otherCollider.Owner.ObjectId.IsEqual(target.Owner.ObjectId)) {
                            break;
                        }
                        //set the closest distance first
                        float closestDistance = 100f;
                        //get the target rect
                        Rect targetRect = target.Owner.RootTransform.RectAgainstRoot();
                        Vector2 targetPos = new Vector2(targetRect.x, targetRect.y);
                        //get the other rect
                        Rect otherRect = otherCollider.Owner.RootTransform.RectAgainstRoot();
                        Vector2 otherPos = new Vector2(otherRect.x, otherRect.y);
                        //set the y to the target rect y
                        otherRect.y = targetRect.y;
                        //check whether it intersects, if so add to the candidate collection
                        if (otherRect.Intersects(targetRect)) {
                            float otherToTargetDist = Vector2.Distance(targetPos, otherPos);
                            if (otherToTargetDist < closestDistance) {
                                candidateColliders.Insert(0, otherCollider);
                                //we've got the new distance
                                closestDistance = otherToTargetDist;
                            } else {
                                candidateColliders.Add(otherCollider);
                            }
                        }
                    }
                }
                //generate a new task
                CollisionTask newTask = new CollisionTask(targets, candidateColliders);
                newTask.Owner = obj;
                m_collisionTasks.Add(newTask);
            }

            public void OnStartController() {}

            public void UpdateController(float dt) {
                HashSet<CollisionTask> doneTasks = new HashSet<CollisionTask>();
                foreach (CollisionTask task in m_collisionTasks) {
                    ICollider collided = task.CheckCollision();
                    if (null != collided) {
                        m_gameController.OnComponentProcessed(this, collided);
                        doneTasks.Add(task);
                    }
                }
                //remove the ones that have been done
                foreach (CollisionTask done in doneTasks) {
                    m_collisionTasks.Remove(done);
                }
            }

            public void OnStartGame() {}

            public void OnExitGame() {}
        }
    }
}