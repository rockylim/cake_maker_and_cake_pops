﻿using UnityEngine;
using UnityEngine.UI;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdScreenController : MonoBehaviour, ISceneController {
            public CanvasScaler m_mainCanvasScaler;
            public CanvasScaler.ScreenMatchMode m_screenModeIphoneX = CanvasScaler.ScreenMatchMode.Expand;
            public RectMask2D m_mainMask;
            public RectTransform m_gameMainGroup;

            public void InitSceneController(ISceneStarter sceneStarter) {
                //enable the mask on start
                m_mainMask.enabled = true;
                float aspectRatio = (float)Screen.height / (float)Screen.width;
                if (aspectRatio <= 1.5f) {
                    m_mainCanvasScaler.screenMatchMode = m_screenModeIphoneX;
                    if (m_gameMainGroup != null) {
                        m_gameMainGroup.SetScale(new Vector2(1.11f, 1f));
                    }
                }
            }


            public void StartSceneController(ISceneStarter sceneStarter) {
            }

            public void UpdateSceneController(float dt) {
            }
        }
    }
}
