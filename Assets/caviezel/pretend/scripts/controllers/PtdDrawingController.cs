﻿using System.Collections.Generic;

namespace Caviezel {
    namespace Pretend {        
        public class PtdDrawingController : IComponentController {
            HashSet<PtdCanvas> m_canvases = new HashSet<PtdCanvas>();
            PtdDrawer m_drawer = null;

            public void OnProcessedComponent(IComponent comp) {}

            public void RetainComponent(IComponent comp) {
                PtdCanvas canvas = comp as PtdCanvas;
                if (null != canvas) {
                    m_canvases.Add(canvas);
                }
            }

            public void BeginInteraction(PtdObject obj) {
                m_drawer = obj.ComponentSystem.GetComponent<PtdDrawer>();
            }

            public void ProcessInteraction(PtdObject obj) {                
                m_drawer = null;
            }

            public void OnStartGame() { }

            public void OnExitGame() { }

            public void UpdateController(float dt) {
                if (null != m_drawer) {
                    foreach (PtdCanvas canvas in m_canvases) {
                        if (canvas.m_canvasArea.RectWorldPos().Intersects(m_drawer.m_drawArea.RectWorldPos())) {
                            canvas.Draw(m_drawer.m_drawArea.position, m_drawer.m_color);
                        }
                    }   
                }
            }
        }       
    }
}    
