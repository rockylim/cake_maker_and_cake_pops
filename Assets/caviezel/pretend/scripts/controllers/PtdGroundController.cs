﻿using System.Collections.Generic;

namespace Caviezel {
    namespace Pretend {
        public class PtdGroundController : IComponentController {
            HashSet<IGroundable> m_groundables = new HashSet<IGroundable>();
            HashSet<IGravityable> m_gravityables = new HashSet<IGravityable>();
            IGameController m_controller;

            public PtdGroundController(IGameController controller) {
                m_controller = controller;
            }

            #region IPtdObjectsController
            public void RetainComponent(IComponent item) {
                IGroundable groundable = item as IGroundable;
                if (null != groundable) {
                    m_groundables.Add(groundable);
                }
            }

            public void BeginInteraction(PtdObject obj) { }

            public void ProcessInteraction(PtdObject obj) {
                IGravityable gravityable = obj.ComponentSystem.GetComponent<IGravityable>();
                if (null != gravityable) {
                    gravityable.ShouldFall = true;
                    gravityable.Owner.IsInAction = true;
                    m_gravityables.Add(gravityable);  
                }
            }

            public void OnStartController() { }

            public void UpdateController(float dt) {
                foreach (IGravityable gravityable in m_gravityables) {
                    if (gravityable.ShouldFall) {
                        if (!gravityable.BeganLanded) {
                            //keep falling until landed
                            gravityable.OnFalling(dt);
                            //check whether it collides with the ground
                            foreach (IGroundable ground in m_groundables) {
                                //if it has been grounded
                                if (ground.IsGrounded(gravityable)) {
                                    gravityable.BeganLanded = true;
                                    break;
                                }
                            }
                        } else {
                            if (gravityable.HasGrounded(dt)) {
                                //set back shouldfall flag to false
                                gravityable.ShouldFall = false;
                                gravityable.Owner.IsInAction = false;
                                //tell the gc that the comp has been processed
                                m_controller.OnComponentProcessed(this, gravityable);
                            }
                        }
                    }
                }
            }

            public void OnProcessedComponent(IComponent comp) {                
                IGravityable compGrav = comp.Owner.ComponentSystem.GetComponent<IGravityable>();
                if (null != compGrav) {
                    compGrav.ShouldFall = false;
                    m_gravityables.Remove(compGrav);   
                }
            }

            public void OnStartGame() { }

            public void OnExitGame() { }
            #endregion
        }
    }
}
