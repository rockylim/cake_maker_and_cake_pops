﻿using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public delegate bool OpenSceneCondition(string sceneId);

        public interface IAchievementSystem : ISystem {
            void AddAchievement(string id);
            bool HasObtainedGift(string id);
            void AddGiftObserver(IAchievementGiftObserver observer);
        }

        public interface IAchievementGiftObserver {
            void OnObtainedGift(string id);
        }

        #region Util
        public static class Debugger {
            public static void Log(string tag, string txt) {
#if UNITY_EDITOR
                if (Constants.IsDebugMode) {
                    UnityEngine.Debug.Log(string.Format("{0} ({1})", txt, tag));
                }
#endif
            }
        }
        #endregion
    }
}