﻿using System;
using System.Collections.Generic;

namespace Caviezel {
  namespace Pretend {
    [Serializable]
    public class PtdPersisterData {
      public List<string> m_productIds;
      public bool m_stopRateGame;
      public float m_soundVolume;
      public float m_bgmVolume;
      public List<SceneData> m_sceneData;
      public SaveObjectData m_records;
      public SaveObjectData m_processedRecords;

      public PtdPersisterData() {
        m_soundVolume = 1f;
        m_bgmVolume = 1f;
        m_productIds = new List<string>();
        ResetData();
      }

      public void ResetData() {
        m_sceneData = new List<SceneData>();
        m_records = new SaveObjectData(SaveKeys.Record);
        m_processedRecords = new SaveObjectData(SaveKeys.ProcessedRecord);
      }
    }

    [Serializable]
    public class SceneData {
      public string m_sceneId;
      public List<SaveObjectData> m_saveables;

      public SceneData(string sceneId) {
        m_sceneId = sceneId;
        m_saveables = new List<SaveObjectData>();
      }
    }

    [Serializable]
    public class KVData {
      public string k;
      public string v;

      public KVData(string key) {
        k = key;
        v = string.Empty;
      }

      public KVData(string key, string val) {
        k = key;
        Add(val);
      }

      public KVData(string key, int val) {
        k = key;
        Add(val);
      }

      public void Add(int val) {
        v = val.ToString();
      }

      public void Add(string val) {
        v = val;
      }
    }

    [Serializable]
    public class SaveObjectData {
      public string id;
      public List<KVData> kv = new List<KVData>();

      public SaveObjectData(string theId) {
        id = theId;
      }

      public int Count() {
        return kv.Count;
      }

      public void Clear() {
        kv.Clear();
      }

      public void AddPair(KVData data) {
        kv.Add(data);
      }

      public void InsertPair(KVData data) {
        KVData pair = null;
        if (TryGetPair(data.k, out pair)) {
          pair.v = data.v;
        } else {
          AddPair(data);
        }
      }

      public void RemovePair(string key) {
        KVData pair = null;
        if (TryGetPair(key, out pair)) {
          kv.Remove(pair);
        }
      }

      public bool RemovePairWithValue(string key, string value) {
        KVData pair = null;
        if (TryGetPairWithValue(key, value, out pair)) {
          return kv.Remove(pair);
        }
        return false;
      }

      public bool TryGetPair(string key, out KVData pair) {
        pair = null;
        for (int i = 0; i < kv.Count; ++i) {
          if (kv[i].k.IsEqual(key)) {
            pair = kv[i];
            return true;
          }
        }
        return false;
      }

      public bool TryGetPairWithValue(string key, string value, out KVData pair) {
        pair = null;
        for (int i = 0; i < kv.Count; ++i) {
          if (kv[i].k.IsEqual(key) && kv[i].v.IsEqual(value)) {
            pair = kv[i];
            return true;
          }
        }
        return false;
      }

      public void AddPairWithValue(string key, string value) {
        KVData pair = null;
        if (!TryGetPairWithValue(key, value, out pair)) {
          AddPair(new KVData(key, value));
        }
      }

      public bool TryGetString(string key, out string strVal) {
        strVal = string.Empty;
        KVData pair = null;
        if (TryGetPair(key, out pair)) {
          strVal = pair.v;
          return true;
        }
        return false;
      }

      public bool TryGetInt(string key, out int intVal) {
        intVal = -1;
        KVData pair = null;
        if (TryGetPair(key, out pair)) {
          if (int.TryParse(pair.v, out intVal)) {
            return true;
          }
        }
        return false;
      }

      public List<string> GetStrings(string key) {
        List<string> values = new List<string>();
        for (int i = 0; i < kv.Count; ++i) {
          if (kv[i].k.IsEqual(key)) {
            values.Add(kv[i].v);
          }
        }
        return values;
      }
    }
  }
}