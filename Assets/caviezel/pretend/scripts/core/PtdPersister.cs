﻿using System.IO;
using Caviezel.Serialization;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class PtdPersister : MonoBehaviour, IPersister<PtdPersisterData> {
      PtdPersisterData m_data;
      SceneData m_curSceneData;
      SaveObjectData m_temp;

      public bool StopRateGame {
        get { return m_data.m_stopRateGame; }
        set { m_data.m_stopRateGame = value; }
      }

      public float SoundVolume {
        get { return m_data.m_soundVolume; }
        set { m_data.m_soundVolume = value; }
      }

      public float BGMVolume {
        get { return m_data.m_bgmVolume; }
        set { m_data.m_bgmVolume = value; }
      }

      public void SetProduct(string id) {
        if (!m_data.m_productIds.Contains(id)) {
          m_data.m_productIds.Add(id);
        }
      }

      public bool ProductExists(string id) {
        return m_data.m_productIds.Contains(id);
      }

      public bool HasAnyProduct() {
        return m_data.m_productIds.Count > 0;
      }

      public void SetSceneData(string id) {
        m_curSceneData = null;
        for (int i = 0; i < m_data.m_sceneData.Count; ++i) {
          if (m_data.m_sceneData[i].m_sceneId.IsEqual(id)) {
            m_curSceneData = m_data.m_sceneData[i];
          }
        }
        //add if not exist
        if (null == m_curSceneData) {
          m_curSceneData = new SceneData(id);
          m_data.m_sceneData.Add(m_curSceneData);
        }
      }

      public void ClearSaveables() {
        m_curSceneData.m_saveables.Clear();
      }

      public void FlushSaveObjects() {
        m_data.ResetData();
      }
      public void FlushTempRecord() {
        m_temp = new SaveObjectData(SaveKeys.Temp);
      }

      public SaveObjectData GetObjectData(string id) {
        if (null != m_curSceneData) {
          for (int i = 0; i < m_curSceneData.m_saveables.Count; ++i) {
            if (m_curSceneData.m_saveables[i].id.IsEqual(id)) {
              return m_curSceneData.m_saveables[i];
            }
          }
        }
        return null;
      }

      public void SetObjectData(SaveObjectData data) {
        m_curSceneData.m_saveables.Add(data);
      }

      public SaveObjectData GetRecord() {
        return m_data.m_records;
      }

      public SaveObjectData GetProcessedRecord() {
        return m_data.m_processedRecords;
      }

      public SaveObjectData GetTemp() {
        return m_temp;
      }

      public static string GetPath() {
        return Path.Combine(Application.persistentDataPath, "ptddata.txt");
      }

      #region IPersister
      public void Init(IDataSerializer serializer) {
        m_data = serializer.Load<PtdPersisterData>(GetPath());
        if (null == m_data) {
          Debug.Log("ptd data doesnt exist, create new");
          m_data = new PtdPersisterData();
          serializer.Save(m_data, GetPath());
        }
        //init cookie
        FlushTempRecord();
      }

      public void Reset(IDataSerializer serializer) { }

      public void Save(IDataSerializer serializer) {
        serializer.Save(m_data, GetPath());
      }

      public void StartPersister(GamePersisters persisters) {
      }
      #endregion
    }
  }
}