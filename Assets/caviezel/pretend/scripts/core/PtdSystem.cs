﻿using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdSystem : MonoBehaviour, ISystem {
            public OpenSceneCondition OpenSceneConditions { get; set; }

            #region ISystem
            public virtual void InitSystem() { }

            public virtual void StartSystem(CavEngine gameEngine) { }

            public virtual void UpdateSystem(float dt) { }

            public virtual void OnChangeScene(int index) { }
            #endregion

            public virtual bool CanOpenScene(string id) {
                if (null == OpenSceneConditions) {
                    return true;
                }
                return OpenSceneConditions(id);
            }
        }
    }
}
