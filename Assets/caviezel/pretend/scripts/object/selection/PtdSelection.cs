﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel.UI;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        [RequireComponent(typeof(RectTransform))]
        public class PtdSelection : PtdComponentActive, IUpdateableComponent {
            public SwipeAxis m_axis;
            public Vector2 m_limit;
            public Image m_reference;
            public string m_spriteFolder;
            public float m_maxSize;
            public bool m_hasSelectedEv;
            [ConditionalHide("m_hasSelectedEv", true)]
            public string m_senderId;

            RectTransform m_selectionArea;
            Dictionary<string, RectTransform> m_items = new Dictionary<string, RectTransform>();
            CScrollable m_scrollable;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //init scrollable
                m_selectionArea = GetComponent<RectTransform>();
                m_scrollable = new CScrollable(m_selectionArea, m_axis, m_limit);
                //init gesture handler
                Owner.InitGestureHandler();
                Owner.Gestures.SetTappable();
                Owner.Gestures.OnTapListeners += OnTap;
                Owner.Gestures.SetDraggable();
                Owner.Gestures.OnStartSwipeListeners += OnBeginScroll;
                Owner.Gestures.OnSwipingListeners += OnScroll;
                Owner.Gestures.OnEndSwipeListeners += OnEndScroll;
                //load all sprites in the path
                Sprite[] sprites = Resources.LoadAll<Sprite>(m_spriteFolder);
                //render the items
                for (int i = 0; i < sprites.Length; ++i) {
                    Image image = null;
                    if (i == 0) {
                        image = m_reference;
                    } else {
                        GameObject newObj = Instantiate(m_reference.gameObject) as GameObject;
                        newObj.name = m_reference.name + i.ToString();
                        newObj.transform.SetParent(m_selectionArea, false);
                        image = newObj.GetComponent<Image>();
                    }
                    //set the sprite
                    image.raycastTarget = false;
                    image.sprite = sprites[i];
                    if (m_maxSize > 0f) {
                        image.SetMaxSize(m_maxSize);
                    }
                    m_items.Add(sprites[i].name, image.rectTransform);
                }
            }

            public void UpdateComponent(float dt) {
                m_scrollable.UpdateScroll(dt);
            }

            public void OnTap(Vector2 pos) {
                if (m_items.Count == 0) {
                    return;
                }
                Vector2 localPos = Vector2.zero;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(m_selectionArea, pos, null, out localPos);
                //get the nearest distance to the tapped pos
                string tapped = string.Empty;
                float nearest = 200f;
                foreach (var item in m_items) {
                    float candidate = Vector2.Distance(localPos, item.Value.localPosition);
                    if (candidate < nearest) {
                        //we got a new nearest dist
                        nearest = candidate;
                        tapped = item.Key;
                    }
                }
                //if something is tapped
                if (!string.IsNullOrEmpty(tapped) && m_hasSelectedEv) {
                    BroadcastEvent(new BaseSenderEv(m_senderId, tapped));
                }
            }

            public void OnBeginScroll(SwipeEventData ev) {
                m_scrollable.OnBeginScroll();
            }

            public void OnScroll(SwipeEventData ev) {
                m_scrollable.OnScroll(ev.Delta);
            }

            public void OnEndScroll(SwipeEventData ev) {
                m_scrollable.OnEndScroll();
            }
        }
    }
}