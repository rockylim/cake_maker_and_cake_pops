﻿using System;
using System.Collections.Generic;
using Caviezel.Core;
using Caviezel.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Caviezel {
  namespace Pretend {
    [RequireComponent(typeof(RectTransform))]
    public class PtdSelectionWithContainer : PtdComponentActive, IUpdateableComponent {
      public SwipeAxis m_axis;
      public Vector2 m_limit;
      public GameObject m_reference;
      public string m_spriteFolder;
      public float m_maxSize;
      public bool m_hasSelectedEv;
      [ConditionalHide("m_hasSelectedEv", true)]
      public string m_senderId;

      #region IAP

      [Header("IAP CUSTOMIZATION")]
      public bool m_hasIAPLock;
      [ConditionalHide("m_hasIAPLock", true)]
      public Sprite m_lockImage;
      [ConditionalHide("m_hasIAPLock", true)]
      public float m_lockImageNativeSizeScale;
      [ConditionalHide("m_hasIAPLock", true)]
      public Vector2 m_lockPos;
      [ConditionalHide("m_hasIAPLock", true)]
      public int m_lockedStartIndex;
      [ConditionalHide("m_hasIAPLock", true)]
      public BaseSenderEv m_onLockedEv;
      [ConditionalHide("m_hasIAPLock", true)]
      public List<BaseReceiverEv> m_onUnlockedEv;
      #endregion
      RectTransform m_selectionArea;
      List<GameObject> m_containers = new List<GameObject>();
      CScrollable m_scrollable;


      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        //init scrollable
        m_selectionArea = GetComponent<RectTransform>();
        m_scrollable = new CScrollable(m_selectionArea, m_axis, m_limit);
        //init gesture handler
        Owner.InitGestureHandler();
        Owner.Gestures.SetTappable();
        Owner.Gestures.OnTapListeners += OnTap;
        Owner.Gestures.SetDraggable();
        Owner.Gestures.OnStartSwipeListeners += OnBeginScroll;
        Owner.Gestures.OnSwipingListeners += OnScroll;
        Owner.Gestures.OnEndSwipeListeners += OnEndScroll;

        //load all sprites in the path
        Sprite[] sprites = Resources.LoadAll<Sprite>(m_spriteFolder);
        Array.Sort(sprites, delegate (Sprite x, Sprite y) { return GetIndex(x.name).CompareTo(GetIndex(y.name)); });
        //render the items
        for (int i = 0; i < sprites.Length; ++i) {
          GameObject container = null;
          if (i == 0) {
            container = m_reference;
          } else {
            container = Instantiate(m_reference) as GameObject;
            container.transform.SetParent(m_selectionArea, false);
          }
          //set the sprite
          Image image = container.transform.GetChild(0).gameObject.GetComponent<Image>();
          image.raycastTarget = false;
          image.sprite = sprites[i];
          if (m_maxSize > 0f) {
            image.SetMaxSize(m_maxSize);
          }
          container.name = sprites[i].name;
          m_containers.Add(container);

          if ((m_hasIAPLock) && (m_lockImage != null)) {
            if (i >= m_lockedStartIndex) {
              GameObject lockObject = new GameObject("Lock");
              lockObject.AddComponent<RectTransform>();
              lockObject.AddComponent<Image>();
              Image lockImage = lockObject.GetComponent<Image>();
              RectTransform rectImage = lockObject.GetComponent<RectTransform>();
              lockImage.sprite = m_lockImage;
              lockImage.SetNativeSize();
              rectImage.SetWidth(rectImage.Width() * m_lockImageNativeSizeScale);
              rectImage.SetHeight(rectImage.Height() * m_lockImageNativeSizeScale);
              lockObject.transform.SetParent(container.transform);
              rectImage.SetScale(new Vector2(1f, 1f));
              RectTransform containerRect = container.GetComponent<RectTransform>();
              rectImage.SetPos(m_lockPos);
            }
          }
        }
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_onUnlockedEv.Count; ++i) {
            if (m_onUnlockedEv[i].Matches(ev, scope)) {
              m_hasIAPLock = false;
              for (int j = 0; j < m_containers.Count; j++) {
                if (j >= m_lockedStartIndex) {
                  GameObject container = m_containers[j];
                  container.transform.GetChild(1).gameObject.SetActive(false);
                }
              }
              return true;
            }
          }
        }
        return false;
      }

      public void UpdateComponent(float dt) {
        m_scrollable.UpdateScroll(dt);
      }

      public void OnTap(Vector2 pos) {
        if (m_containers.Count == 0) {
          return;
        }

        Vector2 localPos = Vector2.zero;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(m_selectionArea, pos, null, out localPos);
        //get the nearest distance to the tapped pos
        string tapped = string.Empty;
        float nearest = 200f;
        int indexTapped = -99;
        foreach (var container in m_containers) {
          float candidate = Vector2.Distance(localPos, container.transform.localPosition);
          if (candidate < nearest) {
            //we got a new nearest dist
            nearest = candidate;
            tapped = container.name;
            indexTapped = m_containers.IndexOf(container);
          }
        }

        if (m_hasIAPLock) {
          if (indexTapped >= m_lockedStartIndex) {
            BroadcastEvent(m_onLockedEv);
            return;
          }
        }

        //if something is tapped
        if (!string.IsNullOrEmpty(tapped) && m_hasSelectedEv) {
          BroadcastEvent(new BaseSenderEv(m_senderId, tapped));
        }
      }

      public void OnBeginScroll(SwipeEventData ev) {
        m_scrollable.OnBeginScroll();
      }

      public void OnScroll(SwipeEventData ev) {
        m_scrollable.OnScroll(ev.Delta);
      }

      public void OnEndScroll(SwipeEventData ev) {
        m_scrollable.OnEndScroll();
      }

      int GetIndex(string objectName) {
        return int.Parse(objectName.Substring(objectName.LastIndexOf("_") + 1));
      }
    }
  }
}