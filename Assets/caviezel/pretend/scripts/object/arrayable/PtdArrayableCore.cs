﻿using System;

namespace Caviezel {
    namespace Pretend {
        [Serializable]
        public class ArrayImageData {
            public string m_id;
            public SpriteData[] m_valueData;
        }
    }
}