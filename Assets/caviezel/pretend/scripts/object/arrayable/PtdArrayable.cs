﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel.Tween;

namespace Caviezel {
    namespace Pretend {
        public class PtdArrayable : PtdComponentActive, IUpdateableComponent, ITweenTarget<TweenAnimData> {
            public ArrayableEv[] m_keyEvs;
            public ArrayableEv[] m_valueEvs;
            public SelectArrayEv[] m_selectKeyEvs;
            public ArrayableAnimEv[] m_animEvs;
            public Image m_target;
            public ArrayImageData[] m_keyData;
            public float m_maxSize;
            public string m_defaultKeyId;
            public string m_defaultValueId;
            public float m_onAppearAlpha = 1f;

            Dictionary<string, Dictionary<string, Sprite>> m_collections = new Dictionary<string, Dictionary<string, Sprite>>();
            Vector2 m_originScale;
            float m_originAlpha;
            SpriteData[] m_curValueData = null;
            CTween<TweenAnimData> m_animTween;

            public string Key { get; set; }

            public string Value { get; set; }

            public override void InitComponent(Core.ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //set defaults
                Key = m_defaultKeyId;
                Value = m_defaultValueId;
                //init the dictionary
                for (int i = 0; i < m_keyData.Length; ++i) {
                    Dictionary<string, Sprite> collection = new Dictionary<string, Sprite>();
                    foreach (SpriteData spriteData in m_keyData[i].m_valueData) {
                        collection.Add(spriteData.m_id, spriteData.m_sprite);
                    }
                    m_collections.Add(m_keyData[i].m_id, collection);
                }
                //cache the origin state of the target
                m_originAlpha = m_target.Alpha();
                m_originScale = m_target.rectTransform.Scale();
                //render
                Render(true);
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //iterate over the key evs
                    for (int i = 0; i < m_keyEvs.Length; ++i) {
                        if (m_keyEvs[i].Matches(ev, scope)) {
                            Key = ev.m_eventId;
                            Owner.ObjectData.InsertPair(new KVData(SaveKeys.ArrayableKey + ComponentId, Key));
                            Render();
                            return true;
                        }
                    }
                    //iterate over the value evs
                    for (int i = 0; i < m_valueEvs.Length; ++i) {
                        if (m_valueEvs[i].Matches(ev, scope)) {
                            Value = ev.m_eventId;
                            Owner.ObjectData.InsertPair(new KVData(SaveKeys.ArrayableKey + ComponentId, Value));
                            Render();
                            return true;
                        }
                    }
                    //iterate over select key evs
                    for (int i = 0; i < m_selectKeyEvs.Length; ++i) {
                        if (m_selectKeyEvs[i].Matches(ev, scope)) {
                            Key = m_selectKeyEvs[i].GetSelected();
                            Owner.ObjectData.InsertPair(new KVData(SaveKeys.ArrayableKey + ComponentId, Key));
                            Owner.ObjectData.InsertPair(new KVData(SaveKeys.ArrayableSelectKey + ComponentId, i));
                            Render();
                            return true;
                        }
                    }
                    //anim ev
                    for (int i = 0; i < m_animEvs.Length; ++i) {
                        if (null != m_curValueData && m_animEvs[i].Matches(ev, scope)) {
                            m_animTween = new CTween<TweenAnimData>(this, name);
                            TweenLerpData[] nodes = new TweenLerpData[m_curValueData.Length];
                            for (int j = 0; j < nodes.Length; ++j) {
                                nodes[j] = new TweenLerpData(0f, 1f, m_animEvs[i].m_duration);
                            }
                            m_animTween.Play(nodes, new LoopData(0));
                            return true;
                        }
                    }
                }
                return false;
            }

            public override void Reinit() {
                base.Reinit();
                m_target.SetAlpha(m_originAlpha);
                m_target.rectTransform.SetScale(m_originScale);
            }

            public override void OnLoaded() {
                base.OnLoaded();
                //load key and val
                string key;
                if (Owner.ObjectData.TryGetString(SaveKeys.ArrayableKey + ComponentId, out key)) {
                    Key = key;
                    Render();
                }
                string val;
                if (Owner.ObjectData.TryGetString(SaveKeys.ArrayableValue + ComponentId, out val)) {
                    Value = val;
                    Render();
                }
                //update selected idx
                int selectedIdx;
                if (Owner.ObjectData.TryGetInt(SaveKeys.ArrayableSelectKey + ComponentId, out selectedIdx)) {
                    m_selectKeyEvs[selectedIdx].SetSelected(Key);
                }
            }

            public void Render(bool isInit = false) {
                if (m_collections.ContainsKey(Key)) {
                    Dictionary<string, Sprite> collection = m_collections[Key];
                    Sprite sprite = null;
                    if (collection.TryGetValue(Value, out sprite)) {
                        m_target.sprite = sprite;
                        if (!isInit) {
                            if (m_maxSize > 0f) {
                                m_target.SetMaxSize(m_maxSize);
                            }
                            m_target.SetAlpha(m_onAppearAlpha);
                        }
                    }
                    //set cur value data
                    for (int i = 0; i < m_keyData.Length; ++i) {
                        if (m_keyData[i].m_id.IsEqual(Key)) {
                            m_curValueData = m_keyData[i].m_valueData;
                            break;
                        }
                    }
                }
            }

            public void UpdateComponent(float dt) {
                if (null != m_animTween && !m_animTween.UpdateTween(dt)) {
                    m_animTween = null;
                }
            }

            #region ITweenTarget
            public TweenAnimData GetTargetData(TweenAnimData data) {
                data.TargetData = new float[] { 0f };
                return data;
            }

            public void SetTargetData(TweenAnimData data) {
                if (null != m_curValueData) {
                    //set the sprite
                    m_target.sprite = m_curValueData[data.CurIdx].m_sprite;
                }
            }
            #endregion
        }
    }
}
