﻿using UnityEngine;
using Caviezel.Tween;

namespace Caviezel {
    namespace Pretend {
        public class PtdStickable : PtdComponentActive, IUpdateableComponent {
            public StickableEv[] m_stickEvs;

            ITween m_tweenMove;

            const Easing m_moveEase = Easing.OutQuad;

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //check stickable ev
                    for (int i = 0; i < m_stickEvs.Length; ++i) {
                        if (m_stickEvs[i].Matches(ev, scope)) {
                            CollisionSenderEv collisionEv = ev as CollisionSenderEv;
                            DoStick(
                                m_stickEvs[i]
                                , collisionEv.m_data.m_otherCollider.Owner
                                , collisionEv.m_data.m_otherColliderData.m_area
                                , collisionEv.m_data.m_collider.Owner
                                , collisionEv.m_data.m_colliderData.m_area
                            );
                            //save
                            Owner.ObjectData.RemovePair(SaveKeys.StickToOther);
                            Owner.ObjectData.InsertPair(new KVData(SaveKeys.ColliderArea, collisionEv.m_data.m_otherColliderData.m_id));
                            break;
                        }
                    }
                }
                return false;
            }

            public void UpdateComponent(float dt) {
                if (null != m_tweenMove && !m_tweenMove.UpdateTween(dt)) {
                    Owner.IsInAction = false;
                    m_tweenMove = null;
                }
            }

            void DoStick(StickableEv ev, PtdObject parent, RectTransform parentArea, PtdObject child, RectTransform childArea) {
                Owner.IsInAction = true;
                //check from our dict
                bool centerToParent = ev.m_centerToParent;
                //if position stays, check the area and animate accordingly
                if (!centerToParent) {
                    m_tweenMove = new CTweenMove(
                        child.gameObject, child.RootTransform.Pos() + ev.m_offset, ev.m_duration, m_moveEase)
                        .SetCompleteCallback(
                            () => {
                                child.RootTransform.SetParent(parentArea);
                                //broadcast ev                                   
                                BroadcastEvent(ev.m_onStickedEv);
                            }
                        )
                        .Start();
                } else {
                    //stick to the area being collided on collision first                 
                    child.RootTransform.SetParent(parentArea);
                    //animate it then
                    float stickToParentDuration = ev.m_duration;
                    //force to 0.2f if it's 0f for now
                    if (Mathf.Approximately(stickToParentDuration, 0f)) {
                        stickToParentDuration = 0.2f;
                    }
                    m_tweenMove = new CTweenMove(child.gameObject, ev.m_offset - childArea.Pos(), stickToParentDuration, m_moveEase)
                        .SetCompleteCallback(
                            () => {
                                //broadcast ev                                   
                                BroadcastEvent(ev.m_onStickedEv);
                            }
                        )
                        .Start();
                }
                //set the parent to the collider
                child.SetParent(parent);
            }
        }
    }
}
