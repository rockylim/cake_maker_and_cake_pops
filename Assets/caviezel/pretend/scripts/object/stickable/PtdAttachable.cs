﻿namespace Caviezel {
    namespace Pretend {
        public class PtdAttachable : PtdComponentActive, IDataProvider<ParentAreaData> {
            public AttachEv[] m_attachEvs;

			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //check stick to other
                    for (int i = 0; i < m_attachEvs.Length; ++i) {
                        if (m_attachEvs[i].Matches(ev, scope)) {
                            //do attach
                            Owner.SetParent(m_attachEvs[i].m_parent);
                            Owner.RootTransform.SetParent(m_attachEvs[i].m_parentArea);
                            //save
                            Owner.ObjectData.RemovePair(SaveKeys.ColliderArea);
                            Owner.ObjectData.InsertPair(new KVData(SaveKeys.StickToOther, m_attachEvs[i].m_eventId));
                            //broadcast
                            BroadcastEvent(m_attachEvs[i].m_onAttachedEv);
                            return true;
                        }
                    }
                }
                return false;
			}

            #region IDataProvider
            public ParentAreaData ProvideData(SaveObjectData data) {
                //check whether the component data has same owner with us
                if (Owner.ObjectId.IsEqual(data.id)) {
                    string eventId;
                    if (data.TryGetString(SaveKeys.StickToOther, out eventId)) {
                        for (int i = 0; i < m_attachEvs.Length; ++i) {
                            if (m_attachEvs[i].m_eventId.IsEqual(eventId)) {                                
                                return new ParentAreaData(m_attachEvs[i].m_parentArea);
                            }
                        }
                    }
                }
                return null;
            }
            #endregion
		}       
    }
}
