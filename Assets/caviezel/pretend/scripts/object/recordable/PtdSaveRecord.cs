﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.Serialization;

namespace Caviezel {
    namespace Pretend {
        public class PtdSaveRecord : PtdComponentActive {
            public BaseReceiverEv m_shouldSaveEvs;
            GamePersisters m_persister;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //get persister
                m_persister = sceneStarter.Engine.GetSystem<GamePersisters>();
                Debug.Assert(null != m_persister);
            }

            public override void OnStartGame() {
                base.OnStartGame();
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    if (null != m_shouldSaveEvs) {
                        if (m_shouldSaveEvs.Matches(ev, scope)) {
                            m_persister.SaveAll();
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }
}
