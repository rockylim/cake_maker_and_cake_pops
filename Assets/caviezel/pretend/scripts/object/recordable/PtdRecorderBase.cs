﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.Serialization;

namespace Caviezel {
    namespace Pretend {
        public  abstract class PtdRecorderBase : PtdComponentActive {
            public RecordEv[] m_recordEvs;

            PtdPersister m_persister;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //get persister
                m_persister = sceneStarter.Engine.GetSystem<GamePersisters>().GetPersister<PtdPersister>();
                Debug.Assert(null != m_persister);
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_recordEvs.Length; ++i) {
                        if (m_recordEvs[i].Matches(ev, scope)) {
                            bool isRecorded = false;
                            foreach (string recordId in m_recordEvs[i].m_recordIds) {
                                SaveObjectData data = m_recordEvs[i].m_isTemporary ? m_persister.GetTemp() : m_persister.GetRecord();
								Record(recordId, data, ev);
                                isRecorded = true;
                            }
                            //broadcast the event on recorded
                            if (isRecorded) {
                                BroadcastEvent(m_recordEvs[i].m_onRecordedEv);
                            }
                        }
                    }
                }
                return false;
            }

			public abstract void Record(string recordId, SaveObjectData saveObjectData, BaseSenderEv ev);
        }
    }
}
