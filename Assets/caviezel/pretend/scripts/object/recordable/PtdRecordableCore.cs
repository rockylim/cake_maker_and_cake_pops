﻿using System;

namespace Caviezel {
	namespace Pretend {
		[Serializable]
        public class TrackerEv {
            public string m_recordId;            
            public bool m_isTemporary;
            public BaseSenderEv m_onNotFoundEv;
        }

		[Serializable]
		public class TrackNumberEv : TrackerEv {
			public int m_threshold;
            public bool m_isRepeatable;
			public BaseSenderEv m_onDoneEv;            
        }
 
		[Serializable]
        public class RecordEv : BaseReceiverEv {
            public string[] m_recordIds;
            public bool m_isTemporary;
            public BaseSenderEv m_onRecordedEv;
        }
	}
}
