﻿namespace Caviezel {
	namespace Pretend {
		public class PtdRecordString : PtdRecorderBase {			
			public override void Record(string recordId, SaveObjectData saveObjectData, BaseSenderEv ev) {				
				saveObjectData.InsertPair(new KVData(recordId, ev.m_eventId));
			}
        }		
	}
}
