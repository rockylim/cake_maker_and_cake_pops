﻿namespace Caviezel {
    namespace Pretend {
		public class PtdRecordNumber : PtdRecorderBase {                    
			public override void Record(string recordId, SaveObjectData saveObjectData, BaseSenderEv ev) {
				int recordVal;
				if (saveObjectData.TryGetInt(recordId, out recordVal)) {
                    ++recordVal;
                } else {
                    recordVal = 1;
                }
                //insert pair
				saveObjectData.InsertPair(new KVData(recordId, recordVal));
			}
		}       
    }
}
