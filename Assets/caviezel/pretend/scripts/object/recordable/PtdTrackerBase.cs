﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.Serialization;

namespace Caviezel {
	namespace Pretend {
		public abstract class PtdTrackerBase : PtdComponentActive {			
			public BaseReceiverEv[] m_shouldTrackEvs;

			protected PtdPersister m_persister;

			public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
				base.InitComponent(sceneStarter, gameController);
				//get persister
				m_persister = sceneStarter.Engine.GetSystem<GamePersisters>().GetPersister<PtdPersister>();
				Debug.Assert(null != m_persister);
			}

			public override void OnStartGame() {
				base.OnStartGame();
				//do track on loaded
				Track();
			}

			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
				base.OnReceiveEvent(ev, scope);
				if (IsActive) {
					if (null != m_shouldTrackEvs) {
						for (int i = 0; i < m_shouldTrackEvs.Length; ++i) {
							if (m_shouldTrackEvs[i].Matches(ev, scope)) {
								Track();
								return true;
							}
						}
					}
				}
				return false;
			}

			public abstract void Track();            			           
		}
	}
}
