﻿using System;
using Caviezel.Core;
using Caviezel.Pretend;

namespace Caviezel {
    namespace Pretend {
        public class PtdRandomNumber : PtdComponentActive {
            public RandomNumberEvent[] m_randomNumberEvs;
            public bool m_randomOnStart;
            public int m_minNumber;
            public int m_maxNumber;

            [Serializable]
            public class RandomNumberEvent : BaseReceiverEv {
                public RandomNumberSenderEv m_onRandomNumberEv;
            }

            [Serializable]
            public class RandomNumberSenderEv : BaseSenderEv {
                public RandomNumberSenderEv(BaseSenderEv other) : base(other) { }
                public int m_minNumber;
                public int m_maxNumber;
            }

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
            }

            public override void OnStartGame() {
                base.OnStartGame();
                if ((m_randomOnStart) && (IsActive)) {
                    int rand = GetRandom(m_minNumber, m_maxNumber);
                    BaseSenderEv ev_send = new BaseSenderEv(this.gameObject.name, "ev_startrandom" + rand.ToString());
                    BroadcastEvent(ev_send);
                }
            }


            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_randomNumberEvs.Length; ++i) {
                        if (m_randomNumberEvs[i].Matches(ev, scope)) {
                            int rand = GetRandom(m_randomNumberEvs[i].m_onRandomNumberEv.m_minNumber, m_randomNumberEvs[i].m_onRandomNumberEv.m_maxNumber);
                            GenerateRandomEvent(m_randomNumberEvs[i], rand);
                            return true;
                        }
                    }
                }
                return false;
            }

            int GetRandom(int min, int max) {
                Random rand = Caviezel.MathExtensions.GetRandom();
                int number = rand.Next(min, max + 1);
                return number;
            }

            void GenerateRandomEvent(RandomNumberEvent ev, int number) {
                BaseSenderEv ev_send = new BaseSenderEv(ev.m_onRandomNumberEv.m_eventId + number.ToString());
                ev_send.m_senderId = ev.m_onRandomNumberEv.m_senderId;
                BroadcastEvent(ev_send);
            }
        }
    }
}