﻿namespace Caviezel {
    namespace Pretend {
        public interface IPuzzleSolveCondition {
            bool HasSolved { get; }
        }
    }
}