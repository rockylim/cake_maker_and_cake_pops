﻿using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        public class MovableUnit {
            RectTransform m_rectTransform;
            CPathwayable m_pathway;
            bool m_shouldLoop;

            public MovableUnit(RectTransform rt, Vector2[] points, float speed, bool shouldLoop) {
                m_rectTransform = rt;
                m_pathway = new CPathwayable(points, speed);
                m_pathway.SetProgress(rt.Pos());
                m_shouldLoop = shouldLoop;
            }

            public void Move(float dt) {
                if (m_pathway.Moving(dt)) {
                    m_rectTransform.SetPos(m_pathway.Cur);
                } else {
                    if (m_shouldLoop) {
                        m_pathway.Reset();   
                    }
                }
            }
        }       
    }
}
