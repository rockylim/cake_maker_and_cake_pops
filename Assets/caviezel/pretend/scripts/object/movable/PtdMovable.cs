﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdMovable : PtdComponentBase, IUpdateableComponent {
            public RectTransform[] m_pointReferences;
            public float m_speed = 0.3f;
            public bool m_shouldLoop;

            Vector2[] m_points;
            HashSet<MovableUnit> m_children = new HashSet<MovableUnit>();
            int m_childCount;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //create the points out of the references
                m_points = new Vector2[m_pointReferences.Length];
                for (int i = 0; i < m_points.Length; ++i) {
                    m_points[i] = m_pointReferences[i].Pos();
                }
            }

            public void UpdateComponent(float dt) {
                if (m_childCount != transform.childCount) {
                    m_childCount = transform.childCount;
                    m_children.Clear();
                    foreach (Transform child in gameObject.transform) {                        
                        RectTransform rt = child.GetComponent<RectTransform>();
                        if (null != rt) {
                            m_children.Add(new MovableUnit(rt, m_points, m_speed, m_shouldLoop));
                        }
                    }
                } else {
                    foreach (MovableUnit child in m_children) {
                        child.Move(dt);
                    }
                }                                             
            }
        }
    }
}
