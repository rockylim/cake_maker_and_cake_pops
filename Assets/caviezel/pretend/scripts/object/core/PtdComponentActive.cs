﻿using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdComponentActive : PtdComponentBase, IActiveComponent {
            [Tooltip("wont process the component's action if set to true")]
            public bool m_isInactive;
            public ActivatorEv[] m_activateEvs;

            IGameController m_gameController;

            public bool IsActive { get; set; }

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {                
                base.InitComponent(sceneStarter, gameController);
                //cache the gc
                m_gameController = gameController;
                //set the active flag
                IsActive = !m_isInactive;
            }

            public override void Reinit() {
                base.Reinit();
                //set the active flag
                IsActive = !m_isInactive;
            }

			public override void OnLoaded() {
				base.OnLoaded();
                //try load the active flag from the persistent data
                int active;
                if (Owner.ObjectData.TryGetInt(SaveKeys.IsActive + ComponentId, out active)) {
                    IsActive = (active == 1);
                }
			}

			public virtual bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                for (int i = 0; i < m_activateEvs.Length; ++i) {
                    if (m_activateEvs[i].Matches(ev, scope)) {
                        IsActive = m_activateEvs[i].m_flag;
                        KVData activeData = new KVData(SaveKeys.IsActive + ComponentId);                            
                        activeData.Add(IsActive ? 1 : 0);                            
                        Owner.ObjectData.InsertPair(activeData);   
                        return true;
                    }
                }

                return false;
            }

            public void BroadcastEvent(BaseSenderEv ev) {
                if (ev.IsSendable()) {
                    m_gameController.BroadcastEvent(Owner, ev);
                }
            }                               
        }       
    }
}
