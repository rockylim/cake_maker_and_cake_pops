﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Caviezel.Core;
using Caviezel.Audio;

namespace Caviezel {
    namespace Pretend {
        #region Component
        public interface IComponent {
            string ComponentId { get; set; }
            PtdObject Owner { get; set; }
            void InitComponent(ISceneStarter sceneStarter, IGameController gameController);
            void OnAwakeGame();
            void OnStartGame();
            void Reinit();
            void OnLoaded();
        }

        public interface IActiveComponent : IComponent {
            bool IsActive { get; set; }
            bool OnReceiveEvent(BaseSenderEv ev, EventScope scope);
            void BroadcastEvent(BaseSenderEv ev);
        }

        public interface IUpdateableComponent : IComponent {
            void UpdateComponent(float dt);
        }

        public interface IGravityable : IComponent {
            RectTransform GroundedArea { get; }
            Rect GroundBound { get; }
            bool ShouldFall { get; set; }
            bool BeganLanded { get; set; }
            void OnFalling(float dt);
            bool HasGrounded(float dt);
        }

        public interface IGroundable : IComponent {
            bool IsGrounded(IGravityable gravityable);
        }

        public interface IRespawnable : IComponent {
            bool HasRespawned { get; set; }
            bool Respawn(float dt);
        }

        public interface ICollider : IActiveComponent {
            IList<ColliderData> Colliders { get; }
            bool ShouldCollide(OnCollideData collideData);
        }

        public interface ILayerable : IComponent {
            RectTransform LayerArea { get; }
            float Pivot { get; }
            void SortLayer();
        }
        #endregion

        #region Receiver Events
        public enum EventScope {
            Internal,
            Global,
            Sibling
        }

        [Serializable]
        public class BaseReceiverEv {
            public EventScope m_type;
            public string m_eventId;
            public ComparisonType m_compareEvType;
            public string m_senderId;
            public ComparisonType m_compareSenderType;

            public bool Matches(BaseSenderEv sender, EventScope scope, bool ignoreScope = false) {
                if (ignoreScope) {
                    return sender.m_eventId.Matches(m_eventId, m_compareEvType);
                } else {
                    //should match the scope
                    if (m_type == scope) {
                        //ignore sender if it's internal scope
                        if (m_type == EventScope.Internal) {
                            return sender.m_eventId.Matches(m_eventId, m_compareEvType);
                        }
                        //otherwise if siblings or global scope, check both sender and event 
                        return sender.m_senderId.Matches(m_senderId, m_compareSenderType) && sender.m_eventId.Matches(m_eventId, m_compareEvType);
                    }
                }
                //not match
                return false;
            }
        }

        [Serializable]
        public class ActivatorEv : BaseReceiverEv {
            public bool m_flag;
        }

        [Serializable]
        public class VisibilityEv : ActivatorEv {
            [Tooltip("will toggle visibility of other object if it's not null, otherwise will do it for the parent where this component is attached to")]
            public GameObject m_otherObject;
        }

        [Serializable]
        public class ArrayableEv : BaseReceiverEv {
        }

        [Serializable]
        public class ArrayableAnimEv : BaseReceiverEv {
            public float m_duration;
        }

        [Serializable]
        public class SelectableEv : BaseReceiverEv {
            public int m_delta;
        }

        [Serializable]
        public class SelectArrayEv {
            public SelectableEv[] m_selectEvs;
            public string[] m_ids;

            int m_idx = 0;

            public bool Matches(BaseSenderEv ev, EventScope scope, bool ignoreScope = false) {
                for (int i = 0; i < m_selectEvs.Length; ++i) {
                    if (m_selectEvs[i].Matches(ev, scope, ignoreScope)) {
                        m_idx += m_selectEvs[i].m_delta;
                        m_idx = m_idx.Clamp(0, m_ids.Length - 1);
                        return true;
                    }
                }
                return false;
            }

            public void SetSelected(string key) {
                for (int i = 0; i < m_ids.Length; ++i) {
                    if (m_ids[i].IsEqual(key)) {
                        m_idx = i;
                    }
                }
            }

            public string GetSelected() {
                return m_ids[m_idx];
            }
        }

        [Serializable]
        public class PoolableEv : BaseReceiverEv {
        }

        [Serializable]
        public class CounterDeltaEv : BaseReceiverEv {
            public bool m_resetFirst;
            public int m_delta;
        }

        [Serializable]
        public class CounterMaxEv : BaseReceiverEv {
            public int m_max;
        }

        [Serializable]
        public class TextEv : BaseReceiverEv {
            public string m_text;
        }

        [Serializable]
        public class DestroyerEv : BaseReceiverEv {
            public Vector2 m_anchorFromCenter;
            [Tooltip("if not defined, will play the default one in PtdDestroyer instead")]
            public string m_destroyTweenId;
            public BaseSenderEv m_onDestroyedEv;
        }

        [Serializable]
        public class StickableEv : BaseReceiverEv {
            public bool m_centerToParent;
            [Tooltip("if it should center to parent, offset pos will offset from parent's center pos, otherwise it will offset from the current position")]
            public Vector2 m_offset;
            [Tooltip("time in sec of how long it animates towards the new sticking pos")]
            public float m_duration = 0f;
            public StickableSenderEv m_onStickedEv;
        }

        [Serializable]
        public class AttachEv : BaseReceiverEv {
            public PtdObject m_parent;
            public RectTransform m_parentArea;
            public BaseSenderEv m_onAttachedEv;
        }

        [Serializable]
        public struct TweenableData {
            public string m_tweenId;
            public bool m_shouldStop;
        }

        [Serializable]
        public class AudioEv : BaseReceiverEv {
            public SfxData[] m_sfxs;
        }

        [Serializable]
        public class VisualEv : BaseReceiverEv {
            public TweenableData[] m_data;
            [Tooltip("leave both sender and event ids empty if no on done tween ev should be sent")]
            public TweenSenderEv m_onDoneTweenEv;
        }

        [Serializable]
        public class AudioVisualEv : BaseReceiverEv {
            public TweenableData m_tween;
            public SfxData[] m_sfxs;
            [Tooltip("leave both sender and event ids empty if no on done tween ev should be sent")]
            public TweenSenderEv m_onDoneTweenEv;
        }

        [Serializable]
        public class NavigatorSceneEv : BaseReceiverEv {
            public string m_sceneId;
            public SfxData[] m_sfxs;
        }

        [Serializable]
        public class ScreenCaptureEv : BaseReceiverEv {
        }

        [Serializable]
        public class SequentialEv : BaseReceiverEv {
        }

        [Serializable]
        public class CloneableEv : BaseReceiverEv {
            public float m_nativeSizeRatio;
            public float m_maxSize;
        }

        [Serializable]
        public class RendererEv : BaseReceiverEv {
        }
        #endregion

        #region Sender Events
        [Serializable]
        public class BaseSenderEv {
            public string m_senderId;
            public string m_eventId;

            public BaseSenderEv(string eventId) {
                m_senderId = string.Empty;
                m_eventId = eventId;
            }

            public BaseSenderEv(string senderId, string eventId) {
                m_senderId = senderId;
                m_eventId = eventId;
            }

            public BaseSenderEv(BaseSenderEv other) {
                m_senderId = other.m_senderId;
                m_eventId = other.m_eventId;
            }

            public bool IsSendable() {
                if (string.IsNullOrEmpty(m_senderId) && string.IsNullOrEmpty(m_eventId)) {
                    return false;
                }
                return true;
            }

            public override string ToString() {
                return string.Format("Event ID = {0}, Sender ID = {1}", m_eventId, m_senderId);
            }
        }

        public enum DragType {
            Began,
            Dragging,
            Ended
        }

        [Serializable]
        public class DragSenderEv : BaseSenderEv {
            public DragType m_type;

            public DragSenderEv(BaseSenderEv other, DragType type) : base(other) {
                m_type = type;
            }
        }

        [Serializable]
        public class CollisionSenderEv : BaseSenderEv {
            public OnCollideData m_data;

            public CollisionSenderEv(BaseSenderEv other, OnCollideData data) : base(other) {
                m_data = data;
            }
        }

        [Serializable]
        public class TapSenderEv : BaseSenderEv {
            public SfxData[] m_sfxs;

            public TapSenderEv(BaseSenderEv other) : base(other) { }
        }

        public enum PressType {
            Hold,
            Release
        }

        [Serializable]
        public class PressSenderEv : BaseSenderEv {
            public PressType m_type;

            public PressSenderEv(BaseSenderEv other, PressType type) : base(other) {
                m_type = type;
            }
        }

        [Serializable]
        public class PoolableSenderEv : BaseSenderEv {
            public PoolableSenderEv(BaseSenderEv other) : base(other) { }
        }

        [Serializable]
        public class PuzzleableEv : BaseSenderEv {
            public PuzzleableEv(BaseSenderEv other) : base(other) { }
        }

        [Serializable]
        public class TweenSenderEv : BaseSenderEv {
            public TweenSenderEv(BaseSenderEv other) : base(other) { }
        }

        [Serializable]
        public class StickableSenderEv : BaseSenderEv {
            public StickableSenderEv(BaseSenderEv other) : base(other) { }
        }
        #endregion
    }
}