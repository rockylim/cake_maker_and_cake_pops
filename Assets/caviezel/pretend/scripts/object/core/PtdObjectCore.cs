﻿using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        #region Saveable
        public interface IDataProvider {            
        }

        public interface IDataProvider<T> : IDataProvider {
            T ProvideData(SaveObjectData data);
        }

        public class ParentAreaData {
            public RectTransform m_area;

            public ParentAreaData(RectTransform area) {
                m_area = area;
            }
        }

        public class SaveKeys {
            public static readonly string Record = "rec";
            public static readonly string ProcessedRecord = "processed_rec";
            public static readonly string Temp = "temporary";
            public static readonly string PosX = "x";
            public static readonly string PosY = "y";
            public static readonly string IsActive = "active";
            public static readonly string Parent = "parent";
            public static readonly string ColliderArea = "collider_area";
            public static readonly string StickToOther = "stick_to_other";
            public static readonly string ArrayableKey = "array_key";
            public static readonly string ArrayableValue = "array_value";
            public static readonly string ArrayableSelectKey = "select_key";
            public static readonly string LastPlayedTween = "last_tween";
            public static readonly string PoolableRelease = "pool_release";
            public static readonly string TappableIndex = "tap_idx";
        }
        #endregion
    }
}
