﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.Core;
using Caviezel.Generic;
using Caviezel.Tween;
using Caviezel.UI;

namespace Caviezel {
    namespace Pretend {
        public sealed class PtdObject : MonoBehaviour {
            public bool m_isSaveable;

            public string ObjectId { get; private set; }

            public PtdObject Parent { get; private set; }

            public bool IsInAction { get; set; }

            public bool IsDestroyed { get; set; }

            public RectTransform RootTransform { get; private set; }

            public CComponentable<IComponent> ComponentSystem { get; private set; }

            public CTweenChainer Tweener { get; private set; }

            public CGestureHandler Gestures { get; private set; }

            public SaveObjectData ObjectData { get; private set; }

            HashSet<IUpdateableComponent> m_updateableComps = new HashSet<IUpdateableComponent>();

            HashSet<IActiveComponent> m_activeComps = new HashSet<IActiveComponent>();

            IGameController m_gameController;

            public void Setup() {
                //set the obj id
                ObjectId = name;
                //set dummy obj data
                ObjectData = new SaveObjectData(ObjectId);
                //cache the rect transform
                RootTransform = GetComponent<RectTransform>();
                //init the component system
                ComponentSystem = new CComponentable<IComponent>(false);
                //instantiate the tweener
                Tweener = new CTweenChainer(TweenerType.Simultaneous);
                //setup all the comps and tweens in self
                SetupTweens(gameObject);
                SetupComponents(gameObject);
            }

            public void SetParent(PtdObject parent) {
                Parent = parent;
            }

            public void InitGestureHandler() {
                //instantiate the gesture handler if still null
                if (null == Gestures) {                    
                    Gestures = new CGestureHandler(RootTransform.gameObject);   
                }
            }

            public void InitObject(ISceneStarter sceneStarter, IGameController controller) {
                //cache the controller
                m_gameController = controller;
                //let the gamecontroller retain the components accordingly once everything has been set
                for (int i = 0; i < ComponentSystem.Components.Count; ++i) {
                    IComponent component = ComponentSystem.Components[i];
                    component.ComponentId = i.ToString();
                    component.InitComponent(sceneStarter, controller);
                    controller.RetainComponent(component);
                    //check if it's active comp
                    IActiveComponent active = component as IActiveComponent;
                    if (null != active) {
                        m_activeComps.Add(active);
                    }
                }
            }

            public void UpdateObject(float dt) {                
                //update the updateables
                foreach (IUpdateableComponent updateable in m_updateableComps) {
                    updateable.UpdateComponent(dt);
                }
                //update the tweener
                Tweener.Update(dt);
                //update gesture handler
                if (null != Gestures) {
                    Gestures.UpdateHandler(dt);
                }
            }

            public void OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                //broadcast to the components
                foreach (IActiveComponent component in m_activeComps) {
                    if (component.OnReceiveEvent(ev, scope)) {
                        Debugger.Log("GAME EVENTS", string.Format("RECEIVED {0}, by PTDOBJECT = {1}", ev, component));
                    }
                }
            }

            public void OnAwakeGame() {
                foreach (IComponent component in ComponentSystem.Components) {
                    component.OnAwakeGame();
                }
                //trace the children and find the ptd components as well as the tweens
                SetupChildren();
            }

            public void OnStartGame() {
                foreach (IComponent component in ComponentSystem.Components) {
                    component.OnStartGame();
                }
            }

            public SaveObjectData OnSave() {
                //generate transform data
                Vector2 pos = RootTransform.Pos();                    
                ObjectData.InsertPair(new KVData(SaveKeys.PosX, (int)pos.x));                    
                ObjectData.InsertPair(new KVData(SaveKeys.PosY, (int)pos.y));                    
                ObjectData.InsertPair(new KVData(SaveKeys.Parent, null != Parent ? Parent.ObjectId : string.Empty)); 
                //remove collider area and stick to other save if parent is null
                if (null == Parent) {
                    ObjectData.RemovePair(SaveKeys.ColliderArea);
                    ObjectData.RemovePair(SaveKeys.StickToOther);
                }
                //return
                return ObjectData;   
            }

            public void OnLoad(SaveObjectData data) {
                //set the transform data
                ObjectData = data;
                //set parent
                string parentId;
                //set parent
                if (ObjectData.TryGetString(SaveKeys.Parent, out parentId)) {
                    if (string.IsNullOrEmpty(parentId)) {
                        SetParent(null);
                    } else {
                        PtdObject parentObj = m_gameController.GetObjectById(parentId);
                        if (null != parentObj) {
                            SetParent(parentObj);
                            //get parent area 
                            ParentAreaData parentArea = m_gameController.GetProviderData<ParentAreaData>(ObjectData);
                            if (null != parentArea) {
                                RootTransform.SetParent(parentArea.m_area);
                            }
                        }   
                    }
                }
                //notify on loaded
                foreach (IActiveComponent component in m_activeComps) {
                    component.OnLoaded();
                }
                //set pos
                int x, y;
                if (ObjectData.TryGetInt(SaveKeys.PosX, out x) && ObjectData.TryGetInt(SaveKeys.PosY, out y)) {
                    RootTransform.SetPos(new Vector2(x, y));
                }
            }

            public bool IsSibling(PtdObject other) {                
                if (null != other.Parent) {
                    int otherParentId = other.Parent.GetInstanceID();
                    //check whether other is parented to this object
                    if (GetInstanceID() == otherParentId) {
                        return true;
                    }
                    //if we have a parent, check whether the parent is the same as the other object
                    if (null != Parent) {
                        int parentId = Parent.GetInstanceID();
                        if (parentId == otherParentId) {
                            return true;
                        }
                    }
                } else {
                    //if other has no parent, check whether this obj is parented to it
                    if (null != Parent) {
                        int otherId = other.GetInstanceID();
                        int parentId = Parent.GetInstanceID();
                        if (parentId == otherId) {
                            return true;
                        }
                    }
                }
                return false;
            }

            #region Private Methods
            void SetupChildren() {
                GameObjectExt.IterateChildren(gameObject, delegate (GameObject go) {
                    PtdObject obj = go.GetComponent<PtdObject>();
                    //dont trace anymore if we found an IObject in the children already
                    if (null != obj) {
                        //set this as the parent's of the object
                        obj.SetParent(this);
                        //exit the recursion
                        return false;
                    }
                    //set tweens
                    SetupTweens(go);
                    //set components
                    SetupComponents(go);
                    //recurse next child go
                    return true;
                });
            }

            void SetupTweens(GameObject go) {
                //add the tweens
                ITween[] tweens = go.GetComponents<ITween>();
                foreach (ITween tween in tweens) {
                    Tweener.Add(tween);
                }
            }

            void SetupComponents(GameObject go) {
                //get the go's components
                IComponent[] components = go.GetComponents<IComponent>();
                for (int i = 0; i < components.Length; ++i) {                    
                    //init the component
                    components[i].Owner = this;
                    //add to the component system
                    ComponentSystem.AddComponent(components[i]);
                    //check if it's updateable
                    IUpdateableComponent updateable = components[i] as IUpdateableComponent;
                    if (null != updateable) {
                        m_updateableComps.Add(updateable);
                    }
                }
            }
            #endregion
        }       
    }
}
