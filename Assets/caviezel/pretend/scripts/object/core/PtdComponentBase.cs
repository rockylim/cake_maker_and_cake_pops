﻿using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public abstract class PtdComponentBase : MonoBehaviour, IComponent {            
            #region IComponent
            public string ComponentId { get; set; }

            public PtdObject Owner {get; set;}

            public virtual void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {}

            public virtual void OnAwakeGame() {}

            public virtual void OnStartGame() {}

            public virtual void Reinit() {}

            public virtual void OnLoaded() {}
            #endregion
        }
    }
}
