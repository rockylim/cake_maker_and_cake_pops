﻿using Caviezel.Core;
using Caviezel.Audio;

namespace Caviezel {
    namespace Pretend {
        public class PtdNavigatorScene : PtdComponentActive {
            public NavigatorSceneEv[] m_navSceneEvs;

            IGameController m_gameController;
            SfxManager m_sfxManager;

			public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //cache the gc
                m_gameController = gameController;
                //cache the sfx manager
                m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
			}

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_navSceneEvs.Length; ++i) {
                        if (m_navSceneEvs[i].Matches(ev, scope)) {
                            //play the sound(s) first
                            if (null != m_navSceneEvs[i].m_sfxs && m_navSceneEvs[i].m_sfxs.Length > 0) {
                                m_sfxManager.PlaySound(m_navSceneEvs[i].m_sfxs);
                            }
                            //go to scene
                            m_gameController.ToScene(m_navSceneEvs[i].m_sceneId);
                            return true;
                        }
                    }
                }
                return false;
			}
		}       
    }
}