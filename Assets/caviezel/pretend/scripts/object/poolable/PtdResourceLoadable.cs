﻿using UnityEngine;
using UnityEngine.UI;
using Caviezel.Core;


namespace Caviezel {
    namespace Pretend {
        
        public class PtdResourceLoadable : PtdComponentActive {
            public BaseReceiverEv[] m_onShouldLoadSpriteEv;

            public Image m_targetImage;
            public string m_folderName;
            public string m_fileNamePrefix;
            public bool m_useSelectedImage;
            [ConditionalHide("m_useSelectedImage", true)]
            public Sprite m_selectedImage;
            public float m_nativeSizeRatio;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                if (m_targetImage == null) {
                    m_targetImage = GetComponent<Image>();
                }
            }
            public override void OnStartGame() {
                base.OnStartGame();
            }


            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_onShouldLoadSpriteEv.Length; ++i) {
                        if (m_onShouldLoadSpriteEv[i].Matches(ev, scope)) {
                            if(m_useSelectedImage) {
                                m_targetImage.sprite = m_selectedImage;
                            } else {
                                string index = ev.m_eventId.Substring(m_onShouldLoadSpriteEv[i].m_eventId.Length);
                                m_targetImage.sprite = Resources.Load<Sprite>(m_folderName + "/" + m_fileNamePrefix + index);
                            }
                            if (m_nativeSizeRatio > 0f) {
                                m_targetImage.SetNativeSize();
                                m_targetImage.rectTransform.SetSize(new Vector2(m_targetImage.rectTransform.Width() * m_nativeSizeRatio, m_targetImage.rectTransform.Height() * m_nativeSizeRatio));
                            }
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }
}
