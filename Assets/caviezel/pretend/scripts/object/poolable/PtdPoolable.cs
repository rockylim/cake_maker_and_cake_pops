﻿using System.Collections.Generic;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdPoolable : PtdComponentActive, IUpdateableComponent {            
            public PoolableEv[] m_poolEvs;
            public PoolItemData[] m_items;
            public bool m_hasRetainEv;
            [ConditionalHide("m_hasRetainEv", true)]
            public PoolableSenderEv m_retainEv;
            public bool m_hasRunOutEv;
            [ConditionalHide("m_hasRunOutEv", true)]
            public PoolableSenderEv m_runOutEv;
            public bool m_showOnStart;

            #region PtdComponentBase
            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                for (int i = 0; i < m_items.Length; ++i) {
                    m_items[i].InitPoolItem();
                    //show/hide all on start
                    m_items[i].m_item.gameObject.SetActive(m_showOnStart);
                }
            }
            			
			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_poolEvs.Length; ++i) {
                        if (m_poolEvs[i].Matches(ev, scope)) {
                            ReleaseItem();
                            return true;
                        }
                    }
                }

                return false;
            }

			public override void OnLoaded() {
				base.OnLoaded();
                List<string> releasedItems = Owner.ObjectData.GetStrings(SaveKeys.PoolableRelease + ComponentId);
                int i = 0;
                while (releasedItems.Count > 0) {
                    string releasedItem = releasedItems[i];
                    foreach (PoolItemData poolItem in m_items) {
                        if (!poolItem.IsReleased && poolItem.m_item.ObjectId.IsEqual(releasedItem)) {
                            poolItem.Release();
                            releasedItems.Remove(releasedItem);
                        }
                    }
                }
			}
			#endregion

			public void ReleaseItem() {
                PoolItemData itemData = null;
                //get the item from pool
                for (int i = 0; i < m_items.Length; ++i) {
                    if (!m_items[i].IsReleased) {
                        itemData = m_items[i];
                        //show and change the is released flag to true
                        itemData.Release();
                        //broadcast ev
                        BroadcastEvent(itemData.m_onReleaseEv);
                        //save
                        Owner.ObjectData.AddPairWithValue(SaveKeys.PoolableRelease + ComponentId, itemData.m_item.ObjectId);
                        //get one only
                        break;
                    }
                }
                //do run out of action if no more
                if (m_items.Length == 0) {
                    //broadcast run out ev if necessary
                    if (m_hasRunOutEv) {
                        BroadcastEvent(m_runOutEv);
                    }
                }
            }

            public void UpdateComponent(float dt) {
                for (int i = 0; i < m_items.Length; ++i) {
                    //retain item
                    if (m_items[i].m_item.IsDestroyed && m_items[i].IsReleased) {
                        m_items[i].m_item.IsDestroyed = false;
                        //hide and change the isreleased flag to false
                        m_items[i].Retain(m_showOnStart);
                        //remove the save item
                        Owner.ObjectData.RemovePairWithValue(SaveKeys.PoolableRelease + ComponentId, m_items[i].m_item.ObjectId);
                        //broadcast retain ev if necessary
                        if (m_hasRetainEv) {
                            BroadcastEvent(m_retainEv);
                        }
                    }
                }
            }
        }       
    }
}