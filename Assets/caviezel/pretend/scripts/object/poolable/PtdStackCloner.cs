﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel.Core;
using Caviezel.UI;

namespace Caviezel {
    namespace Pretend {
        public class PtdStackCloner : PtdComponentActive, IUpdateableComponent {
            public Image m_creamRef;
            public int m_maxItem = 10;
            public string m_spriteFolder;
            public BaseReceiverEv[] m_cloneEvs;
            public RectTransform m_creamContainer;
            public float m_spacing = -10f;

            Dictionary<string, Sprite> m_spriteCollections = new Dictionary<string, Sprite>();
            Stack<Image> m_pools = new Stack<Image>();
            List<Image> m_stacking = new List<Image>();
            CGestureHandler m_gestureHandler;
            CStacker m_stacker;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //create the sprite collections
                Sprite[] sprites = Resources.LoadAll<Sprite>(m_spriteFolder);
                for (int i = 0; i < sprites.Length; ++i) {
                    m_spriteCollections.Add(sprites[i].name, sprites[i]);
                }
                //init tap
                m_gestureHandler = new CGestureHandler(gameObject);
                m_gestureHandler.SetTappable();
                m_gestureHandler.OnTapListeners += OnTap;
                //instantiate
                for (int i = 0; i < m_maxItem; ++i) {
                    Image image = null;
                    if (i == 0) {
                        image = m_creamRef;
                    } else {
                        GameObject newObj = Instantiate(m_creamRef.gameObject) as GameObject;
                        newObj.name = m_creamRef.name + i.ToString();
                        newObj.transform.SetParent(m_creamContainer, false);
                        image = newObj.GetComponent<Image>();
                    }
                    //add to stack
                    image.gameObject.SetActive(false);
                    m_pools.Push(image);
                }
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_cloneEvs.Length; ++i) {
                        if (m_cloneEvs[i].Matches(ev, scope) && m_spriteCollections.ContainsKey(ev.m_eventId)) {
                            StackNew(m_spriteCollections[ev.m_eventId]);
                        }
                    }
                }
                return false;
            }

            public void UpdateComponent(float dt) {
                if (null != m_stacker) {
                    m_stacker.Arrange(dt);
                }
            }

            void StackNew(Sprite sprite) {
                if (m_pools.Count > 0) {
                    Image stacked = m_pools.Pop();
                    stacked.sprite = sprite;
                    stacked.SetMaxSize(200f);
                    stacked.transform.SetAsLastSibling();
                    stacked.gameObject.SetActive(true);
                    m_stacking.Add(stacked);
                    Restack();
                }
            }

            void OnTap(Vector2 pos) {
                if (IsActive && m_stacking.Count > 0) {
                    int idx = 0;
                    m_stacking[idx].gameObject.SetActive(false);
                    m_pools.Push(m_stacking[idx]);
                    m_stacking.RemoveAt(idx);
                    Restack();
                }
            }

            void Restack() {
                List<RectTransform> rts = new List<RectTransform>();
                for (int i = 0; i < m_stacking.Count; ++i) {
                    rts.Add(m_stacking[i].rectTransform);
                }
                m_stacker = new CStacker(rts, m_creamContainer.Pos(), 1, 1, m_spacing, 0.2f);                
            }
        }       
    }
}
