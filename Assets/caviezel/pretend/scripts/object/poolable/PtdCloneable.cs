﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdCloneable : PtdComponentActive {
            public Image m_reference;
            public RectTransform m_cloneArea;
            public string[] m_spriteFolders;
            public int m_maxCount;
            public CloneableEv[] m_cloneEvs;
            public BaseReceiverEv[] m_undoEvs;
            public bool m_hasClonedEv;
            [ConditionalHide("m_hasClonedEv", true)]
            public BaseSenderEv m_clonedEv;
            public bool m_hasUndoneEv;
            [ConditionalHide("m_hasUndoneEv", true)]
            public BaseSenderEv m_undoneEv;

            Dictionary<string, Sprite> m_sprites = new Dictionary<string, Sprite>();
            Stack<Image> m_items = new Stack<Image>();
            Stack<Image> m_released = new Stack<Image>();

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //load and store the sprite first
                for (int i = 0; i < m_spriteFolders.Length; ++i) {
                    Sprite[] sprites = Resources.LoadAll<Sprite>(m_spriteFolders[i]);
                    for (int j = 0; j < sprites.Length; ++j) {
                        m_sprites.Add(sprites[j].name, sprites[j]);
                    }
                }
                //instantiate
                for (int i = 0; i < m_maxCount; ++i) {
                    Image image = null;
                    if (i == 0) {
                        image = m_reference;
                    } else {
                        GameObject newObj = Instantiate(m_reference.gameObject) as GameObject;
                        newObj.name = m_reference.name + i.ToString();
                        newObj.transform.SetParent(m_cloneArea, false);
                        image = newObj.GetComponent<Image>();
                    }
                    //add to stack
                    image.gameObject.SetActive(false);
                    m_items.Push(image);
                }
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //clone ev
                    for (int i = 0; i < m_cloneEvs.Length; ++i) {
                        if (m_cloneEvs[i].Matches(ev, scope)) {
                            if (m_items.Count > 0) {
                                Sprite sprite = null;
                                m_sprites.TryGetValue(ev.m_eventId, out sprite);
                                Image item = m_items.Pop();
                                item.sprite = sprite;
                                float maxSize = m_cloneEvs[i].m_maxSize;
                                float nativeSizeRatio = m_cloneEvs[i].m_nativeSizeRatio;
                                float curMaxSize = 0f;
                                if ((maxSize > 0f) || (nativeSizeRatio > 0f)) {
                                    item.SetNativeSize();
                                    Vector2 curSize = item.rectTransform.Size();
                                    float ratio;

                                    // make the item based on native size, and multiplied by nativeSizeRatio
                                    if (nativeSizeRatio > 0f) {
                                        ratio = nativeSizeRatio;
                                        item.rectTransform.SetSize(curSize *= ratio);
                                        curMaxSize = Mathf.Max(curSize.x, curSize.y);
                                    }

                                    // check max size of the image
                                    if (maxSize > 0f) {
                                        bool needResize = true;
                                        if ((nativeSizeRatio > 0f) && (curMaxSize < maxSize)) {
                                            // if using native size ratio, check that
                                            // if the size is smaller than max size, if not, then do not resize
                                            needResize = false;
                                        }

                                        if (needResize) {
                                            float biggest = Mathf.Max(curSize.x, curSize.y);
                                            ratio = maxSize / biggest;
                                            item.rectTransform.SetSize(curSize *= ratio);
                                        }
                                    }
                                }
                                item.gameObject.SetActive(true);
                                item.rectTransform.SetAsLastSibling();
                                m_released.Push(item);
                                //broadcast
                                if (m_hasClonedEv) {
                                    BroadcastEvent(m_clonedEv);
                                }
                            }
                            return true;
                        }
                    }
                    //undo ev
                    for (int i = 0; i < m_undoEvs.Length; ++i) {
                        if (m_undoEvs[i].Matches(ev, scope)) {
                            if (m_released.Count > 0) {
                                Image item = m_released.Pop();
                                item.gameObject.SetActive(false);
                                m_items.Push(item);
                                if (m_hasUndoneEv) {
                                    BroadcastEvent(m_undoneEv);
                                }
                            }
                        }
                    }
                }
                return false;
            }
        }
    }
}
