﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdTapCloneableCore : PtdComponentActive {
            public List<PtdTapCloneable> m_tapCloneable;
            public bool m_isAutoPlacement;
            public Image m_reference;
            public string[] m_spriteFolders;
            public int m_maxCount;
            public PtdParticleController m_particleController;
            public CloneableEv[] m_cloneEvs;
            public BaseReceiverEv[] m_undoEvs;
            public BaseSenderEv m_spriteSelectedEv;
            public bool m_hasClonedEv;
            [ConditionalHide("m_hasClonedEv", true)]
            public BaseSenderEv m_clonedEv;
            public bool m_hasUndoneEv;
            [ConditionalHide("m_hasUndoneEv", true)]
            public BaseSenderEv m_undoneEv;
            public BaseSenderEv m_allReleasedEv;


            Dictionary<string, Sprite> m_sprites = new Dictionary<string, Sprite>();
            Stack<Image> m_items = new Stack<Image>();
            Stack<Image> m_released = new Stack<Image>();
            Sprite m_selectedSprite;
            RectTransform m_rectTransform;
            float m_selectedMaxSize;
            float m_nativeSizeRatio;
            int m_activeIndexPlacement;
            System.Random m_rand;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);

                m_rand = MathExtensions.GetRandom();

                //load and store the sprite first
                for (int i = 0; i < m_spriteFolders.Length; ++i) {
                    Sprite[] sprites = Resources.LoadAll<Sprite>(m_spriteFolders[i]);
                    for (int j = 0; j < sprites.Length; ++j) {
                        m_sprites.Add(sprites[j].name, sprites[j]);
                    }
                }

                m_rectTransform = GetComponent<RectTransform>();

                //instantiate
                for (int i = 0; i < m_maxCount; ++i) {
                    Image image = null;
                    if (i == 0) {
                        image = m_reference;
                    } else {
                        GameObject newObj = Instantiate(m_reference.gameObject) as GameObject;
                        newObj.name = m_reference.name + i.ToString();
                        newObj.transform.SetParent(m_rectTransform, false);
                        image = newObj.GetComponent<Image>();
                    }
                    //add to stack
                    image.gameObject.SetActive(false);
                    m_items.Push(image);
                }
                // init controller
                for (int i = 0; i < m_tapCloneable.Count; ++i) {
                    m_tapCloneable[i].InitController(this);
                }
                m_selectedSprite = null;

                m_activeIndexPlacement = m_rand.Next(0, m_tapCloneable.Count);
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //clone ev
                    for (int i = 0; i < m_cloneEvs.Length; ++i) {
                        if (m_cloneEvs[i].Matches(ev, scope)) {
                            m_sprites.TryGetValue(ev.m_eventId, out m_selectedSprite);
                            m_selectedMaxSize = m_cloneEvs[i].m_maxSize;
                            m_nativeSizeRatio = m_cloneEvs[i].m_nativeSizeRatio;
                            BroadcastEvent(m_spriteSelectedEv);
                            if (m_isAutoPlacement) {
                                AutoPlacement();
                            }
                            return true;
                        }
                    }
                    //undo ev
                    for (int i = 0; i < m_undoEvs.Length; ++i) {
                        if (m_undoEvs[i].Matches(ev, scope)) {
                            if (m_released.Count > 0) {
                                Image item = m_released.Pop();
                                item.gameObject.SetActive(false);
                                m_items.Push(item);
                                if (m_hasUndoneEv) {
                                    BroadcastEvent(m_undoneEv);
                                }
                            }
                            return true;
                        }
                    }
                }
                return false;
            }

            public void ClonedOnTarget(RectTransform area, Vector2 pos, bool isAutoPlace) {
                if (m_items.Count > 0 && m_selectedSprite != null) {
                    Image item = m_items.Pop();
                    item.sprite = m_selectedSprite;
                    float maxSize = m_selectedMaxSize;
                    float nativeSizeRatio = m_nativeSizeRatio;
                    float curMaxSize = 0f;
                    Vector2 newDecorPosition = pos;
                    if ((maxSize > 0f) || (nativeSizeRatio > 0f)) {
                        item.SetNativeSize();
                        Vector2 curSize = item.rectTransform.Size();
                        float ratio;

                        // make the item based on native size, and multiplied by nativeSizeRatio
                        if (nativeSizeRatio > 0f) {
                            ratio = nativeSizeRatio;
                            item.rectTransform.SetSize(curSize *= ratio);
                            curMaxSize = Mathf.Max(curSize.x, curSize.y);
                        }

                        // check max size of the image
                        if (maxSize > 0f) {
                            bool needResize = true;
                            if ((nativeSizeRatio > 0f) && (curMaxSize < maxSize)) {
                                // if using native size ratio, check that
                                // if the size is smaller than max size, if not, then do not resize
                                needResize = false;
                            }

                            if (needResize) {
                                float biggest = Mathf.Max(curSize.x, curSize.y);
                                ratio = maxSize / biggest;
                                item.rectTransform.SetSize(curSize *= ratio);
                            }
                        }
                    }
                    item.gameObject.SetActive(true);
                    item.transform.SetParent(area);
                    item.rectTransform.SetScale(new Vector2(1f, 1f));
                    item.rectTransform.SetAsLastSibling();
                    if (!isAutoPlace) {
                        RectTransformUtility.ScreenPointToLocalPointInRectangle(area, pos, null, out newDecorPosition);
                    }
                    item.SetPos(newDecorPosition);
                    m_released.Push(item);
                    //broadcast
                    if (m_hasClonedEv) {
                        BroadcastEvent(m_clonedEv);
                    }
                    if (m_released.Count == m_maxCount) {
                        BroadcastEvent(m_allReleasedEv);
                    }

                    if (m_particleController != null) {
                        MoveParticleController(item.transform.position);
                    }
                }
            }

            public void ClonedOnTarget(RectTransform area, Vector2 pos) {
                ClonedOnTarget(area, pos, false);
            }

            void AutoPlacement() {
                if (m_tapCloneable.Count > 0) {
                    RectTransform selectedPlace = m_tapCloneable[m_activeIndexPlacement].GetClonedArea();
                    Vector2 pos = new Vector2(RandomPos(selectedPlace.Width()), RandomPos(selectedPlace.Height()));
                    ClonedOnTarget(selectedPlace, pos, true);
                    m_activeIndexPlacement++;
                    if (m_activeIndexPlacement == m_tapCloneable.Count) {
                        m_activeIndexPlacement = 0;
                    }
                }
            }

            float RandomPos(float size) {
                float pos = m_rand.Next(0, (int)(size * .5f));
                if (m_rand.Next(1, 3) == 1) {
                    pos = -pos;
                }
                return pos;
            }

            void MoveParticleController(Vector2 worldPos) {
                m_particleController.m_area.transform.position = worldPos;
            }
        }
    }
}
