﻿namespace Caviezel {
    namespace Pretend {
        public class PtdCounter : PtdComponentActive {
            public CounterDeltaEv[] m_deltaEvs;
            public bool m_hasCountEv;
            [ConditionalHide("m_hasCountEv", true)]
            public BaseSenderEv m_onCountedEv;
            public CounterMaxEv[] m_maxEvs;
            public bool m_hasMaxEv;
            [ConditionalHide("m_hasMaxEv", true)]
            public int m_maxCounter;
            [ConditionalHide("m_hasMaxEv", true)]
            public BaseSenderEv m_onExceedMaxEv;
            public bool m_hasReachZeroEv;
            [ConditionalHide("m_hasReachZeroEv", true)]
            public BaseSenderEv m_onReachZeroEv;

            int m_cur;

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //delta ev
                    for (int i = 0; i < m_deltaEvs.Length; ++i) {
                        if (m_deltaEvs[i].Matches(ev, scope)) {
                            if (m_deltaEvs[i].m_resetFirst) {
                                m_cur = 0;
                            }
                            m_cur += m_deltaEvs[i].m_delta;
                            if (m_hasMaxEv && m_cur >= m_maxCounter) {
                                BroadcastEvent(m_onExceedMaxEv);
                            }

                            if (m_hasReachZeroEv && m_cur == 0) {
                                BroadcastEvent(m_onReachZeroEv);
                            }

                            if (m_hasCountEv) {
                                BaseSenderEv onCounted = new BaseSenderEv(m_onCountedEv.m_senderId, m_onCountedEv.m_eventId + m_cur.ToString());
                                BroadcastEvent(onCounted);
                            }

                            return true;
                        }
                    }
                    //max ev
                    for (int i = 0; i < m_maxEvs.Length; ++i) {
                        if (m_maxEvs[i].Matches(ev, scope)) {
                            m_maxCounter = m_maxEvs[i].m_max;
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }
}
