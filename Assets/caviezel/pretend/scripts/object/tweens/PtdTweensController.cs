﻿using System;
using Caviezel.Audio;
using Caviezel.Core;
using Caviezel.Pretend;
using Caviezel.Tween;
using UnityEngine;

namespace Detentionapps {
  namespace BabyCareDay {
    public class PtdTweensController : PtdComponentActive, IUpdateableComponent {
      public TweensStopEvent[] m_tweensStopEvs;
      public string m_tweenReset;
      public bool m_StopSounds;
      [ConditionalHide("m_StopSounds", true)]
      public string[] m_soundsToStop;

      // do not stop tween if tween priority is playing!
      public string[] m_tweensPriority;

      [Serializable]
      public class TweensStopEvent : BaseReceiverEv {
        public OnStoppedSenderEv m_onStoppedEv;
      }

      [Serializable]
      public class OnStoppedSenderEv : BaseSenderEv {
        public OnStoppedSenderEv(BaseSenderEv other) : base(other) { }
      }


      CTweenChainer m_tweener;
      OnStoppedSenderEv m_afterResetEv;
      SfxManager m_sfxManager;
      bool m_isReseting;

      #region Public Methods

      #endregion


      #region PtdComponentActive
      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);

        //cache the sfx manager
        m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
        Debug.Assert(null != m_sfxManager);

        //init tweener
        m_tweener = new CTweenChainer();
        m_tweener.Add(gameObject, true);

        m_isReseting = false;

        m_tweener.PlayExistingTween(new string[] { m_tweenReset }, () => {
        });
      }

      public override void OnStartGame() {
        base.OnStartGame();
      }
      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_tweensPriority.Length; i++) {
            if (Owner.Tweener.IsTweenPlaying(m_tweensPriority[i])) {
              return false;
            }
          }
          for (int i = 0; i < m_tweensStopEvs.Length; ++i) {
            if (m_tweensStopEvs[i].Matches(ev, scope)) {
              m_afterResetEv = m_tweensStopEvs[i].m_onStoppedEv;

              if (!m_isReseting) {

                if (m_StopSounds) {
                  StopSounds();
                }

                m_isReseting = true;
                Owner.Tweener.Stop();
                Owner.Tweener.PlayExistingTween(new string[] { m_tweenReset }, () => {
                  m_isReseting = false;
                  BroadcastEvent(m_afterResetEv);
                });
              }

              return true;
            }
          }
        }

        return false;
      }
      #endregion


      #region IUpdateable
      public void UpdateComponent(float dt) {
        m_tweener.Update(dt);
      }
      #endregion

      void StopSounds() {
        for (int i = 0; i < m_soundsToStop.Length; i++) {
          m_sfxManager.StopAudioByName(m_soundsToStop[i]);
        }
      }
    }
  }
}