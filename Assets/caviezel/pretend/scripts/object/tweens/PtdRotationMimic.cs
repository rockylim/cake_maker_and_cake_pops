﻿using Caviezel.Core;
using Caviezel.Pretend;
using UnityEngine;

namespace Detentionapps {
  namespace BabyCareDay {
    public class PtdRotationMimic : PtdComponentActive, IUpdateableComponent {
      public Transform m_mimicTarget;

      #region PtdComponentActive
      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        transform.rotation = m_mimicTarget.rotation;
      }

      public override void OnStartGame() {
        base.OnStartGame();
      }
      #endregion

      #region IUpdateable
      public void UpdateComponent(float dt) {
        transform.rotation = m_mimicTarget.rotation;
      }
      #endregion
    }
  }
}