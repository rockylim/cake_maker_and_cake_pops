﻿using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdGravityable : PtdComponentBase, IGravityable {
            [Tooltip("if defined, then will use this RT as the grounded area checking, otherwise will try to get component RT in where this comp is attached to")]
            public bool m_useOtherArea;
            [ConditionalHide("m_useOtherArea", true)]
            public RuntimeColliderData m_otherArea;

            RectTransform m_groundArea;
            bool m_shouldFall; //will be called when it begins and is done falling
            bool m_beganLanded; //will be called on began grounded
            bool m_shouldBounce; //flag when it should bounce
            float m_speed;
            float m_posY; // store position Y that the object grounded for the 1st time, after bounce should be stop in the same position

            const float m_initialSpeed = 120f;
            const float m_gravitySpeed = 60f;
            const float m_minSpeedToBounce = 900f; // if the falling object speed is grater than this, means need to bounce
            const float m_minDistanceFromBottom = 10f; // minimum distance from bottom of the screen, so the object cannot put exactly on the bottom of the screen
            const float m_minSpeedToPushUp = 500f;
            const float m_backSpeed = 350f;

            #region IPtdGravityable
            public RectTransform GroundedArea {
                get { return Owner.RootTransform; }
            }

            public Rect GroundBound {
                get { return m_groundArea.RectAgainstRoot(); }
            }

            public bool ShouldFall {
                get { return m_shouldFall; }
                set {
                    m_shouldFall = value;
                    if (!m_shouldFall) {
                        //reset the values below when it's done falling
                        m_speed = m_initialSpeed;
                        m_beganLanded = false;
                    } 
                }
            }

            public bool BeganLanded {
                get { return m_beganLanded; }
                set {                    
                    m_beganLanded = value;
                    // if landed out of screen's height
                    if (GroundedArea.IsOutOfHeight(new Vector2(0f, -m_minDistanceFromBottom))) {
                        m_shouldBounce = true;
                        Vector2 bounceTo = GroundedArea.AnchoredPosFromRoot(new Vector2(0f, -1f), new Vector2(0f, m_minDistanceFromBottom));
                        m_posY = bounceTo.y;
                        // further from screen, more bounce!
                        m_speed = (bounceTo.y - GroundedArea.Pos().y) * -7f;
                        m_speed = m_speed > -m_minSpeedToPushUp ? -m_minSpeedToPushUp : m_speed;
                    } else if(m_beganLanded && m_speed >= m_minSpeedToBounce) {
                        //on began grounded, if fast enough to bounce
                        m_shouldBounce = true;
                        m_speed = -m_speed / 3f;
                        m_posY = GroundedArea.Pos().y;
                    }
                }
            }

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //set the ground area and speed on init
                m_groundArea = m_useOtherArea ? m_otherArea.Generate().m_area : Owner.RootTransform;
                m_speed = m_initialSpeed;
            }

            public bool HasGrounded(float dt) {                
                bool hasGrounded = !m_shouldBounce;
                if (m_shouldBounce) {              
                    Vector2 pos = GroundedArea.Pos();
                    float nextYPosition = pos.y - GetMovementY(dt);
                    if (m_speed > 0) {
                        // time to bounce down
                        // if reach target first grounded position, stop the bounce movement
                        if (nextYPosition <= m_posY) {
                            // stop bounce
                            m_speed = 0f;
                            m_shouldBounce = false;
                            nextYPosition = m_posY;
                        }
                    }
                    pos.y = nextYPosition;
                    GroundedArea.SetPos(pos);   
                }

                if (GroundedArea.IsOutOfWidth()) {
                    Vector2 pos = GroundedArea.Pos();
                    GroundedArea.AddPosX(Mathf.Sign(-pos.x) * dt * m_backSpeed);
                    return false;
                }

                return hasGrounded;
            }

            public void OnFalling(float dt) {                
                Vector2 pos = GroundedArea.Pos();
                pos.y -= GetMovementY(dt);
                GroundedArea.SetPos(pos);
            }
            #endregion

            // get next Y position based on m_speed (move up or move down)
            float GetMovementY(float dt) {
                m_speed = m_speed + m_gravitySpeed;
                return dt * m_speed;
            }
        }
    }
}
