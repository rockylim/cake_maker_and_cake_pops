﻿using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdGroundable : PtdComponentBase, IGroundable {
            public RectTransform m_groundArea;

            public bool IsGrounded(IGravityable gravityable) {
                if (m_groundArea.RectAgainstRoot().Intersects(gravityable.GroundBound)) {                    
                    return true;
                }
                return false;
            }
        }
    }
}