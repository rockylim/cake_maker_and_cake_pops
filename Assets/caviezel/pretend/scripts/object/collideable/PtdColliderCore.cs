﻿using System;
using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        [Serializable]
        public class ColliderData {
            public string m_id;
            public RectTransform m_area;

            public ColliderData(string id, RectTransform area) {
                m_id = id;
                m_area = area;
            }
        }

        public enum AnchorType {
            Center,
            Top,
            Bottom
        }

        [Serializable]
        public class RuntimeColliderData {
            public RectTransform m_reference;
            public RectTransform m_parent;
            public string m_id;
            public Vector2 m_sizeMultiplier;
            public AnchorType m_anchor;

            public ColliderData Generate() {
                GameObject go = new GameObject(m_id);
                RectTransform rt = go.AddComponent<RectTransform>();
                Vector2 refSize = m_reference.Size();
                Vector2 newSize = new Vector2(refSize.x * m_sizeMultiplier.x, refSize.y * m_sizeMultiplier.y);
                rt.SetParent(m_parent, false);
                rt.SetSize(newSize);
                rt.SetPos(GetAnchoredPos(m_anchor, refSize, newSize));

                return new ColliderData(m_id, rt);
            }

            Vector2 GetAnchoredPos(AnchorType type, Vector2 refSize, Vector2 newSize) {
                Vector2 pos = Vector2.zero;

                switch (type) {
                    case AnchorType.Bottom:
                        pos.y = -(refSize.y / 2f) + (newSize.y / 2f);
                        break;
                    case AnchorType.Top:
                        pos.y = (refSize.y / 2f) - (newSize.y / 2f);
                        break;                    
                }

                return pos;
            }
        }

        [Serializable]
        public class CollisionData {
            public string m_colliderId;
            public ComparisonType m_comparisonCollider;
            public string m_otherColliderId;
            public ComparisonType m_comparisonOther;
            public BaseSenderEv m_senderEv;

            public CollisionData(string colliderId, string otherId) {
                m_colliderId = colliderId;
                m_otherColliderId = otherId;
            }

            public bool Matches(OnCollideData collideData) {
                return (collideData.m_colliderData.m_id.Matches(m_colliderId, m_comparisonCollider) && collideData.m_otherColliderData.m_id.Matches(m_otherColliderId, m_comparisonOther));
            }

            public bool Matches(string colliderId, string otherId) {                
                return (colliderId.Matches(m_colliderId, m_comparisonCollider) && otherId.Matches(m_otherColliderId, m_comparisonOther));
            }
        }

        public class OnCollideData {            
            public ICollider m_collider;
            public ColliderData m_colliderData;
            public ICollider m_otherCollider;
            public ColliderData m_otherColliderData;

            public OnCollideData() {}

            public OnCollideData(ICollider collider, ColliderData colliderData, ICollider otherCollider, ColliderData otherColliderData) {
                m_collider = collider;
                m_colliderData = colliderData;
                m_otherCollider = otherCollider;
                m_otherColliderData = otherColliderData;
            }

            public bool IsSticking() {
                if (m_collider.Owner.Parent == null || m_otherCollider.Owner.Parent == null) {
                    return false;
                }
                return (m_collider.Owner.Parent == m_otherCollider.Owner.Parent || m_otherCollider.Owner.Parent == m_collider.Owner.Parent);
            }
        }                       
    }
}