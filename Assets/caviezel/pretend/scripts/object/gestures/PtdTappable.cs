﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.Audio;
using Caviezel.Chrono;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdTappable : PtdComponentActive, IUpdateableComponent, IPuzzleSolveCondition {                                    
            public TapSenderEv[] m_tapData;
            public int m_startIdx = 0;
            public bool m_isPuzzleable;
            [ConditionalHide("m_isPuzzleable", true)]
            public int m_puzzleableIdx;
            public bool m_hasNextTapDelay;
            [ConditionalHide("m_hasNextTapDelay", true)]
            public float m_delayTapTime;
            public ResizeableImage m_tapArea;

            int m_idx = 0;
            SfxManager m_sfxManager;
            Timer m_tapDelayTimer = null;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //resize the tap area
                m_tapArea.Resize();
                //set the idx
                m_idx = m_startIdx;
                //just in case, clamp
                if (m_idx >= m_tapData.Length) {
                    m_idx = m_tapData.Length - 1;
                }
                //handle the tap
                Owner.InitGestureHandler();
                Owner.Gestures.SetTappable();
                Owner.Gestures.OnTapListeners += OnTap;
                //cache the sfx manager
                m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
            }

			public override void OnLoaded() {
                base.OnLoaded();
                //try load the tap idx
                int tapIdx;
                if (Owner.ObjectData.TryGetInt(SaveKeys.TappableIndex + ComponentId, out tapIdx)) {
                    m_idx = tapIdx;
                }
			}

			#region IUpdateableComponent
			public void UpdateComponent(float dt) {
                if (null != m_tapDelayTimer && !m_tapDelayTimer.UpdateTimer(dt)) {
                    m_tapDelayTimer = null;
                }
            }
            #endregion

            #region IPuzzleSolveCondition
            public bool HasSolved {
                get {return m_isPuzzleable ? (m_idx == m_puzzleableIdx) : false;}
            }
            #endregion

            void OnTap(Vector2 pos) {
                if (IsTappable()) {
                    //set next tween idx
                    ++m_idx;
                    //if it exceeds the ids length, reset                    
                    if (m_idx >= m_tapData.Length) {
                        m_idx = 0;
                    }
                    TapSenderEv indexData = m_tapData[m_idx];
                    //play the sound
                    m_sfxManager.PlaySound(indexData.m_sfxs);
                    //broadcast
                    BroadcastEvent(indexData);
                    //delay for the next tap if it should
                    if (m_hasNextTapDelay) {                        
                        m_tapDelayTimer = new Timer(m_delayTapTime);
                    }
                    //save
                    Owner.ObjectData.InsertPair(new KVData(SaveKeys.TappableIndex + ComponentId, m_idx));
                }
            }

            bool IsTappable() {
                if (!IsActive || null != m_tapDelayTimer) {
                    return false;
                }
                return true;
            }
        }
    }
}
