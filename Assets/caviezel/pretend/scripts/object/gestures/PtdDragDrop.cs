﻿using UnityEngine;
using Caviezel.UI;
using Caviezel.Tween;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdDragDrop : PtdDraggable, IUpdateableComponent {
            public RectTransform m_dropTarget;
            public float m_snapBackDuration;
            public bool m_hasSuccessDropEv;
            [ConditionalHide("m_hasSuccessDropEv", true)]
            public BaseSenderEv m_successDropEv;
            public bool m_hasFailDropEv;
            [ConditionalHide("m_hasFailDropEv", true)]
            public BaseSenderEv m_failedDropEv;
            public bool m_snapBackOnSuccess;

            PtdObject m_originParent;
            Vector2 m_originPos;
            ITween m_snapBackTween;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //cache origin states
                m_originParent = Owner.Parent;
                m_originPos = Owner.RootTransform.Pos();
            }

            public override void OnEndSwipe(SwipeEventData ev) {
                bool hasDragged = m_hasDragged;
                base.OnEndSwipe(ev);
                if (hasDragged) {
                    CheckCollisionWithTarget();
                }
            }

            public void UpdateComponent(float dt) {
                if (null != m_snapBackTween && !m_snapBackTween.UpdateTween(dt)) {
                    m_snapBackTween = null;
                    Owner.IsInAction = false;
                }
            }

            void CheckCollisionWithTarget() {
                bool shouldSnapBack = false;
                if (Owner.RootTransform.RectAgainstRoot().Intersects(m_dropTarget.RectAgainstRoot())) {
                    //broadcast
                    if (m_hasSuccessDropEv) {
                        BroadcastEvent(m_successDropEv);
                    }
                    shouldSnapBack = m_snapBackOnSuccess;
                } else {
                    if (null != m_originParent) {
                        if (m_snapBackDuration > -1) {
                            Owner.SetParent(m_originParent);
                            Owner.RootTransform.SetParent(m_originParent.RootTransform);
                            shouldSnapBack = true;
                        } else {
                            shouldSnapBack = false;
                        }
                        if (m_hasFailDropEv) {
                            BroadcastEvent(m_failedDropEv);
                        }
                    }

                }
                //snap back if it should
                if (shouldSnapBack) {
                    m_snapBackTween = new CTweenMove(Owner.gameObject, m_originPos, m_snapBackDuration).Start();
                    Owner.IsInAction = true;
                }
            }
        }
    }
}
