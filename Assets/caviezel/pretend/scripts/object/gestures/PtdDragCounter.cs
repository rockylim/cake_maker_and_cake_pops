﻿using UnityEngine;
using System.Collections.Generic;
using Caviezel.Core;
using Caviezel.UI;
using Caviezel.Audio;

namespace Caviezel {
  namespace Pretend {
    public class PtdDragCounter : PtdDragSnapBack {
      public BaseSenderEv m_onAllCounterDoneEv;
      public List<PtdDragCounterReceiver> m_targets;
      public string m_tweenCount;
      public SfxData[] m_sfxs;
      bool m_done;

      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        for (int i = 0; i < m_targets.Count; i++) {
          m_targets[i].SetDragCounter(this);
        }
        m_done = false;
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        return base.OnReceiveEvent(ev, scope);
      }

      public override void OnSwiping(SwipeEventData ev) {
        if ((m_lockSwipe) || (m_done)) {
          return;
        }
        base.OnSwiping(ev);
        //check collision
        bool isPlaySfx = false;
        for (int i = 0; i < m_targets.Count; i++) {
          if (m_checkArea.RectAgainstRoot().Intersects(m_targets[i].GetRectTransform().RectAgainstRoot())) {
            bool needToPlaySfx = false;
            m_targets[i].IncCounter(m_tweenCount, out needToPlaySfx);
            if (needToPlaySfx) {
              isPlaySfx = true;
            }
          }
        }

        if (isPlaySfx) {
          m_sfxManager.PlaySound(m_sfxs);
        }
      }

      public override void OnEndSwipe(SwipeEventData ev) {
        if ((!m_lockSwipe) && (!m_done)) {
          base.OnEndSwipe(ev);
        }
      }

      public void OnCounterDone() {
        for (int i = 0; i < m_targets.Count; i++) {
          if (!m_targets[i].IsDone()) {
            return;
          }
        }
        m_done = true;
        BroadcastEvent(m_onAllCounterDoneEv);
      }

      public void OnCounterReset() {
        m_done = false;
      }
    }
  }
}