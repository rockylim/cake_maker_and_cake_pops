﻿using Caviezel.Chrono;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    [DisallowMultipleComponent]
    public class PtdPressable : PtdComponentActive {
      [Tooltip("the duration this pressable needs to be pressed until it broadcasts the event when exceeded")]
      public float m_duration;
      public bool m_hasStartPressEv;
      [ConditionalHide("m_hasStartPressEv", true)]
      public BaseSenderEv m_startPressEv;
      public bool m_hasReleaseEv;
      [ConditionalHide("m_hasReleaseEv", true)]
      public BaseSenderEv m_releaseEv;
      public bool m_hasExceedEv;
      [ConditionalHide("m_hasExceedEv", true)]
      public bool m_isResetCounter;
      [ConditionalHide("m_hasExceedEv", true)]
      public BaseSenderEv m_exceedDurationEv;
      public ResizeableImage m_tapArea;
      public bool m_isAdditive;

      Timer m_timer;

      public override void InitComponent(Core.ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        //resize the tap area
        m_tapArea.Resize();
        //init pressable
        Owner.InitGestureHandler();
        Owner.Gestures.SetPressable();
        Owner.Gestures.OnStartPressListeners += OnStartPress;
        Owner.Gestures.OnPressingListeners += OnPress;
        Owner.Gestures.OnEndPressListeners += OnReleasePress;
        //set the press time
        m_timer = new Timer(m_duration);
      }

      #region IPressableListener
      public void OnStartPress(Vector2 pos) {
        if (IsActive && m_hasStartPressEv) {
          BroadcastEvent(m_startPressEv);
        }
      }

      public void OnPress(float dt) {
        if (null != m_timer && !m_timer.UpdateTimer(dt)) {
          if (!m_isAdditive) {
            m_timer.Reset();
          } else {
            m_timer = null;
          }
          //broadcast exceed ev
          if (IsActive && m_hasExceedEv) {
            BroadcastEvent(m_exceedDurationEv);
            if (m_isResetCounter) {
              m_timer = new Timer(m_duration);
            }
          }
        }
      }

      public void OnReleasePress(float dt) {
        if (!m_isAdditive) {
          m_timer.Reset();
        }
        //broadcast release ev
        if (IsActive && m_hasReleaseEv) {
          BroadcastEvent(m_releaseEv);
        }
      }
      #endregion
    }
  }
}