﻿using System;
using UnityEngine.UI;
using UnityEngine;

namespace Caviezel {
    namespace Pretend {        
        [Serializable]
        public class ResizeableImage {
            public Image m_reference;
            public Image m_target;

            public void Resize() {
                if (null != m_reference && null != m_target) {
                    Vector2 refSize = m_reference.rectTransform.Size();
                    Vector2 targetSize = refSize.ClampVec2(Constants.TapAreaThreshold, refSize);
                    m_target.rectTransform.SetSize(targetSize);
                }
            }
        }

        public class ScrollableCell {
            public bool IsListenerAdded { get; set; }

            PtdScrollable m_container;
            PtdObject m_object;
            PtdObject m_parent;
            bool m_isListenerAdded;

            public ScrollableCell(PtdScrollable container, PtdObject ptdObject) {                
                m_container = container;
                m_object = ptdObject;
                m_object.InitGestureHandler();
                m_object.Gestures.SetDraggable();
                m_parent = m_object.Parent;
                AddListeners();
            }

            public void UpdateCell() {
                if (m_object.Parent != m_parent) {
                    RemoveListeners();
                } else {
                    AddListeners();
                }
            }

            void AddListeners() {
                if (!m_isListenerAdded) {
                    m_isListenerAdded = true;   
                    m_object.Gestures.OnStartSwipeListeners += m_container.OnBeginScroll;
                    m_object.Gestures.OnSwipingListeners += m_container.OnScroll;
                    m_object.Gestures.OnEndSwipeListeners += m_container.OnEndScroll;
                }
            }

            void RemoveListeners() {
                if (m_isListenerAdded) {
                    m_isListenerAdded = false;  
                    m_object.Gestures.OnStartSwipeListeners -= m_container.OnBeginScroll;
                    m_object.Gestures.OnSwipingListeners -= m_container.OnScroll;
                    m_object.Gestures.OnEndSwipeListeners -= m_container.OnEndScroll;
                }
            }
        }
    }
}