﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.UI;
using Caviezel.Audio;

namespace Caviezel {
  namespace Pretend {
    public class PtdScrollable : PtdComponentBase, IUpdateableComponent {
      public SwipeAxis m_axis;
      public Vector2 m_limit;
      public bool m_isChildBlock;
      public SfxData[] m_onScrollSfx;
      protected SfxManager m_sfxManager;

      ScrollableCell[] m_cells;
      CGestureHandler m_gestureHandler;
      CScrollable m_scrollable;


      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        //init gesture
        m_gestureHandler = new CGestureHandler(gameObject);
        m_gestureHandler.SetDraggable();
        m_gestureHandler.OnStartSwipeListeners += OnBeginScroll;
        m_gestureHandler.OnSwipingListeners += OnScroll;
        m_gestureHandler.OnEndSwipeListeners += OnEndScroll;
        m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
        //init the cell(s)
        if (!m_isChildBlock) {
          PtdObject[] objs = GetComponentsInChildren<PtdObject>(true);
          m_cells = new ScrollableCell[objs.Length];

          for (int i = 0; i < m_cells.Length; ++i) {
            m_cells[i] = new ScrollableCell(this, objs[i]);
          }
        }

        //init scrollable
        m_scrollable = new CScrollable(GetComponent<RectTransform>(), m_axis, m_limit);
      }

      public void UpdateComponent(float dt) {
        //update cell
        if (!m_isChildBlock) {
          for (int i = 0; i < m_cells.Length; ++i) {
            m_cells[i].UpdateCell();
          }
        }
        //update scroll
        m_scrollable.UpdateScroll(dt);
      }

      public void OnBeginScroll(SwipeEventData ev) {
        m_scrollable.OnBeginScroll();
      }

      public void OnScroll(SwipeEventData ev) {
        m_scrollable.OnScroll(ev.Delta);
        m_sfxManager.PlaySound(m_onScrollSfx);
      }

      public void OnEndScroll(SwipeEventData ev) {
        m_scrollable.OnEndScroll();
      }
    }
  }
}
