﻿using System.Collections.Generic;
using Caviezel.Core;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class PtdDragCounterReceiver : PtdComponentActive {
      public BaseSenderEv m_evExceedCounter;
      public List<BaseReceiverEv> m_evResetCount;
      public int m_maxCounter;

      PtdDragCounter m_dragCounter;
      RectTransform m_rect;
      int m_counter;
      bool m_isPlaying;

      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        m_rect = GetComponent<RectTransform>();
        m_counter = 0;
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_evResetCount.Count; ++i) {
            if (m_evResetCount[i].Matches(ev, scope)) {
              m_counter = 0;
              m_dragCounter.OnCounterReset();
              return true;
            }
          }
        }
        return false;
      }
      public RectTransform GetRectTransform() {
        return m_rect;
      }

      public void SetDragCounter(PtdDragCounter dragCounter) {
        m_dragCounter = dragCounter;
      }

      public void IncCounter(string tweenName, out bool isPlayCountSfx) {
        isPlayCountSfx = false;
        if (!IsActive) {
          return;
        }
        if ((IsDone()) || (m_isPlaying)) {
          return;
        }
        m_counter++;
        m_isPlaying = true;
        isPlayCountSfx = true;
        Owner.Tweener.PlayExistingTween(new string[] { tweenName }, () => {
          m_isPlaying = false;
          if (IsDone()) {
            BroadcastEvent(m_evExceedCounter);
            m_dragCounter.OnCounterDone();
          }
        });
      }

      public bool IsDone() {
        return m_counter == m_maxCounter;
      }
    }
  }
}
