﻿using System.Collections;
using UnityEngine;
using Caviezel.UI;
using Caviezel.Tween;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdDragSnapBack : PtdDraggable, IUpdateableComponent {
            public RectTransform m_checkArea;
            public float m_snapBackDuration;
            public BaseReceiverEv m_setDefaultEv;
            public BaseReceiverEv m_snapBackEv;
            public BaseSenderEv m_successDropEv;

            protected bool m_lockSwipe;
            protected PtdObject m_originParent;
            protected Vector2 m_originPos;
            protected ITween m_snapBackTween;


            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //cache origin states
                SetParent();

                if (m_checkArea == null) {
                    m_checkArea = Owner.RootTransform;
                }
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    if (m_setDefaultEv.Matches(ev, scope)) {
                        SetParent();
                        return true;
                    }

                    if (m_snapBackEv.Matches(ev, scope)) {
                        StartCoroutine(DelaySnapBack());
                        return true;
                    }
                }
                return false;
            }

            public override void OnStartSwipe(SwipeEventData ev) {
                if (m_lockSwipe) {
                    return;
                }
                base.OnStartSwipe(ev);
            }

            public override void OnSwiping(SwipeEventData ev) {
                if (m_lockSwipe) {
                    return;
                }
                base.OnSwiping(ev);
            }

            public override void OnEndSwipe(SwipeEventData ev) {
                base.OnEndSwipe(ev);
                if (!m_lockSwipe) {
                    SnapBack();
                }

            }

            public void UpdateComponent(float dt) {
                if (null != m_snapBackTween && !m_snapBackTween.UpdateTween(dt)) {
                    m_snapBackTween = null;
                    Owner.IsInAction = false;
                }
            }

            void SetParent() {
                m_originParent = Owner.Parent;
                m_originPos = Owner.RootTransform.Pos();
            }

            protected IEnumerator DelaySnapBack() {
                yield return null;
                SnapBack();
            }

            protected void SnapBack() {
                Owner.SetParent(m_originParent);
                Owner.RootTransform.SetParent(m_originParent.RootTransform);
                Owner.IsInAction = true;
                m_snapBackTween = new CTweenMove(Owner.gameObject, m_originPos, m_snapBackDuration).Start().SetCompleteCallback(() => {
                    m_lockSwipe = false;
                });
            }
        }
    }
}
