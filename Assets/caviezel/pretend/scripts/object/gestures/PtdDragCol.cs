﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Caviezel.UI;
using Caviezel.Tween;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdDragCol : PtdDragSnapBack {
            public List<RectTransform> m_dropTargets;


            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                return base.OnReceiveEvent(ev, scope);
            }

            public override void OnStartSwipe(SwipeEventData ev) {
                base.OnStartSwipe(ev);
            }

            public override void OnSwiping(SwipeEventData ev) {
                if (m_lockSwipe) {
                    return;
                }
                base.OnSwiping(ev);
                CheckDragCollision(ev);
            }

            public override void OnEndSwipe(SwipeEventData ev) {
                base.OnEndSwipe(ev);
            }

            void CheckDragCollision(SwipeEventData ev) {
                for (int i = 0; i < m_dropTargets.Count; i++) {
                    if (m_checkArea.RectAgainstRoot().Intersects(m_dropTargets[i].RectAgainstRoot())) {
                        m_lockSwipe = true;
                        //broadcast
                        BaseSenderEv senderEv = new BaseSenderEv(m_successDropEv.m_senderId, m_successDropEv.m_eventId + m_dropTargets[i].gameObject.name);
                        BroadcastEvent(senderEv);
                        OnEndSwipe(ev);
                        return;
                    }
                }
            }
        }
    }
}
