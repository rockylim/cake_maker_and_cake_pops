﻿using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdSequential : PtdComponentActive, IUpdateableComponent {
            public PtdObject[] m_sequences;
            public string m_startTweenId;
            public string m_doneTweenId;
            public SequentialEv[] m_nextEvs;
            public SequentialEv[] m_prevEvs;
            public bool m_hasLastEv;
            [ConditionalHide("m_hasLastEv", true)]
            public BaseSenderEv m_onLastEv;

            PtdObject m_cur;
            PtdObject m_prev;
            int m_curSeqIdx;

            public bool IsPlaying {
                get {
                    if (null != m_cur && m_cur.Tweener.IsTweenPlaying(m_startTweenId) && m_cur.Tweener.IsTweenPlaying(m_doneTweenId)) {
                        return true;
                    }
                    if (null != m_prev && m_prev.Tweener.IsTweenPlaying(m_startTweenId) && m_prev.Tweener.IsTweenPlaying(m_doneTweenId)) {
                        return true;
                    }
                    return false;
                }
            }

			public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //init queues
                for (int i = 0; i < m_sequences.Length; ++i) {
                    m_sequences[i].gameObject.SetActive(false);
                }
                //start the step
                PlaySequence();
			}

			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {                    
                    //next seq ev
                    for (int i = 0; i < m_nextEvs.Length; ++i) {
                        if (m_nextEvs[i].Matches(ev, scope)) {                            
                            SetNextStep();
                            return true;
                        }
                    }
                    //prev seq ev
                    for (int i = 0; i < m_prevEvs.Length; ++i) {
                        if (m_prevEvs[i].Matches(ev, scope)) {
                            SetPrevStep();
                            return true;
                        }
                    }
                }
                return false;
			}

			public void UpdateComponent(float dt) {
                if (null != m_prev && !m_prev.Tweener.IsPlaying) {
                    m_prev.gameObject.SetActive(false);
                }
            }

            void SetNextStep() {                
                if (!IsPlaying && m_curSeqIdx < m_sequences.Length) {
                    ++m_curSeqIdx;
                    PlaySequence();
                }
            }

            void SetPrevStep() {
                if (!IsPlaying && m_curSeqIdx > 0) {
                    --m_curSeqIdx;
                    PlaySequence();
                }
            }

            void PlaySequence() {                          
                //set cur as prev if not null
                if (null != m_cur) {
                    m_prev = m_cur;
                    m_prev.Tweener.PlayExistingTween(new string[] { m_doneTweenId });
                }
                if (m_curSeqIdx >= m_sequences.Length) {                    
                    if (m_hasLastEv) {
                        BroadcastEvent(m_onLastEv);   
                    }
                } else {
                    m_cur = m_sequences[m_curSeqIdx];
                    m_cur.gameObject.SetActive(true);
                    m_cur.Tweener.PlayExistingTween(new string[] { m_startTweenId });   
                }
            }
		}       
    }
}
