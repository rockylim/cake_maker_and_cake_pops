﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdRenderer : PtdComponentActive {
            public Image m_reference;
            public RectTransform m_parentArea;
            public RendererEv[] m_flushEvs;
            public bool m_hasRenderedEv;
            [ConditionalHide("m_hasRenderedEv", true)]
            public BaseSenderEv m_onRenderedEv;

            Stack<Image> m_renderers = new Stack<Image>();
            Stack<Image> m_showings = new Stack<Image>();

			public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //init gesture
                Owner.InitGestureHandler();
                Owner.Gestures.SetTappable();
                Owner.Gestures.OnTapListeners += OnTap;
                //set ref inactive on start
                m_reference.gameObject.SetActive(false);
                m_renderers.Push(m_reference);
			}

			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //flush ev
                    for (int i = 0; i < m_flushEvs.Length; ++i) {
                        if (m_flushEvs[i].Matches(ev, scope)) {
                            Flush();
                            return true;
                        }
                    }
                }
                return false;
			}

			void OnTap(Vector2 pos) {
                if (IsActive) {
                    Image img = null;
                    if (m_renderers.Count > 0) {
                        img = m_renderers.Pop();
                    } else {
                        GameObject imgObj = Instantiate(m_reference.gameObject) as GameObject;
                        img = imgObj.GetComponent<Image>();
                        img.name = "renderer";
                    }
                    m_showings.Push(img);
                    img.rectTransform.position = pos;
                    img.rectTransform.SetParent(m_parentArea);                     
                    img.rectTransform.SetScale(Vector2.one);  
                    img.gameObject.SetActive(true);
                    //broadcast if it should
                    if (m_hasRenderedEv) {
                        BroadcastEvent(m_onRenderedEv);
                    }
                }
            }

            void Flush() {
                while (m_showings.Count > 0) {
                    Image showing = m_showings.Pop();
                    showing.gameObject.SetActive(false);
                    m_renderers.Push(showing);
                }
            }
		}       
    }
}
