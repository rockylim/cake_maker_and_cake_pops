﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdCanvas : PtdComponentBase {
            public RectTransform m_canvasArea;
            public Image m_reference;
            public Vector2 m_size;
            public int m_maxCapacity;
            public bool m_isRecycleable;

            List<Image> m_renderers;
            int m_curIdx;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                m_renderers = new List<Image>(m_maxCapacity);
                //create the renderers
                for (int i = 0; i < m_maxCapacity; ++i) {
                    CreateRenderer(i == 0 ? m_reference : null);
                }
            }

            public void Draw(Vector3 pos, Color color) {
                if (CanDraw(pos)) {
                    //check the index
                    if (m_curIdx >= m_renderers.Count) {
                        m_curIdx = m_isRecycleable ? 0 : m_renderers.Count - 1;
                    }
                    //render
                    Image img = m_renderers[m_curIdx];
                    img.rectTransform.position = pos;
                    img.rectTransform.SetSize(m_size);
                    img.SetColor(color);
                    img.gameObject.SetActive(true);
                    //increment the idx
                    ++m_curIdx;   
                }
            }

            void CreateRenderer(Image img) {
                if (null == img) {
                    GameObject imgObj = Instantiate(m_reference.gameObject) as GameObject;
                    img = imgObj.GetComponent<Image>();               
                }
                img.name = m_reference.name;                    
                img.SetPos(Vector2.zero);                    
                img.rectTransform.SetParent(m_canvasArea);                    
                img.gameObject.SetActive(false);
                //add
                m_renderers.Add(img);
            }

            bool CanDraw(Vector3 pos) {
                for (int i = 0; i < m_renderers.Count; ++i) {
                    Vector3 rendererPos = m_renderers[i].transform.position;
                    if (Mathf.Approximately(rendererPos.x, pos.x) && Mathf.Approximately(rendererPos.y, pos.y)) {
                        return false;
                    }
                }
                return true;
            }
        }       
    }
}
