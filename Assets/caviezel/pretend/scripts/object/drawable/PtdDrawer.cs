﻿using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        public class PtdDrawer : PtdComponentBase {
            public RectTransform m_drawArea;
            public Color m_color;

            Vector2 m_originPos;
        }
    }
}
