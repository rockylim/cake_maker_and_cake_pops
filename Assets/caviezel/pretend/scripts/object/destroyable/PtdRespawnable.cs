﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.Tween;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdRespawnable : PtdComponentBase, IRespawnable {            
            public float m_respawnTime;
            [Tooltip("true = will force the respawn pos to m_respawnPos, false = use origin init pos")]
            public bool m_hasRespawnPos;
            [ConditionalHide("m_hasRespawnPos", true)]
            public Vector2 m_respawnPos;
            public bool m_hasRespawnTween;
            [ConditionalHide("m_hasRespawnTween", true)]
            public string m_respawnTweenId;

            ITween m_tween;
            Vector2 m_originPos;
            float m_originRotation;
            PtdObject m_originParent;
            Transform m_originTrasformParent;

            public bool HasRespawned { get; set; }

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //cache the initial state
                CacheInitialState();
            }

            public override void Reinit() {
                base.Reinit();
                //set back to default
                RectTransform ownerRt = Owner.RootTransform;
                ownerRt.SetParent(m_originTrasformParent);
                ownerRt.SetPos(m_hasRespawnPos ? m_respawnPos : m_originPos);
                ownerRt.SetRotation(m_originRotation);
                ownerRt.SetScale(Vector2.zero);
                ownerRt.SetAsFirstSibling();
                //set Owner's parent to null
                Owner.SetParent(m_originParent);
                //play respawn tween if any
                if (m_hasRespawnTween) {                    
                    Owner.Tweener.PlayExistingTween(new string[] { m_respawnTweenId });
                }
                //instantiate the tween scale here
                m_tween = new CTweenScale(
                    ownerRt.gameObject
                    , Vector2.one
                    , m_respawnTime / 3f
                    , m_respawnTime
                    , Easing.OutBounce
                ).Start();   
            }

            public bool Respawn(float dt) {
                if (null != m_tween) {                    
                    if (m_tween.UpdateTween(dt)) {
                        return true;
                    } else {
                        m_tween = null;
                    }
                }
                return false;
            }

            void CacheInitialState() {
                RectTransform ownerRt = Owner.RootTransform;
                m_originPos = ownerRt.Pos();
                m_originRotation = ownerRt.Rotation();
                m_originTrasformParent = ownerRt.parent;
                m_originParent = Owner.Parent;
            }
        }
    }
}