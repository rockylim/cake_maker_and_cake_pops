﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.Tween;

namespace Caviezel {
    namespace Pretend {
        public class PtdDestroyer : PtdComponentActive, IUpdateableComponent {
            public DestroyerEv[] m_destroyEvs;

            CTweenChainer m_tweener;

            const float m_destroyTime = 0.3f;
            const Easing m_destroyEase = Easing.InOutQuad;

            public override void InitComponent(Core.ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //init tweener
                m_tweener = new CTweenChainer();
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_destroyEvs.Length; ++i) {
                        CollisionSenderEv collisionEv = ev as CollisionSenderEv;
                        if (null != collisionEv && m_destroyEvs[i].Matches(ev, scope)) {                            
                            Destruct(collisionEv.m_data.m_collider.Owner, m_destroyEvs[i]);
                            return true;
                        }
                    }
                }
                return false;
            }

            public void UpdateComponent(float dt) {
                m_tweener.Update(dt);
            }

            void Destruct(PtdObject target, DestroyerEv destroyerEv) {   
                target.IsInAction = true;
                //set target's parent to this object first
                target.RootTransform.SetParent(Owner.RootTransform);
                //create the tween container
                HashSet<ITween> destroyTweens = new HashSet<ITween>();
                //play default tween if no tween ids defined
                if (string.IsNullOrEmpty(destroyerEv.m_destroyTweenId)) {
                    //move
                    destroyTweens.Add(new CTweenMove(
                        target.gameObject
                        , Owner.RootTransform.AnchorFromSize(destroyerEv.m_anchorFromCenter)
                        , m_destroyTime
                        , m_destroyEase
                    ));
                    //scale
                    destroyTweens.Add(new CTweenScale(
                        target.gameObject
                        , Vector2.zero
                        , m_destroyTime
                        , 0f
                        , m_destroyEase
                    ));
                } else {
                    destroyTweens = target.Tweener.GetExistingTweens(destroyerEv.m_destroyTweenId);
                    Debug.Assert(destroyTweens.Count > 0, string.Format("cant find any tween with id= {0}, in ptdobject name= {1}", destroyerEv.m_destroyTweenId, target.name));
                }
                //play the tween(s)
                m_tweener.PlayOnly(destroyTweens, () => {
                    OnTargetDestroyed(target, destroyerEv);
                });
            }

            void OnTargetDestroyed(PtdObject target, DestroyerEv destroyerEv) {
                target.IsInAction = false;
                target.IsDestroyed = true;
                //broadcast on destroyed ev
                BroadcastEvent(destroyerEv.m_onDestroyedEv);
            }
        }       
    }
}
