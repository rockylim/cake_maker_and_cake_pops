﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.UI;

namespace Caviezel {
    namespace Pretend {
        public class PtdStackable : PtdComponentBase, IUpdateableComponent {                        
            public SwipeAxis m_axis;
            public int m_direction;
            public float m_spacing;
            public Vector2 m_startPos;
            public float m_duration;

            CStacker m_stacker;
            int m_childCount;

            public void UpdateComponent(float dt) {
                if (m_childCount != transform.childCount) {
                    m_childCount = transform.childCount;
                    if (m_childCount > 0) {
                        List<RectTransform> children = new List<RectTransform>();
                        foreach (Transform child in gameObject.transform) {
                            RectTransform rt = child.GetComponent<RectTransform>();
                            if (null != rt) {
                                children.Add(rt);
                            }
                        }
                        //instantiate the arranger
                        m_stacker = new CStacker(children, m_startPos, (int)m_axis, m_direction, m_spacing, m_duration);
                    } else {
                        m_stacker = null;
                    }
                } else {
                    if (null != m_stacker) {
                        m_stacker.Arrange(dt);
                    }
                }
            }
		}       
    }
}
