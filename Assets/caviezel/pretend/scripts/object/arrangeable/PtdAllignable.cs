﻿using System.Collections.Generic;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class PtdAllignable : PtdComponentBase, IUpdateableComponent {
      public float m_duration = 0.1f;
      public float m_spacing = 10f;

      CAlligner m_arranger;
      int m_childCount;

      public void UpdateComponent(float dt) {
        if (m_childCount != transform.childCount) {
          m_childCount = transform.childCount;
          if (m_childCount > 0) {
            List<RectTransform> children = new List<RectTransform>();
            foreach (Transform child in gameObject.transform) {
              RectTransform rt = child.GetComponent<RectTransform>();
              if (null != rt) {
                children.Add(rt);
              }
            }
            //instantiate the arranger
            m_arranger = new CAlligner(children, m_duration, m_spacing);
          } else {
            m_arranger = null;
          }
        } else {
          if (null != m_arranger) {
            m_arranger.Arrange(dt);
          }
        }
      }
    }
  }
}