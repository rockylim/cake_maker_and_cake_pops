﻿using UnityEngine;
using UnityEngine.UI;

namespace Caviezel {
    namespace Pretend {
        public class PtdActivator : PtdComponentActive {
            public enum m_type {
                Gameobject,
                Mask
            }
            public m_type m_objectType;
            public VisibilityEv[] m_visibilityEv;

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_visibilityEv.Length; ++i) {
                        if (m_visibilityEv[i].Matches(ev, scope)) {
                            GameObject obj = null != m_visibilityEv[i].m_otherObject ? m_visibilityEv[i].m_otherObject : Owner.gameObject;
                            if (m_objectType == m_type.Gameobject) {
                                obj.SetActive(m_visibilityEv[i].m_flag);
                            } else if (m_objectType == m_type.Mask) {
                                obj.GetComponent<RectMask2D>().enabled = false;
                            }

                            return true;
                        }
                    }
                }
                return false;
            }
        }
    }
}