﻿using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        public class PtdToggleActive : PtdComponentActive {
            public BaseReceiverEv[] m_toggleEvs;

			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_toggleEvs.Length; ++i) {
                        if (m_toggleEvs[i].Matches(ev, scope)) {
                            bool isActive = Owner.gameObject.activeSelf;
                            Owner.gameObject.SetActive(!isActive);
                            return true;
                        }
                    }
                }
                return false;
			}
		}       
    }
}
