﻿using UnityEngine;
using Caviezel.Chrono;

namespace Caviezel {
    namespace Pretend {
        public class PtdConfetti : PtdParticle {
            float m_targetX;

            public override void InitParticle(PtdParticleController controller, RectTransform area) {
                base.InitParticle(controller, area);
                SetShowDelay(new PtdParticleController.ParticleRangeCustomization(1, 1));
                SetSpeed(new PtdParticleController.ParticleRangeCustomization(20, 50), new PtdParticleController.ParticleRangeCustomization(70, 400));
                m_alphaInc = 1f;
            }

            public override void UpdateMove(float dt) {
                base.UpdateMove(dt);

                float speedY = m_speedY * dt;
                m_rectTransform.SetY(m_rectTransform.Y() - speedY);

                float speedR = m_speedR * dt;
                m_rectTransform.SetRotation(m_rectTransform.Rotation() + speedR);

                float speedX = m_speedX * dt;
                float posX = m_rectTransform.X();
                if (speedX <= 0) {
                    if (posX + speedX < m_targetX) {
                        m_rectTransform.SetX(m_targetX);
                        SetXTargetAndSpeed();
                    } else {
                        m_rectTransform.SetX(posX + speedX);
                    }
                } else {
                    if (posX + speedX > m_targetX) {
                        m_rectTransform.SetX(m_targetX);
                        SetXTargetAndSpeed();
                    } else {
                        m_rectTransform.SetX(posX + speedX);
                    }
                }

                if (m_rectTransform.Y() < (-m_area.Height() * .5f) - 30f) {
                    OnHide();
                }
            }

            public override void ResetParticle() {
                base.ResetParticle();
                int halfWidth = Mathf.RoundToInt(m_area.Width() * .5f);
                float posX = m_rand.Next(0, halfWidth);
                if (m_rand.Next(1, 3) == 1) {
                    m_rectTransform.SetX(-posX);
                } else {
                    m_rectTransform.SetX(posX);
                }

                float posY = m_rand.Next(30, Mathf.RoundToInt(m_area.Height()));
                int top = Mathf.RoundToInt(m_area.Height() * .5f);
                m_rectTransform.SetY(top + posY);

                m_speedY = m_rand.Next(m_speedYRange.min, m_speedYRange.max);
                m_speedR = m_rand.Next(100, 200);
                if (m_rand.Next(1, 3) == 1) {
                    m_speedR = -m_speedR;
                }
                m_rectTransform.SetRotation(0);

                SetXTargetAndSpeed();
            }

            public override void OnInitTimerShow() {
                base.OnInitTimerShow();
                m_timer = new Timer(m_rand.Next(m_showDelay.min, m_showDelay.max) / 100f);
            }


            public override void OnShow() {
                base.OnShow();
                m_image.SetAlpha(1f);
            }

            // private void
            void SetXTargetAndSpeed() {
                m_targetX = m_rand.Next(300, 600);
                m_speedX = m_rand.Next(m_speedXRange.min, m_speedXRange.max);
                if (m_rand.Next(1, 3) == 1) {
                    m_targetX = -m_targetX;
                    m_speedX = -m_speedX;
                }
                m_targetX = m_rectTransform.X() + m_targetX;
            }
        }
    }
}

