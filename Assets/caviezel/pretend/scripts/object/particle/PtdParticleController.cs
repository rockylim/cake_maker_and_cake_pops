﻿using System;
using System.Collections.Generic;
using Caviezel.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Caviezel {
  namespace Pretend {
    public class PtdParticleController : PtdComponentActive, IUpdateableComponent {
      [Serializable]
      public struct ParticleRangeCustomization {
        public int min;
        public int max;
        public ParticleRangeCustomization(int minRange, int maxRange) {
          min = minRange;
          max = maxRange;
        }
      }

      [Serializable]
      public enum ParticleType {
        Confetti,
        Dust,
        Splash,
        Spread,
        Twinkle,
        FallenLeaf,
        FireFlies,
        Circular
      }

      [Serializable]
      public class LimitSpriteSelectionEv : BaseReceiverEv {
        public int m_limitStart;
        public int m_limitEnd;
      }


      public ParticleType m_type;
      public List<Sprite> m_sprites = new List<Sprite>();
      public List<Color> m_colors = new List<Color>();
      public GameObject m_template;
      public BaseReceiverEv m_onStartParticleEv;
      public BaseReceiverEv m_onStopParticleEv;
      public LimitSpriteSelectionEv[] m_limitSpriteSelectionEvs;
      public RectTransform m_area;
      public bool m_isLoop;
      public bool m_isResetOnStart;
      public int m_total;
      public float m_nativeSizeRatio;
      [Header("Customization Area")]
      [Header("Show Delay")]
      public ParticleRangeCustomization m_showDelay;
      [Header("Speed")]
      public ParticleRangeCustomization m_speedXRange;
      public ParticleRangeCustomization m_speedYRange;
      int m_totalDone;

      List<PtdParticle> m_particles = new List<PtdParticle>();
      System.Random m_rand;
      bool m_isAnimated;
      bool m_isRestartLoop;
      int m_totalColor;
      Image m_image;


      #region PtdComponentActive
      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        InitParticle();
      }

      public override void OnStartGame() {
        base.OnStartGame();
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          if (m_onStartParticleEv.Matches(ev, scope)) {
            if (m_isAnimated) {
              if (m_isResetOnStart) {
                ParticlesDone();
              } else {
                return true;
              }
            }

            m_isLoop = m_isRestartLoop;
            OnParticleStart();
            return true;
          }

          if (m_onStopParticleEv.Matches(ev, scope)) {
            m_isLoop = false;
            return true;
          }

          if (m_limitSpriteSelectionEvs != null) {
            for (int i = 0; i < m_limitSpriteSelectionEvs.Length; i++) {
              if (m_limitSpriteSelectionEvs[i].Matches(ev, scope)) {
                for (int j = 0; j < m_particles.Count; j++) {
                  m_image = m_particles[j].gameObject.GetComponent<Image>();
                  SetImage(m_limitSpriteSelectionEvs[i].m_limitStart, m_limitSpriteSelectionEvs[i].m_limitEnd + 1);
                }
                return true;
              }
            }
          }
        }

        return false;
      }

      public void InitParticle() {
        m_rand = MathExtensions.GetRandom();

        if (m_area == null) {
          m_area = GetComponent<RectTransform>();
        }

        m_totalColor = (m_colors != null && m_colors.Count > 0) ? m_colors.Count : -1;

        for (int i = 0; i < m_total; i++) {
          GameObject obj = null;
          if (i == 0) {
            AddParticleScript();
            obj = m_template;
          } else {
            obj = Instantiate(m_template.gameObject) as GameObject;
          }
          obj.name = "particle" + i.ToString();
          obj.transform.SetParent(transform, false);

          m_image = obj.GetComponent<Image>();
          m_image.raycastTarget = false;

          SetImage(0, m_sprites.Count);

          PtdParticle particle = obj.GetComponent<PtdParticle>();
          particle.InitParticle(this, m_area);

          m_particles.Add(particle);

          m_isRestartLoop = m_isLoop;
        }

        gameObject.SetActive(false);
      }

      public void UpdateParticle(float dt) {
        if (IsActive && m_isAnimated) {
          for (int i = 0; i < m_total; i++) {
            m_particles[i].UpdateParticle(dt);
          }
        }
      }

      public void OnParticleStart() {
        gameObject.SetActive(true);
        m_isAnimated = true;
        for (int i = 0; i < m_total; i++) {
          m_particles[i].OnInitTimerShow();
        }
      }

      public void OnParticleStop() {
        m_totalDone = m_totalDone + 1;
        if (m_totalDone >= m_total) {
          ParticlesDone();
        }
      }



      public List<PtdParticle> GetParticles() {
        return m_particles;
      }

      #endregion

      #region IUpdateable
      public void UpdateComponent(float dt) {
        UpdateParticle(dt);
      }
      #endregion


      void ParticlesDone() {
        Debug.Log("PARTICLE DONE");
        m_isAnimated = false;
        m_totalDone = 0;
        gameObject.SetActive(false);
      }

      void AddParticleScript() {
        if (m_type == ParticleType.Confetti) {
          m_template.AddComponent<PtdConfetti>();
        } else if (m_type == ParticleType.Dust) {
          m_template.AddComponent<PtdDust>();
        } else if (m_type == ParticleType.Splash) {
          m_template.AddComponent<PtdSplash>();
        } else if (m_type == ParticleType.Spread) {
          m_template.AddComponent<PtdSpread>();
        } else if (m_type == ParticleType.Twinkle) {
          m_template.AddComponent<PtdTwinkle>();
        } else if (m_type == ParticleType.FireFlies) {
          m_template.AddComponent<PtdFireFlies>();
        } else if (m_type == ParticleType.FallenLeaf) {
          m_template.AddComponent<PtdFallenLeaf>();
        }
      }

      void SetImage(int min, int max) {
        int rand = m_rand.Next(min, max);
        m_image.sprite = m_sprites[rand];
        m_image.SetNativeSize();
        if (m_nativeSizeRatio > 0f) {
          RectTransform m_imageRect = m_image.GetComponent<RectTransform>();
          m_imageRect.SetSize(new Vector2(m_imageRect.Width() * m_nativeSizeRatio, m_imageRect.Height() * m_nativeSizeRatio));
        }

        if (m_totalColor > -1) {
          if ((rand < m_totalColor)) {
            if ((m_colors[rand] != Color.white)) {
              m_image.SetColor(m_colors[rand]);
            }
          }
        }
      }
    }
  }
}