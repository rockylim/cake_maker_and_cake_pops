﻿using System.Collections.Generic;
using Caviezel.Chrono;
using Caviezel.Tween;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class PtdTwinkle : PtdParticle {
      float m_posX;
      float m_posY;
      float m_speed;

      public override void InitParticle(PtdParticleController controller, RectTransform area) {
        base.InitParticle(controller, area);
        m_alphaInc = 0f;
        m_area = area;
        SetShowDelay(new PtdParticleController.ParticleRangeCustomization(0, 250));
        SetSpeed(new PtdParticleController.ParticleRangeCustomization(70, 70), new PtdParticleController.ParticleRangeCustomization(70, 70));
        m_speed = m_speedXRange.min / 100f;
      }

      public override void UpdateMove(float dt) {
        base.UpdateMove(dt);
        m_image.SetAlpha(m_image.color.a + m_speed * dt);
        if ((m_image.Alpha() >= 1f) || (m_image.Alpha() <= 0f)) {
          m_speed = -m_speed;
          if (m_image.Alpha() <= 0) {
            OnHide();
          }
        }
      }

      public override void OnInitTimerShow() {
        base.OnInitTimerShow();
        m_timer = new Timer(m_rand.Next(m_showDelay.min, m_showDelay.max) / 100f);
      }

      public override void OnShow() {
        base.OnShow();
      }

      public override void OnDone() {
        base.OnDone();
      }

      public override void ResetParticle() {
        base.ResetParticle();
        m_posY = GetRandomPosition(0, (int)(m_area.Height() * .5f));
        m_posX = GetRandomPosition(0, (int)(m_area.Width() * .5f));

        m_rectTransform.SetPos(new Vector2(m_posX, m_posY));

        float randomScale = m_rand.Next(20, 100) / 100f;
        m_rectTransform.SetScale(new Vector2(randomScale, randomScale));

        float randomRotation = m_rand.Next(0, 360);
        m_rectTransform.SetRotation(randomRotation);

        m_image.SetAlpha(0f);
      }

      float GetRandomPosition(int range1, int range2) {
        float target = m_rand.Next(range1, range2);

        int randomTarget = m_rand.Next(1, 3);
        if (randomTarget == 1) {
          target = -target;
        }

        return target;
      }

    }
  }
}


