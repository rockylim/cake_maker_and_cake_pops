﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caviezel;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdParticleSelector : PtdComponentActive {
            public List<PtdParticleController> m_particleControllers;
            public BaseReceiverEv[] m_selectParticleControllerEvs;

            #region PtdComponentActive
            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
            }

            public override void OnStartGame() {
                base.OnStartGame();
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_selectParticleControllerEvs.Length; ++i) {
                        if (m_selectParticleControllerEvs[i].Matches(ev, scope)) {
                            int index = GetIndex();
                            if (index > -1) {
                                m_particleControllers[index].OnParticleStart();
                            }
                            return true;
                        }
                    }
                }

                return false;
            }

            public void OnParticleControllerDone() {
            }
            #endregion         

            int GetIndex() {
                for (int i = 0; i < m_particleControllers.Count; i++) {
                    if (!m_particleControllers[i].gameObject.activeSelf) {
                        return i;
                    }
                }
                return -1;
            }
        }
    }
}