﻿using UnityEngine;
using Caviezel.Chrono;

namespace Caviezel {
    namespace Pretend {
        public class PtdDust : PtdParticle {
            float m_speedDistance;

            public override void InitParticle(PtdParticleController controller, RectTransform area) {
                base.InitParticle(controller, area);
                SetShowDelay(new PtdParticleController.ParticleRangeCustomization(0, 60));
                SetSpeed(new PtdParticleController.ParticleRangeCustomization(1, 40), new PtdParticleController.ParticleRangeCustomization(20, 80));
                m_speedDistance = 0f;
                m_alphaInc = 4f;
            }

            public override void UpdateMove(float dt) {
                base.UpdateMove(dt);
                m_rectTransform.SetY(m_rectTransform.Y() + (m_speedY * dt));
                m_rectTransform.SetX(m_rectTransform.X() + (m_speedX * dt));
                m_speedY = m_speedY + m_speedDistance;
            }

            public override void OnInitTimerShow() {
                base.OnInitTimerShow();
                m_timer = new Timer(m_rand.Next(m_showDelay.min, m_showDelay.max) / 100f);
            }

            public override void OnShow() {
                base.OnShow();
                float duration = m_rand.Next(100, 400) / 100;
                m_timer = new Timer(duration);
            }

            public override void ResetParticle() {
                base.ResetParticle();
                int halfWidth = Mathf.RoundToInt(m_area.Width() * .5f);
                float posX = m_rand.Next(-halfWidth, halfWidth);
                m_rectTransform.SetX(posX);

                int halfHight = Mathf.RoundToInt(m_area.Height() * .5f);
                float posY = m_rand.Next(-halfHight, halfHight);
                m_rectTransform.SetY(posY);

                m_speedX = m_rand.Next(m_speedXRange.min, m_speedXRange.max);

                int randomX = m_rand.Next(1, 3);
                if (randomX == 1) {
                    m_speedX = -m_speedX;
                }

                m_speedY = -m_rand.Next(m_speedYRange.min, m_speedYRange.max);
                m_speedDistance = m_rand.Next(2, 8);

                float randomScale = m_rand.Next(20, 100) / 100f;
                m_rectTransform.SetScale(new Vector2(randomScale, randomScale));

                float randomRotation = m_rand.Next(0, 360);
                m_rectTransform.SetRotation(randomRotation);

                m_image.SetAlpha(0f);
            }
        }
    }
}

