﻿using UnityEngine;
using Caviezel.Chrono;

namespace Caviezel {
    namespace Pretend {
        public class PtdSplash : PtdParticle {
            float m_gravity;

            public override void InitParticle(PtdParticleController controller, RectTransform area) {
                base.InitParticle(controller, area);
                m_gravity = 0;
                m_alphaInc = 4f;
                SetShowDelay(new PtdParticleController.ParticleRangeCustomization(0, 300));
                SetSpeed(new PtdParticleController.ParticleRangeCustomization(0, 180), new PtdParticleController.ParticleRangeCustomization(220, 400));
            }

            public override void UpdateMove(float dt) {
                base.UpdateMove(dt);
                m_rectTransform.SetY(m_rectTransform.Y() + (m_speedY * dt));
                m_rectTransform.SetX(m_rectTransform.X() + (m_speedX * dt));
                m_speedY = m_speedY - m_gravity;
            }

            public override void ResetParticle() {
                base.ResetParticle();
                m_rectTransform.SetX(0f);
                m_rectTransform.SetY(0f);
                m_speedX = m_rand.Next(m_speedXRange.min, m_speedXRange.max);
                int randomX = m_rand.Next(1, 3);
                if (randomX == 1) {
                    m_speedX = -m_speedX;
                }

                m_speedY = m_rand.Next(m_speedYRange.min, m_speedYRange.max);
                m_gravity = m_rand.Next(10, 20);

                float randomScale = m_rand.Next(90, 100) / 100f;
                m_rectTransform.SetScale(new Vector2(randomScale, randomScale));
                float randomRotation = m_rand.Next(0, 360);
                m_rectTransform.SetRotation(randomRotation);
                m_image.SetAlpha(0f);
            }

            public override void OnInitTimerShow() {
                base.OnInitTimerShow();
                m_timer = new Timer(m_rand.Next(m_showDelay.min, m_showDelay.max) / 100f);
            }

            public override void OnShow() {
                base.OnShow();
                float duration = m_rand.Next(100, 200) / 100;
                m_timer = new Timer(duration);
            }
        }
    }
}

