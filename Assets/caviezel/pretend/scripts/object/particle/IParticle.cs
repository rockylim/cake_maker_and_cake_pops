﻿using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        public interface IParticle {
            void InitParticle(PtdParticleController controller, RectTransform area);
            void UpdateParticle(float dt);
            void ResetParticle();
            void OnInitTimerShow();
            void OnShow();
            void OnHide();
        }
    }
}

