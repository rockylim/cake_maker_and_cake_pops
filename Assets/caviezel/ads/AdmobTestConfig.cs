using UnityEngine;

[CreateAssetMenu(fileName = "AdmobTestConfig", menuName = "ScriptableObjects/AdmobTestConfigScriptableObject", order = 1)]
public class AdmobTestConfig : ScriptableObject {
  public bool m_testOn = false;
  public string m_testDeviceID = "";
}
