﻿using System;
using System.Collections.Generic;
using Caviezel;
using Caviezel.Core;
using Caviezel.Tween;
using UnityEngine;
using UnityEngine.UI;

namespace Brainfull.CrossPromo {
  using ServerItem = BCServerImages.Item;

  public class BCCrossPromo : BCServerImages, ISystem {
    public delegate string GetUrl(ServerItem s);

    [Serializable]
    public enum Device {
      iOS,
      Android,
      Amazon
    }

    public class CrossPromoItem {
      public ServerItem Model { get; private set; }
      public string Url { get; private set; }

      public CrossPromoItem(ServerItem m, string url) {
        Model = m;
        Url = url;
      }
    }

    public RectTransform IconGroup;
    public BCIcon IconTemplate;
    public BCPopup Popup;
    [Tooltip("when it reaches the max counter defined here, next popup will show")]
    public int MaxPopupCounter;

    Device Platform;

    List<BCIcon> _icons = new List<BCIcon>();
    List<CrossPromoItem> _popups = new List<CrossPromoItem>();
    Dictionary<Device, GetUrl> _handlers = new Dictionary<Device, GetUrl>();
    int _showCounter = 0;
    int _curPopup = 0;

    bool _downloaded = false;
    int m_indexScene = 0;
    bool m_isFirstEntry = false;
    #region ISystem implementation
    public void InitSystem() {
      m_isFirstEntry = true;
      InitCrossPromo();
      OnDownloaded += _ => {
        _downloaded = true;
        print("done downloading");
      };
    }

    public void StartSystem(CavEngine gameEngine) {
    }

    public void UpdateSystem(float dt) {
      UpdateCrossPromo(dt);
    }

    public void OnChangeScene(int index) {
      m_indexScene = index;
      if (index == 0) {
        if (m_isFirstEntry == true) {
          m_isFirstEntry = false;
        } else {
          TryShow(() => ShowNextPopup());
          _showCounter = 0;
        }
      } else {
        TryShow(() => IncrementCounter());
      }
    }
    #endregion

    void InitCrossPromo() {
#if UNITY_IOS
      Platform = Device.iOS;
#else
      if (Constants.IS_AMAZON) {
        Platform = Device.Amazon;
      } else {
        Platform = Device.Android;
        //TweenScale twScale = IconTemplate.GetComponent<TweenScale>();
        //twScale.m_scaleData[0].m_scale.x = 1.3f;
        //twScale.m_scaleData[0].m_scale.y = 1.3f;
      }
#endif

      // init handler callbacks
      _handlers[Device.iOS] = s => s.ios;
      _handlers[Device.Amazon] = s => s.amazon;
      _handlers[Device.Android] = s => s.google;
      // init pop up 
      Popup.InitPopup();
      SetVisibleIconGroup(false);
      // download
      StartCoroutine(InitServerImages());
      OnDownloaded += downloaded => {
        for (int i = 0; i < downloaded.Count; ++i) {
          ServerItem si = downloaded[i];
          CrossPromoItem item = new CrossPromoItem(si, _handlers[Platform](si));

          BCIcon icon = i == 0 ? IconTemplate : IconTemplate.Clone<BCIcon>(IconGroup);
          icon.InitIcon(item);
          _icons.Add(icon);
#if !UNITY_IOS
          /*if (!Constants.IS_AMAZON) {
            //only for google play show 1 icon            
            if (!item.Url.Contains("puppymommy")) {
              icon.gameObject.SetActive(false);
            } else {
              icon.gameObject.SetActive(true);
              HorizontalLayoutGroup layout = icon.transform.parent.GetComponent<HorizontalLayoutGroup>();
              layout.childAlignment = TextAnchor.UpperRight;
              layout.padding.right = 20;
              layout.padding.top = 20;
            }
          }*/
#endif
          // add to thumbnail list if defined in the json
          if (!item.Model.thumbnail.IsEmpty()) _popups.Add(item);
        }
      };
    }

    void UpdateCrossPromo(float dt) {
      foreach (BCIcon ic in _icons) {
        ic.UpdateIcon(dt);
      }
    }

    public void IncrementCounter() {
      ++_showCounter;
      if (_showCounter == MaxPopupCounter) {
        _showCounter = 0;
        ShowNextPopup();
      }
    }

    public void SetVisibleIconGroup(bool flag) {
      IconGroup.SetVisible(flag);
    }

    public void ShowNextPopup() {
      if (Constants.IS_PAID) {
        return;
      }
      if (_popups.Count == 0) {
        Print.Log("no pop up found, nothing to show");
        return;
      }

      if (_curPopup >= _popups.Count) _curPopup = 0;
      // show
      Popup.Item = _popups[_curPopup];
      ++_curPopup;
    }

    public void TryShow(Action cb) {
      if (!_downloaded) {
        print("still downloading");
        return;
      }

      cb();
    }

    public bool IsDownloaded() {
      return _downloaded;
    }
  }
}

