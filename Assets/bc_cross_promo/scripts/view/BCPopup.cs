﻿using UnityEngine;
using UnityEngine.UI;

namespace Brainfull.CrossPromo {
  using Item = BCCrossPromo.CrossPromoItem;

  public class BCPopup : MonoBehaviour {
    public GameObject Root;
    public RawImage ImgThumb;
    public RawImage ImgIcon;
    public Text TxtTitle;
    public GameObject BtnDownload;
    public GameObject[] CloseTriggers;

    public Item Item {
      get { return _item; }
      set {
        Root.SetActive(true);

        _item = value;

        ImgThumb.texture = _item.Model.GetThumbTexture();
        ImgIcon.texture = _item.Model.GetIconTexture();
        TxtTitle.text = _item.Model.name;
      }
    }

    Item _item;

    public void InitPopup() {
    }

    public void OnClose() {
      Root.SetActive(false);
    }

    public void OnTap() {
      Application.OpenURL(Item.Url);
    }
  }
}

