﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace Brainfull.CrossPromo {
  public delegate void DownloadedFile<T>(T file, bool err);

  public class ImageDownloader {
    public bool Downloading { get; private set; }

    public IEnumerator Download(string url, string filename, Action<bool> result) {
      string localPath = Path.Combine(Application.persistentDataPath, filename.LastSplit());
      if (File.Exists(localPath)) {
        result(true);
      } else {
        Downloading = true;

        string fullUrl = string.Format("{0}/{1}", url, filename.LastSplit());
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(fullUrl)) {
          yield return uwr.SendWebRequest();
          if (uwr.result == UnityWebRequest.Result.ConnectionError || uwr.result == UnityWebRequest.Result.ProtocolError || uwr.result == UnityWebRequest.Result.DataProcessingError) {
            result(false);
          } else {
            // save the texture            
            Texture2D texture = DownloadHandlerTexture.GetContent(uwr);
            string ext = filename.LastSplit().ToLower();

            File.WriteAllBytes(localPath, ext.Contains("png") ? texture.EncodeToPNG() : texture.EncodeToJPG());
            result(true);
          }

          Downloading = false;
        }
      }
    }
  }

  public class JsonDownloader<T> {
    public bool Downloading { get; private set; }

    public IEnumerator Download(string url, DownloadedFile<T> result) {
      Downloading = true;

      using (UnityWebRequest uwr = UnityWebRequest.Get(url)) {
        yield return uwr.SendWebRequest();
        if (uwr.result == UnityWebRequest.Result.ConnectionError || uwr.result == UnityWebRequest.Result.ProtocolError || uwr.result == UnityWebRequest.Result.DataProcessingError) {
          result(default(T), true);
        } else {
          // save
          string text = DownloadHandlerBuffer.GetContent(uwr);
          T json = JsonUtility.FromJson<T>(text);
          result(json, false);
        }

        Downloading = false;
      }
    }
  }

  public static class Print {
    /// <summary>
    /// Wrapper around unity Debug.Log but only gets printed on unity editor,
    /// it wont be printed during production e.g. on mobile devices.
    /// </summary>
    /// <param name="msg">The message to print on the editor</param>
    public static void Log(object msg) {
#if UNITY_EDITOR
      Debug.Log(msg);
#endif
    }
  }

  public static class Assert {
    public static void Null(object obj, string err = null) {
#if UNITY_EDITOR
      Debug.Assert(null != obj, string.IsNullOrEmpty(err) ? $"obj is null" : err);
#endif
    }
  }

  public static class Extensions {
    public static Texture2D ToTexture2D(this string filePath) {
      Texture2D t = new Texture2D(1, 1);
      t.LoadImage(File.ReadAllBytes(filePath));
      return t;
    }

    public static T First<T>(this IList<T> arr) {
      return arr[0];
    }

    public static T Last<T>(this IList<T> l) {
      return l.Count > 0 ? l[l.Count - 1] : default(T);
    }

    public static string LastSplit(this string str, char separator = '/') {
      return str.Split(separator).Last();
    }

    public static string FirstSplit(this string str, char separator = '/') {
      return str.Split(separator).First();
    }

    public static bool IsEmpty(this string str) {
      return string.IsNullOrEmpty(str);
    }
  }
}